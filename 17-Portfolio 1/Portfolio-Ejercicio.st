!classDefinition: #PortfolioTest category: 'Portfolio-Ejercicio'!
TestCase subclass: #PortfolioTest
	instanceVariableNames: 'bank portfolio portfolioChild portfolioParent account'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!PortfolioTest methodsFor: 'testing' stamp: 'JDS 10/17/2021 17:26:23'!
setUp

	account _ ReceptiveAccount new.
	
	portfolio := Portfolio new.
	portfolioChild := Portfolio new.
	portfolioParent := Portfolio new.! !

!PortfolioTest methodsFor: 'testing' stamp: 'AE 10/13/2021 04:39:53'!
test01PortforlioHasZeroAsBalanceWhenCreated 

	self assert: 0 equals: account balance.
! !

!PortfolioTest methodsFor: 'testing' stamp: 'JDS 10/17/2021 15:22:54'!
test02AddAccountIsRegistered

	portfolio addBankingInstrument: account.
	
	self assert: (portfolio includes: account).

! !

!PortfolioTest methodsFor: 'testing' stamp: 'JDS 10/17/2021 15:22:54'!
test03AddAccountTwiceRaiseAnError

	portfolio addBankingInstrument: account.
	
	self should: [portfolio addBankingInstrument: account] 
		raise: Error 
		description: Portfolio aBankingInstrumentIsAlreadyAddedErrorDescription.
! !

!PortfolioTest methodsFor: 'testing' stamp: 'JDS 10/17/2021 15:22:54'!
test04AddAccountWithPositiveBalance

	| aQuantity |
	
	aQuantity _ 50.
	Deposit register: aQuantity on: account.
	
	portfolio addBankingInstrument: account.
	
	self assert: aQuantity equals: portfolio balance.

! !

!PortfolioTest methodsFor: 'testing' stamp: 'JDS 10/17/2021 17:16:30'!
test05AddMultipleAccountsAndGetPortfolioBalance

	| firstAccount secondAccount |
	
	firstAccount _ ReceptiveAccount new.
	Deposit register: 50 on: firstAccount.
	secondAccount _ ReceptiveAccount new.
	Withdraw register: 25 on: secondAccount. 
	Deposit register: 75 on: secondAccount.
	
	portfolio addBankingInstrument: firstAccount.
	portfolio addBankingInstrument: secondAccount.
	
	self assert: 100 equals: portfolio balance.

! !

!PortfolioTest methodsFor: 'testing' stamp: 'JDS 10/17/2021 15:22:54'!
test06AddPortfolioToItselfShouldRaiseAnError

	self  should: [portfolio addBankingInstrument: portfolio] 
		raise: Error 
		description: Portfolio portfolioIsRegisteredToItselfErrorDescription.

! !

!PortfolioTest methodsFor: 'testing' stamp: 'JDS 10/17/2021 16:27:25'!
test07AddRegisteredAccountOnParentPortfolioShouldRaiseAnError

	Deposit register: 50 on: account.
	
	portfolioChild addBankingInstrument: account .
	portfolioParent addBankingInstrument: portfolioChild.
	
	self should: [	portfolioParent addBankingInstrument: account] raise: Error.

! !

!PortfolioTest methodsFor: 'testing' stamp: 'JDS 10/17/2021 17:16:45'!
test08PortfoliosWithCommonAccountsCannotBeRegistered

	| account2 account3|
	
	account2 _ ReceptiveAccount new.
	account3 _ ReceptiveAccount new.
		
	portfolioChild addBankingInstrument: account2 .
	portfolioParent addBankingInstrument: account.
	portfolioParent addBankingInstrument: account2.
	portfolioParent addBankingInstrument: account3.
	
	self should: [	portfolioParent addBankingInstrument: portfolioChild] 
		raise: Error
		description: Portfolio portfolioHasAnAlreadyRegisteredBankingInstrumentDescription .
		

! !

!PortfolioTest methodsFor: 'testing' stamp: 'JDS 10/17/2021 15:22:53'!
test09AddRegisteredParentAccountOnAChildPortfolioShouldRaiseAnError


	portfolioParent addBankingInstrument: account .
	portfolioParent addBankingInstrument: portfolioChild.
	
	self should: [	portfolioChild addBankingInstrument: account] raise: Error.

! !

!PortfolioTest methodsFor: 'testing' stamp: 'JDS 10/17/2021 17:16:57'!
test10PortfolioKnowsItsTransactions

	| firstAccount secondAccount transaction transaction2 transaction3 |

	firstAccount _ ReceptiveAccount new.
	transaction _ Deposit register: 50 on: firstAccount.
	
	secondAccount _ ReceptiveAccount new.
	transaction2 _ Withdraw register: 25 on: secondAccount. 
	transaction3 _ Deposit register: 75 on: secondAccount.
	
	portfolio addBankingInstrument: firstAccount.
	portfolio addBankingInstrument: secondAccount.
	portfolio addBankingInstrument: portfolioChild.
	
	self assert: (portfolio hasTransaction: transaction).
	self assert: (portfolio hasTransaction: transaction2).
	self assert: (portfolio hasTransaction: transaction3).
! !

!PortfolioTest methodsFor: 'testing' stamp: 'JDS 10/17/2021 17:17:30'!
test11PortfolioKnowsAllItsTransactions

	| firstAccount secondAccount transaction transaction2 transaction3 allTransactions |
	
	firstAccount _ ReceptiveAccount new.
	transaction _ Deposit register: 50 on: firstAccount.
	
	secondAccount _ ReceptiveAccount new.
	transaction2 _ Withdraw register: 25 on: secondAccount. 
	transaction3 _ Deposit register: 75 on: secondAccount.
	
	portfolio addBankingInstrument: firstAccount.
	portfolio addBankingInstrument: secondAccount.
	portfolio addBankingInstrument: portfolioChild.
	
	allTransactions _ portfolio transactions .
	
	self assert: (allTransactions includes: transaction).
	self assert: (allTransactions includes: transaction2).
	self assert: (allTransactions includes: transaction3).
	self assert: 3 equals: allTransactions size.
! !

!PortfolioTest methodsFor: 'testing' stamp: 'JDS 10/17/2021 18:58:54'!
test12PortfoliosWithCommonPortfoliosCannotBeRegistered

	| portfolioParent2 portfolioParent3 portfolioGrandParent |
	portfolioParent2 _ Portfolio new.
	portfolioParent3 _ Portfolio new.
	portfolioGrandParent _ Portfolio new.
	portfolioParent2 addBankingInstrument: portfolioChild.
	portfolioParent3 addBankingInstrument: portfolioParent2.
	portfolioGrandParent addBankingInstrument: portfolioChild.
	
	self should: [	portfolioParent3 addBankingInstrument: portfolioGrandParent] 
		raise: Error
		description: Portfolio portfolioHasAnAlreadyRegisteredBankingInstrumentDescription .
		

! !


!classDefinition: #ReceptiveAccountTest category: 'Portfolio-Ejercicio'!
TestCase subclass: #ReceptiveAccountTest
	instanceVariableNames: 'bank account'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'JDS 10/17/2021 17:28:24'!
setUp

	account := ReceptiveAccount new.
	
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'AE 10/13/2021 04:48:13'!
test01ReceptiveAccountHaveZeroAsBalanceWhenCreated 

	self assert: 0 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'AE 10/13/2021 04:48:22'!
test02DepositIncreasesBalanceOnTransactionValue 

	
	Deposit register: 100 on: account.
		
	self assert: 100 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'AE 10/13/2021 04:48:31'!
test03WithdrawDecreasesBalanceOnTransactionValue 

	Deposit register: 100 on: account.
	Withdraw register: 50 on: account.
		
	self assert: 50 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'AE 10/13/2021 04:48:47'!
test04WithdrawValueMustBePositive 

	| withdrawValue |

	withdrawValue := 50.
	
	self assert: withdrawValue equals: (Withdraw register: withdrawValue on: account) value
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'AE 10/13/2021 04:49:07'!
test05ReceptiveAccountKnowsRegisteredTransactions 

	| deposit withdraw |

	deposit := Deposit register: 100 on: account.
	withdraw := Withdraw register: 50 on: account.
		
	self assert: (account hasRegistered: deposit).
	self assert: (account hasRegistered: withdraw).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'AE 10/13/2021 04:50:00'!
test06ReceptiveAccountDoNotKnowNotRegisteredTransactions

	| deposit withdraw |
	
	deposit :=  Deposit for: 200.
	withdraw := Withdraw for: 50.
		
	self deny: (account hasRegistered: deposit).
	self deny: (account hasRegistered:withdraw).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'AE 10/13/2021 04:50:40'!
test07AccountKnowsItsTransactions 

	| deposit1 |
	
	deposit1 := Deposit register: 50 on: account.
		
	self assert: 1 equals: account transactions size.
	self assert: (account transactions includes: deposit1).
! !


!classDefinition: #AccountTransaction category: 'Portfolio-Ejercicio'!
Object subclass: #AccountTransaction
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!AccountTransaction methodsFor: 'value' stamp: 'JDS 10/17/2021 13:06:19'!
impactOn: aBalanceNumber

	self subclassResponsibility ! !

!AccountTransaction methodsFor: 'value' stamp: 'HernanWilkinson 9/12/2011 12:25'!
value 

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'AccountTransaction class' category: 'Portfolio-Ejercicio'!
AccountTransaction class
	instanceVariableNames: ''!

!AccountTransaction class methodsFor: 'instance creation' stamp: 'NR 10/17/2019 03:22:00'!
register: aValue on: account

	| transaction |
	
	transaction := self for: aValue.
	account register: transaction.
		
	^ transaction! !


!classDefinition: #Deposit category: 'Portfolio-Ejercicio'!
AccountTransaction subclass: #Deposit
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Deposit methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:45'!
initializeFor: aValue

	value := aValue ! !


!Deposit methodsFor: 'value' stamp: 'JDS 10/17/2021 13:04:57'!
impactOn: aBalanceNumber

	^ aBalanceNumber + self value! !

!Deposit methodsFor: 'value' stamp: 'HernanWilkinson 7/13/2011 18:38'!
value

	^ value! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Deposit class' category: 'Portfolio-Ejercicio'!
Deposit class
	instanceVariableNames: ''!

!Deposit class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:38'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #Withdraw category: 'Portfolio-Ejercicio'!
AccountTransaction subclass: #Withdraw
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Withdraw methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:46'!
initializeFor: aValue

	value := aValue ! !


!Withdraw methodsFor: 'value' stamp: 'JDS 10/17/2021 13:05:07'!
impactOn: aBalanceNumber

	^ aBalanceNumber - self value! !

!Withdraw methodsFor: 'value' stamp: 'HernanWilkinson 7/13/2011 18:33'!
value

	^ value! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Withdraw class' category: 'Portfolio-Ejercicio'!
Withdraw class
	instanceVariableNames: ''!

!Withdraw class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:33'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #BankingInstrument category: 'Portfolio-Ejercicio'!
Object subclass: #BankingInstrument
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!BankingInstrument methodsFor: 'value' stamp: 'JDS 10/17/2021 17:34:08'!
balance

	self subclassResponsibility ! !


!BankingInstrument methodsFor: 'accessing' stamp: 'JDS 10/17/2021 17:35:04'!
allBankingInstruments

	self subclassResponsibility ! !


!BankingInstrument methodsFor: 'private' stamp: 'JDS 10/17/2021 17:37:35'!
addParent: aPortfolio

	self subclassResponsibility ! !


!classDefinition: #Portfolio category: 'Portfolio-Ejercicio'!
BankingInstrument subclass: #Portfolio
	instanceVariableNames: 'contents parents'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Portfolio methodsFor: 'value' stamp: 'JDS 10/8/2021 01:34:04'!
balance

	^ contents sum: [:anAccount | anAccount balance ] ifEmpty: [0]! !


!Portfolio methodsFor: 'initialization' stamp: 'JDS 10/17/2021 17:19:34'!
initialize
	
	contents _ OrderedCollection new.
	parents _ OrderedCollection new.
! !


!Portfolio methodsFor: 'error' stamp: 'JDS 10/17/2021 15:28:20'!
errorABankingInstrumentIsAlreadyAdded

	^ self error: self class aBankingInstrumentIsAlreadyAddedErrorDescription! !

!Portfolio methodsFor: 'error' stamp: 'JDS 10/17/2021 14:49:57'!
errorPortfolioHasAnAlreadyRegisteredAccount
	
	 ^ self error: self class portfolioHasAnAlreadyRegisteredBankingInstrumentDescription! !

!Portfolio methodsFor: 'error' stamp: 'JDS 10/10/2021 16:37:15'!
errorPortfolioIsRegisteredToItself

 ^ self error: self class portfolioIsRegisteredToItselfErrorDescription! !


!Portfolio methodsFor: 'testing' stamp: 'AE 10/12/2021 02:54:34'!
hasTransaction: aTransaction
	
	^self transactions includes: aTransaction! !

!Portfolio methodsFor: 'testing' stamp: 'JDS 10/11/2021 21:03:21'!
includes: aReceptiveAccount 
	
	^self allBankingInstruments includes: aReceptiveAccount ! !

!Portfolio methodsFor: 'testing' stamp: 'JDS 10/18/2021 15:21:39'!
isRoot

	^parents isEmpty! !


!Portfolio methodsFor: 'private' stamp: 'JDS 10/17/2021 16:39:30'!
addBankingInstrument: aBankingInstrument
		
	self assertPortfolioIsNotIncludedToItSelf: aBankingInstrument .
	
	self assertPortfolioIsNotIncludedDirectly: aBankingInstrument.
	
	self assertPortfolioHasNoCommonBankInstrumentWithChilds: aBankingInstrument.
	
	self assertPortfolioHasNoCommonBankInstrumentWithParents: aBankingInstrument.
	
	contents add: aBankingInstrument. 
	aBankingInstrument addParent: self.
! !

!Portfolio methodsFor: 'private' stamp: 'JDS 10/17/2021 16:18:21'!
addParent: aPortfolio

	parents add: aPortfolio.
	! !


!Portfolio methodsFor: 'accessing' stamp: 'JDS 10/11/2021 21:03:21'!
allBankingInstruments
	
	| allAccounts |
	allAccounts := OrderedCollection new.
	
	allAccounts addAll: contents.
	
	contents do: [ :portfolio | allAccounts addAll: portfolio allBankingInstruments ].
	
	^allAccounts.
	
	! !

!Portfolio methodsFor: 'accessing' stamp: 'JDS 10/17/2021 17:04:10'!
allParentPortfolios
	
	| allPortfolios |
	allPortfolios := OrderedCollection new.
	
	allPortfolios addAll: parents.
	
	parents do: [ :aPortfolio | allPortfolios addAll: aPortfolio allParentPortfolios ].
	
	^allPortfolios.
	
	! !

!Portfolio methodsFor: 'accessing' stamp: 'JDS 10/18/2021 15:20:17'!
allRootPortfolios

	^self allParentPortfolios select: [ :aPortfolio | aPortfolio isRoot ]! !

!Portfolio methodsFor: 'accessing' stamp: 'AE 10/12/2021 02:50:36'!
transactions
	
	| allTransactions |
	allTransactions := OrderedCollection new.
	
	contents do: [ :portfolio | allTransactions addAll: portfolio transactions ].
	
	^allTransactions
	
	! !


!Portfolio methodsFor: 'asserting' stamp: 'JDS 10/17/2021 16:10:10'!
assertPortfolioHasNoCommonBankInstrumentWithChilds: aBankingInstrument
		
	(self allBankingInstruments includesAnyOf: aBankingInstrument allBankingInstruments)
		ifTrue: [^self errorPortfolioHasAnAlreadyRegisteredAccount ].
			! !

!Portfolio methodsFor: 'asserting' stamp: 'JDS 10/18/2021 15:23:27'!
assertPortfolioHasNoCommonBankInstrumentWithParents: aBankingInstrument
	
	self allRootPortfolios do: [ :aPortfolio | 	
		aPortfolio assertPortfolioHasNoCommonBankInstrumentWithChilds: aBankingInstrument ].
			! !

!Portfolio methodsFor: 'asserting' stamp: 'JDS 10/17/2021 16:10:40'!
assertPortfolioIsNotIncludedDirectly: aBankingInstrument

	(self allBankingInstruments includes: aBankingInstrument )
			ifTrue: [^self errorABankingInstrumentIsAlreadyAdded ].
			! !

!Portfolio methodsFor: 'asserting' stamp: 'JDS 10/17/2021 15:52:47'!
assertPortfolioIsNotIncludedToItSelf: aBankingInstrument

	(self = aBankingInstrument) 
		ifTrue: [ ^self errorPortfolioIsRegisteredToItself ].	
			! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Portfolio class' category: 'Portfolio-Ejercicio'!
Portfolio class
	instanceVariableNames: 'bank'!

!Portfolio class methodsFor: 'error description' stamp: 'JDS 10/17/2021 17:11:07'!
aBankingInstrumentIsAlreadyAddedErrorDescription

	^ 'The account or portfolio is already added'! !

!Portfolio class methodsFor: 'error description' stamp: 'JDS 10/17/2021 14:49:57'!
portfolioHasAnAlreadyRegisteredBankingInstrumentDescription

	^'The portfolio you are trying to register has an account or portfolio that already is registered on this portfolio.'! !

!Portfolio class methodsFor: 'error description' stamp: 'JDS 10/10/2021 16:33:09'!
portfolioIsRegisteredToItselfErrorDescription

	^'Can not register to myself'! !


!classDefinition: #ReceptiveAccount category: 'Portfolio-Ejercicio'!
BankingInstrument subclass: #ReceptiveAccount
	instanceVariableNames: 'transactions'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!ReceptiveAccount methodsFor: 'initialization' stamp: 'JDS 10/17/2021 17:15:12'!
initialize

	transactions := OrderedCollection new.
	! !


!ReceptiveAccount methodsFor: 'private' stamp: 'JDS 10/17/2021 19:05:49'!
addParent: aPortfolio

	^self

	! !


!ReceptiveAccount methodsFor: 'transactions' stamp: 'JDS 10/8/2021 19:05:44'!
register: aTransaction

	transactions add: aTransaction 
! !

!ReceptiveAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:37'!
transactions 

	^ transactions copy! !


!ReceptiveAccount methodsFor: 'testing' stamp: 'NR 10/17/2019 03:28:43'!
hasRegistered: aTransaction

	^ transactions includes: aTransaction 
! !


!ReceptiveAccount methodsFor: 'value' stamp: 'JDS 10/17/2021 13:04:09'!
balance

	| acumulatedBalance |
	
	acumulatedBalance _ 0.
	transactions do: [ :aTransaction | 
		acumulatedBalance _ aTransaction impactOn: acumulatedBalance ].
	^ acumulatedBalance! !


!ReceptiveAccount methodsFor: 'accessing' stamp: 'JDS 10/17/2021 14:53:01'!
allBankingInstruments

	^ OrderedCollection with: self! !
