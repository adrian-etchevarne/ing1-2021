!classDefinition: #CantSuspend category: 'CodigoRepetido-Ejercicio'!
Error subclass: #CantSuspend
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #NotFound category: 'CodigoRepetido-Ejercicio'!
Error subclass: #NotFound
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #CustomerBookTest category: 'CodigoRepetido-Ejercicio'!
TestCase subclass: #CustomerBookTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBookTest methodsFor: 'testing-auxiliar' stamp: 'jds 9/8/2021 23:15:25'!
addAndSuspend: aCostumer into: aCustomerBook

	aCustomerBook addCustomerNamed: aCostumer.
	aCustomerBook suspendCustomerNamed: aCostumer.
	^aCustomerBook
! !

!CustomerBookTest methodsFor: 'testing-auxiliar' stamp: 'jds 9/8/2021 23:21:01'!
initializeAddAndSuspendCostumer: customerBook withARestingNumberOfCustomers: aRestingNumberOfCustomers	

	| paulMcCartney aPostConditionClosure |
	
	paulMcCartney := 'Paul McCartney'.
	self addAndSuspend: paulMcCartney into: customerBook.

	aPostConditionClosure :=[ 
		self assert: 0 equals: customerBook numberOfActiveCustomers.
		self assert: aRestingNumberOfCustomers equals: customerBook numberOfSuspendedCustomers.
		self assert: aRestingNumberOfCustomers equals: customerBook numberOfCustomers.].
	^aPostConditionClosure
! !

!CustomerBookTest methodsFor: 'testing-auxiliar' stamp: 'jds 9/8/2021 22:55:31'!
initializeAnInvalidCostumerTest: aCustomerBook
	| aPostConditionClosure johnLennon |
	johnLennon := 'John Lennon'.
	aCustomerBook addCustomerNamed: johnLennon.
	aPostConditionClosure := [
			self assert: aCustomerBook numberOfCustomers = 1.
			self assert: (aCustomerBook includesCustomerNamed: johnLennon) ].
	^aPostConditionClosure
! !


!CustomerBookTest methodsFor: 'testing' stamp: 'jds 9/9/2021 15:42:32'!
test01AddingCustomerShouldNotTakeMoreThan50Milliseconds

	| customerBook aClosure |
	
	customerBook := CustomerBook new.
	
	aClosure := [customerBook addCustomerNamed: 'John Lennon'.].
	
	self should: aClosure notTakeMoreThan: 50 milliSeconds 
	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'jds 9/9/2021 15:46:07'!
test02RemovingCustomerShouldNotTakeMoreThan100Milliseconds

	| customerBook paulMcCartney aClosure |
	
	customerBook := CustomerBook new.

	paulMcCartney := 'Paul McCartney'.	
	customerBook addCustomerNamed: paulMcCartney.
	aClosure := [customerBook removeCustomerNamed: paulMcCartney].
	  
	self should: aClosure notTakeMoreThan: 100 milliSeconds
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'jds 9/9/2021 15:59:31'!
test03CanNotAddACustomerWithEmptyName 

	| customerBook testingCode |
			
	customerBook := CustomerBook new.
	testingCode := [ customerBook addCustomerNamed: '' ].

	self should: testingCode raise: Error withMessageText: CustomerBook customerCanNotBeEmptyErrorMessage.
	self assert: customerBook isEmpty.
	
	! !

!CustomerBookTest methodsFor: 'testing' stamp: 'jds 9/9/2021 15:54:41'!
test04CanNotRemoveAnInvalidCustomer
	
	| customerBook aPostConditionClosure testingCode |
			
	customerBook := CustomerBook new.
	
	aPostConditionClosure := self initializeAnInvalidCostumerTest: customerBook.
	testingCode := [customerBook removeCustomerNamed: 'Paul McCartney'.].
	
	self should: testingCode raise: NotFound.
	aPostConditionClosure value.
		
			! !

!CustomerBookTest methodsFor: 'testing' stamp: 'jds 9/8/2021 23:28:44'!
test05SuspendingACustomerShouldNotRemoveItFromCustomerBook

	| customerBook aPostConditionClosure |
	
	customerBook := CustomerBook new.
	
	aPostConditionClosure := self initializeAddAndSuspendCostumer: customerBook withARestingNumberOfCustomers: 1. 

	aPostConditionClosure value.	
	self assert: (customerBook includesCustomerNamed: 'Paul McCartney').
	

	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'jds 9/8/2021 23:22:20'!
test06RemovingASuspendedCustomerShouldRemoveItFromCustomerBook

	| customerBook paulMcCartney aPostConditionClosure |
	
	customerBook := CustomerBook new.

	paulMcCartney := 'Paul McCartney'.
	aPostConditionClosure := self initializeAddAndSuspendCostumer: customerBook withARestingNumberOfCustomers: 0. 
	customerBook removeCustomerNamed: paulMcCartney.

	aPostConditionClosure value.
	self deny: (customerBook includesCustomerNamed: paulMcCartney).! !

!CustomerBookTest methodsFor: 'testing' stamp: 'jds 9/9/2021 16:01:05'!
test07CanNotSuspendAnInvalidCustomer
	
	| customerBook aPostConditionClosure testingCode |
			
	customerBook := CustomerBook new.
	
	aPostConditionClosure := self initializeAnInvalidCostumerTest: customerBook.		
	testingCode := [customerBook suspendCustomerNamed: 'George Harrison'.].
	
	self should: testingCode  raise: CantSuspend.	
	aPostConditionClosure value! !

!CustomerBookTest methodsFor: 'testing' stamp: 'jds 9/9/2021 16:02:08'!
test08CanNotSuspendAnAlreadySuspendedCustomer
	
	| customerBook johnLennon testingCode |
	
	customerBook := CustomerBook new.
			
	johnLennon := 'John Lennon'.
	self addAndSuspend: johnLennon into: customerBook.
	testingCode := [customerBook suspendCustomerNamed: johnLennon.].
	
	self should: testingCode raise: CantSuspend.
	self assert: customerBook numberOfCustomers = 1.
	self assert: (customerBook includesCustomerNamed: johnLennon).
! !


!classDefinition: #CustomerBook category: 'CodigoRepetido-Ejercicio'!
Object subclass: #CustomerBook
	instanceVariableNames: 'suspended active'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
includesCustomerNamed: aName

	^(active includes: aName) or: [ suspended includes: aName ]! !

!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
isEmpty
	
	^active isEmpty and: [ suspended isEmpty ]! !


!CustomerBook methodsFor: 'initialization' stamp: 'NR 9/17/2020 07:23:04'!
initialize

	active := OrderedCollection new.
	suspended:= OrderedCollection new.! !


!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
addCustomerNamed: aName

	aName isEmpty ifTrue: [ self signalCustomerNameCannotBeEmpty ].
	((active includes: aName) or: [suspended includes: aName]) ifTrue: [ self signalCustomerAlreadyExists ].
	
	active add: aName ! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
numberOfActiveCustomers
	
	^active size! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
numberOfCustomers
	
	^active size + suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 9/19/2018 17:36:09'!
numberOfSuspendedCustomers
	
	^suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'jds 9/7/2021 22:51:37'!
removeCustomerNamed: aName 

	active remove: aName ifAbsent: [suspended remove: aName ifAbsent: [^NotFound signal]].
	^ aName
! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:52'!
signalCustomerAlreadyExists 

	self error: self class customerAlreadyExistsErrorMessage! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:51'!
signalCustomerNameCannotBeEmpty 

	self error: self class customerCanNotBeEmptyErrorMessage ! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
suspendCustomerNamed: aName 
	
	(active includes: aName) ifFalse: [^CantSuspend signal].
	
	active remove: aName.
	
	suspended add: aName
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerBook class' category: 'CodigoRepetido-Ejercicio'!
CustomerBook class
	instanceVariableNames: ''!

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 4/12/2021 16:39:13'!
customerAlreadyExistsErrorMessage

	^'customer already exists!!!!!!'! !

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 4/12/2021 16:39:09'!
customerCanNotBeEmptyErrorMessage

	^'customer name cannot be empty!!!!!!'! !
