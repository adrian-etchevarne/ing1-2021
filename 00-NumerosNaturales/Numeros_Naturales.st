!classDefinition: #I category: 'N�meros Naturales'!
DenotativeObject subclass: #I
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'N�meros Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'I class' category: 'N�meros Naturales'!
I class
	instanceVariableNames: ''!

!I class methodsFor: 'error' stamp: 'jds 8/29/2021 12:45:11'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor

	^'descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor'! !

!I class methodsFor: 'error' stamp: 'jds 8/29/2021 12:33:22'!
descripcionDeErrorDeNumerosNegativosNoSoportados

	^'descripcionDeErrorDeNumerosNegativosNoSoportados'! !


!I class methodsFor: 'implementacion' stamp: 'jds 8/26/2021 20:42:57'!
next

	^II! !

!I class methodsFor: 'implementacion' stamp: 'jds 8/29/2021 13:24:12'!
restarRec: aNaturalNumber cociente: aNaturalNumberQuotient
	
	self = aNaturalNumber ifTrue: [^aNaturalNumberQuotient].
	^aNaturalNumberQuotient previous
! !

!I class methodsFor: 'implementacion' stamp: 'jds 8/26/2021 23:06:54'!
restarseloA: aNaturalNumber

	^aNaturalNumber previous! !


!I class methodsFor: 'aritm�ticas' stamp: 'jds 8/27/2021 00:05:39'!
* aNaturalNumber

	^aNaturalNumber! !

!I class methodsFor: 'aritm�ticas' stamp: 'jds 8/26/2021 21:13:57'!
+ aNaturalNumber

	^aNaturalNumber next

! !

!I class methodsFor: 'aritm�ticas' stamp: 'jds 8/29/2021 12:32:52'!
- aNaturalNumber

	^self error: self descripcionDeErrorDeNumerosNegativosNoSoportados! !

!I class methodsFor: 'aritm�ticas' stamp: 'jds 8/29/2021 12:44:56'!
/ aNaturalNumber

	aNaturalNumber = self ifFalse: [self error: self descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor].
	! !


!I class methodsFor: 'orden' stamp: 'jds 8/28/2021 14:40:39'!
< aNaturalNumber

	^(aNaturalNumber = I) not! !


!classDefinition: #II category: 'N�meros Naturales'!
DenotativeObject subclass: #II
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'N�meros Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'II class' category: 'N�meros Naturales'!
II class
	instanceVariableNames: 'next previous'!

!II class methodsFor: 'implementacion' stamp: 'jds 8/26/2021 21:23:54'!
next

	next ifNotNil: [^next].
	next _ II createChildNamed: self name, 'I'.
	next previous: self.
	^next! !

!II class methodsFor: 'implementacion' stamp: 'jds 8/26/2021 21:24:45'!
previous

	^previous! !

!II class methodsFor: 'implementacion' stamp: 'jds 8/28/2021 14:13:51'!
previous: aNaturalNumber

	previous _ aNaturalNumber ! !

!II class methodsFor: 'implementacion' stamp: 'jds 8/28/2021 16:08:01'!
restarRec: aNaturalNumber cociente: aNaturalNumberQuotient

	self = aNaturalNumber ifTrue: [^aNaturalNumberQuotient].
	self < aNaturalNumber ifTrue: [^aNaturalNumberQuotient previous ].
	^(self - aNaturalNumber ) restarRec: aNaturalNumber cociente: aNaturalNumberQuotient next
	! !

!II class methodsFor: 'implementacion' stamp: 'jds 8/26/2021 23:48:41'!
restarseloA: aNaturalNumber

	^aNaturalNumber previous - self previous! !


!II class methodsFor: 'aritm�ticas' stamp: 'jds 8/28/2021 16:29:07'!
* aNaturalNumber

	^ self previous * aNaturalNumber + aNaturalNumber ! !

!II class methodsFor: 'aritm�ticas' stamp: 'jds 8/26/2021 21:22:43'!
+ aNaturalNumber

	^self previous + aNaturalNumber next! !

!II class methodsFor: 'aritm�ticas' stamp: 'jds 8/27/2021 20:37:22'!
- aNaturalNumber

	^aNaturalNumber restarseloA: self! !

!II class methodsFor: 'aritm�ticas' stamp: 'jds 8/29/2021 12:35:09'!
/ aNaturalNumber

	self < aNaturalNumber ifTrue: [^self error: self descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor].
	^self restarRec: aNaturalNumber cociente: I 
	
	
	! !


!II class methodsFor: 'orden' stamp: 'jds 8/28/2021 15:56:54'!
< aNaturalNumber

	[aNaturalNumber - self] ifError: [^false].
	^true! !


!II class methodsFor: 'error' stamp: 'jds 8/29/2021 12:46:19'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor

	^'descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor'! !


!II class methodsFor: 'as yet unclassified' stamp: 'jds 8/29/2021 13:30:08'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := III.
	previous := I.! !


!classDefinition: #III category: 'N�meros Naturales'!
II subclass: #III
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'N�meros Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'III class' category: 'N�meros Naturales'!
III class
	instanceVariableNames: ''!

!III class methodsFor: 'as yet unclassified' stamp: 'jds 8/29/2021 13:30:08'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIII.
	previous := II.! !


!classDefinition: #IIII category: 'N�meros Naturales'!
II subclass: #IIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'N�meros Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIII class' category: 'N�meros Naturales'!
IIII class
	instanceVariableNames: ''!

!IIII class methodsFor: 'as yet unclassified' stamp: 'jds 8/29/2021 13:30:08'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIIII.
	previous := III.! !


!classDefinition: #IIIII category: 'N�meros Naturales'!
II subclass: #IIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'N�meros Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIII class' category: 'N�meros Naturales'!
IIIII class
	instanceVariableNames: ''!

!IIIII class methodsFor: 'as yet unclassified' stamp: 'jds 8/29/2021 13:30:08'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIIIII.
	previous := IIII.! !


!classDefinition: #IIIIII category: 'N�meros Naturales'!
II subclass: #IIIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'N�meros Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIIII class' category: 'N�meros Naturales'!
IIIIII class
	instanceVariableNames: ''!

!IIIIII class methodsFor: 'as yet unclassified' stamp: 'jds 8/29/2021 13:30:08'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIIIIII.
	previous := IIIII.! !


!classDefinition: #IIIIIII category: 'N�meros Naturales'!
II subclass: #IIIIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'N�meros Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIIIII class' category: 'N�meros Naturales'!
IIIIIII class
	instanceVariableNames: ''!

!IIIIIII class methodsFor: 'as yet unclassified' stamp: 'jds 8/29/2021 13:30:08'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIIIIIII.
	previous := IIIIII.! !


!classDefinition: #IIIIIIII category: 'N�meros Naturales'!
II subclass: #IIIIIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'N�meros Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIIIIII class' category: 'N�meros Naturales'!
IIIIIIII class
	instanceVariableNames: ''!

!IIIIIIII class methodsFor: 'as yet unclassified' stamp: 'jds 8/29/2021 13:30:08'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIIIIIIII.
	previous := IIIIIII.! !


!classDefinition: #IIIIIIIII category: 'N�meros Naturales'!
II subclass: #IIIIIIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'N�meros Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIIIIIII class' category: 'N�meros Naturales'!
IIIIIIIII class
	instanceVariableNames: ''!

!IIIIIIIII class methodsFor: 'as yet unclassified' stamp: 'jds 8/29/2021 13:30:08'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIIIIIIIII.
	previous := IIIIIIII.! !


!classDefinition: #IIIIIIIIII category: 'N�meros Naturales'!
II subclass: #IIIIIIIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'N�meros Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIIIIIIII class' category: 'N�meros Naturales'!
IIIIIIIIII class
	instanceVariableNames: ''!

!IIIIIIIIII class methodsFor: 'as yet unclassified' stamp: 'jds 8/29/2021 13:30:08'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIIIIIIIIII.
	previous := IIIIIIIII.! !


!classDefinition: #IIIIIIIIIII category: 'N�meros Naturales'!
II subclass: #IIIIIIIIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'N�meros Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIIIIIIIII class' category: 'N�meros Naturales'!
IIIIIIIIIII class
	instanceVariableNames: ''!

!IIIIIIIIIII class methodsFor: 'as yet unclassified' stamp: 'jds 8/29/2021 13:30:09'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIIIIIIIIIII.
	previous := IIIIIIIIII.! !


!classDefinition: #IIIIIIIIIIII category: 'N�meros Naturales'!
II subclass: #IIIIIIIIIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'N�meros Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIIIIIIIIII class' category: 'N�meros Naturales'!
IIIIIIIIIIII class
	instanceVariableNames: ''!

!IIIIIIIIIIII class methodsFor: 'as yet unclassified' stamp: 'jds 8/29/2021 13:30:09'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIIIIIIIIIIII.
	previous := IIIIIIIIIII.! !


!classDefinition: #IIIIIIIIIIIII category: 'N�meros Naturales'!
II subclass: #IIIIIIIIIIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'N�meros Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIIIIIIIIIII class' category: 'N�meros Naturales'!
IIIIIIIIIIIII class
	instanceVariableNames: ''!

!IIIIIIIIIIIII class methodsFor: 'as yet unclassified' stamp: 'jds 8/29/2021 13:30:09'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIIIIIIIIIIIII.
	previous := IIIIIIIIIIII.! !


!classDefinition: #IIIIIIIIIIIIII category: 'N�meros Naturales'!
II subclass: #IIIIIIIIIIIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'N�meros Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIIIIIIIIIIII class' category: 'N�meros Naturales'!
IIIIIIIIIIIIII class
	instanceVariableNames: ''!

!IIIIIIIIIIIIII class methodsFor: 'as yet unclassified' stamp: 'jds 8/29/2021 13:30:09'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIIIIIIIIIIIIII.
	previous := IIIIIIIIIIIII.! !


!classDefinition: #IIIIIIIIIIIIIII category: 'N�meros Naturales'!
II subclass: #IIIIIIIIIIIIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'N�meros Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIIIIIIIIIIIII class' category: 'N�meros Naturales'!
IIIIIIIIIIIIIII class
	instanceVariableNames: ''!

!IIIIIIIIIIIIIII class methodsFor: 'as yet unclassified' stamp: 'jds 8/29/2021 13:30:09'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIIIIIIIIIIIIIII.
	previous := IIIIIIIIIIIIII.! !


!classDefinition: #IIIIIIIIIIIIIIII category: 'N�meros Naturales'!
II subclass: #IIIIIIIIIIIIIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'N�meros Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIIIIIIIIIIIIII class' category: 'N�meros Naturales'!
IIIIIIIIIIIIIIII class
	instanceVariableNames: ''!

!IIIIIIIIIIIIIIII class methodsFor: 'as yet unclassified' stamp: 'jds 8/29/2021 13:30:09'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := nil.
	previous := IIIIIIIIIIIIIII.! !

II initializeAfterFileIn!
III initializeAfterFileIn!
IIII initializeAfterFileIn!
IIIII initializeAfterFileIn!
IIIIII initializeAfterFileIn!
IIIIIII initializeAfterFileIn!
IIIIIIII initializeAfterFileIn!
IIIIIIIII initializeAfterFileIn!
IIIIIIIIII initializeAfterFileIn!
IIIIIIIIIII initializeAfterFileIn!
IIIIIIIIIIII initializeAfterFileIn!
IIIIIIIIIIIII initializeAfterFileIn!
IIIIIIIIIIIIII initializeAfterFileIn!
IIIIIIIIIIIIIII initializeAfterFileIn!
IIIIIIIIIIIIIIII initializeAfterFileIn!