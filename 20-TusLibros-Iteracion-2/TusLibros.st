!classDefinition: #TransactionRejected category: 'TusLibros'!
Error subclass: #TransactionRejected
	instanceVariableNames: 'reason'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!TransactionRejected methodsFor: 'initialization' stamp: 'AE 11/4/2021 16:43:32'!
initializeReason: aString 
	
	self messageText: aString.
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TransactionRejected class' category: 'TusLibros'!
TransactionRejected class
	instanceVariableNames: ''!

!TransactionRejected class methodsFor: 'instance creation' stamp: 'AE 11/4/2021 16:27:40'!
reason: aString 
	
	^self new initializeReason: aString ! !


!classDefinition: #TusLibrosTest category: 'TusLibros'!
TestCase subclass: #TusLibrosTest
	instanceVariableNames: 'itemSellByTheStore itemSellByTheStorePrice defaultCatalog validCreditCard defaultSalesBook expiredCreditCard itemNotSellByTheStore defaultMerchantProcessor'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!TusLibrosTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:08'!
test01NewCartsAreCreatedEmpty

	self assert: self createCart isEmpty! !

!TusLibrosTest methodsFor: 'tests' stamp: 'AE 11/4/2021 11:45:39'!
test02CanNotAddItemsThatDoNotBelongToStore

	| cart |
	
	cart := self createCart.
	
	self 
		should: [ cart add: itemNotSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!TusLibrosTest methodsFor: 'tests' stamp: 'AE 11/4/2021 11:38:01'!
test03AfterAddingAnItemTheCartIsNotEmptyAnymore

	| cart |
	
	cart := self createCart.
	
	cart add: itemSellByTheStore.
	self deny: cart isEmpty ! !

!TusLibrosTest methodsFor: 'tests' stamp: 'AE 11/4/2021 11:38:15'!
test04CanNotAddNonPositiveNumberOfItems

	| cart |
	
	cart := self createCart.
	
	self 
		should: [cart add: 0 of:  itemSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidQuantityErrorMessage.
			self assert: cart isEmpty ]! !

!TusLibrosTest methodsFor: 'tests' stamp: 'AE 11/4/2021 11:45:47'!
test05CanNotAddMoreThanOneItemNotSellByTheStore

	| cart |
	
	cart := self createCart.
	
	self 
		should: [cart add: 2 of: itemNotSellByTheStore  ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!TusLibrosTest methodsFor: 'tests' stamp: 'AE 11/4/2021 11:38:27'!
test06CartRemembersAddedItems

	| cart |
	
	cart := self createCart.
	
	cart add: itemSellByTheStore.
	self assert: (cart includes: itemSellByTheStore)! !

!TusLibrosTest methodsFor: 'tests' stamp: 'AE 11/4/2021 11:38:34'!
test07CartDoesNotHoldNotAddedItems

	| cart |
	
	cart := self createCart.
	
	self deny: (cart includes: itemSellByTheStore)! !

!TusLibrosTest methodsFor: 'tests' stamp: 'AE 11/4/2021 11:38:43'!
test08CartRemembersTheNumberOfAddedItems

	| cart |
	
	cart := self createCart.
	
	cart add: 2 of: itemSellByTheStore.
	self assert: (cart occurrencesOf: itemSellByTheStore) = 2! !

!TusLibrosTest methodsFor: 'tests' stamp: 'AE 11/4/2021 16:01:21'!
test09CheckOutAnEmptyCartThrowsAnException

	| cart cashier |
	
	cart := self createCart.
	
	cashier _ Cashier 
		withCart: cart 
		charging: validCreditCard 
		on: self today 
		registerOn: defaultSalesBook 
		processOn: defaultMerchantProcessor .
	
	self should: 
			[cashier checkOut] 
		raise: Error - MessageNotUnderstood 
		withMessageText: Cashier emptyCartCheckOutErrorMessage .! !

!TusLibrosTest methodsFor: 'tests' stamp: 'AE 11/4/2021 15:58:33'!
test10CalculatedTotalIsCorrect

	| cart cashier totalAmount |
	
	cart := self createCart.
	
	cart add: 2 of: itemSellByTheStore.
	cashier _ Cashier 
				withCart: cart 
				charging: validCreditCard 
				on: self today 
				registerOn: defaultSalesBook 
				processOn: defaultMerchantProcessor .
	
	totalAmount _ cashier checkOut. 
	
	self assert: totalAmount equals: (itemSellByTheStorePrice * 2).
	! !

!TusLibrosTest methodsFor: 'tests' stamp: 'AE 11/4/2021 16:03:12'!
test11CheckOutACartWithOneItemGivesTheRightPrice

	| cart cashier totalAmount |
	
	cart := self createCart.
	
	cart add: 2 of: itemSellByTheStore.
	cashier _ Cashier 
			withCart: cart 
			charging: (validCreditCard) 
			on: self today 
			registerOn: defaultSalesBook
			processOn: defaultMerchantProcessor .
	
	totalAmount _ cashier checkOut. 
	
	self assert: totalAmount equals: itemSellByTheStorePrice * 2.
	! !

!TusLibrosTest methodsFor: 'tests' stamp: 'AE 11/4/2021 15:39:48'!
test12CanNotCheckoutWithAnExpiredCreditCard

	| cart |
	
	cart := self createCart.
	
	cart add: 2 of: itemSellByTheStore.
	
	self
		should: [Cashier 
					withCart: cart 
					charging: expiredCreditCard 
					on: self today 
					registerOn: defaultSalesBook processOn: merchanProcessor. ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = Cashier expiredCreditCardErrorMessage.
			self assert: defaultSalesBook isEmpty.
			]
	
	! !

!TusLibrosTest methodsFor: 'tests' stamp: 'AE 11/4/2021 16:03:42'!
test13CheckoutRegisterASale

	| cart salesBook cashier |
	
	cart := self createCart.
	
	salesBook _ defaultSalesBook .
	cart add: 2 of: itemSellByTheStore.
	
	cashier _ Cashier
				withCart: cart 
				charging: validCreditCard 
				on: self today 
				registerOn: salesBook 
				processOn: defaultMerchantProcessor .

	cashier checkOut.
		
	self assert: salesBook notEmpty.
	self assert: salesBook first = (itemSellByTheStorePrice * 2) 
	
	! !

!TusLibrosTest methodsFor: 'tests' stamp: 'AE 11/4/2021 14:20:57'!
test14CreditCardInvalidFormatNumberRaiseError


	| invalidCCNumber |
	
	invalidCCNumber _ '7'.
	
	self should:
		[ CreditCard 
			of: 'Roberto'
			number: invalidCCNumber  
			expirationMonth: #August
			expirationYear: 2000.]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = CreditCard invalidCreditCardNumberErrorMessage.
			]

	! !

!TusLibrosTest methodsFor: 'tests' stamp: 'AE 11/4/2021 15:25:10'!
test15CreditCardEmptyNameRaiseError


	| invalidName validCCNumber |
	
	validCCNumber _ '0123456789012345'.
	invalidName _ ''.
	
	self should:
		[ CreditCard 
			of: invalidName
			number: validCCNumber  
			expirationMonth: #August 
			expirationYear: 2000.]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = CreditCard invalidOwnerName .
			]

	! !

!TusLibrosTest methodsFor: 'tests' stamp: 'AE 11/4/2021 14:23:35'!
test16CreditCardExpiresOnMonth


	| validCCNumber creditCard expirationDate todayTestDay |	
	validCCNumber _ '0123456789012345'.
	
	todayTestDay _ Date newDay: 15 month: #June year: 2000.

	expirationDate _ Date newDay: 20 month: #June year: 2000.
		
	creditCard _ CreditCard 
			of: 'Roberto'
			number: validCCNumber  
			expirationMonth: expirationDate monthName 
			expirationYear: expirationDate yearNumber.

	self assert: (creditCard isExpiredOn: todayTestDay).
	
	! !

!TusLibrosTest methodsFor: 'tests' stamp: 'AE 11/4/2021 15:24:49'!
test17CreditCardNameTooLongRaiseError


	| invalidName validCCNumber |
	
	validCCNumber _ '0123456789012345'.
	invalidName _ 'Manuel Jos� Joaqu�n del Coraz�n de Jes�s Belgrano y Gonz�lez'.
	
	self should:
		[ CreditCard 
			of: invalidName
			number: validCCNumber  
			expirationMonth: #August 
			expirationYear: 2000.]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = CreditCard invalidOwnerName .
			]

	! !

!TusLibrosTest methodsFor: 'tests' stamp: 'AE 11/4/2021 16:44:59'!
test18MerchantProcessorRejectedTheTransactionAndRaiseException

	| cart salesBook cashier merchantProcessor |
	
	cart := self createCart.
	
	salesBook _ defaultSalesBook .
	cart add: 2 of: itemSellByTheStore.

	merchantProcessor _ MerchantProcessorSimulatorAlwaysReject new.
		
	cashier _ Cashier
				withCart: cart 
				charging: validCreditCard 
				on: self today 
				registerOn: salesBook 
				processOn: merchantProcessor.

	self should:
		[ cashier checkOut.] 		
		raise: TransactionRejected 

	withExceptionDo: [ :anError |
			self assert: defaultSalesBook isEmpty.
			self assert: (anError messageText notEmpty )
		]
! !


!TusLibrosTest methodsFor: 'support' stamp: 'AE 11/4/2021 11:37:45'!
createCart
	
	^Cart acceptingItemsOf: defaultCatalog! !

!TusLibrosTest methodsFor: 'support' stamp: 'AE 11/4/2021 00:45:45'!
today
	^Date today.
	! !


!TusLibrosTest methodsFor: 'setUp/tearDown' stamp: 'AE 11/4/2021 16:13:21'!
setUp

	| ccNumber |
	itemSellByTheStore _ 'validBook'.
	itemSellByTheStorePrice _ 17.
	
	defaultCatalog _ Dictionary new.
	defaultCatalog add: itemSellByTheStore ->  itemSellByTheStorePrice.
	
	defaultSalesBook _ OrderedCollection new.

	defaultMerchantProcessor _ MerchantProcessorSimulatorAlwaysAccept  new.

	ccNumber _ '0123456789012345'.
	validCreditCard _ CreditCard 
		of: 'Roberta'
		 number: ccNumber 
		expirationMonth: self today monthName 
		expirationYear: self today yearNumber + 1 .
	expiredCreditCard _ CreditCard 
		of: 'Roberto' 
		number: ccNumber 
		expirationMonth: self today monthName 
		expirationYear: self today yearNumber - 1 .
	
	itemNotSellByTheStore _ 'invalidBook'.! !


!classDefinition: #Cart category: 'TusLibros'!
Object subclass: #Cart
	instanceVariableNames: 'catalog items'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidItemErrorMessage
	
	^'Item is not in catalog'! !

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidQuantityErrorMessage
	
	^'Invalid number of items'! !


!Cart methodsFor: 'assertions' stamp: 'AE 11/3/2021 23:39:40'!
assertIsValidItem: anItem

	(catalog includesKey: anItem) ifFalse: [ self error: self invalidItemErrorMessage ]! !

!Cart methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 17:51'!
assertIsValidQuantity: aQuantity

	aQuantity strictlyPositive ifFalse: [ self error: self invalidQuantityErrorMessage ]! !


!Cart methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 17:48'!
initializeAcceptingItemsOf: aCatalog

	catalog := aCatalog.
	items := OrderedCollection new.! !


!Cart methodsFor: 'queries' stamp: 'HernanWilkinson 6/17/2013 17:45'!
occurrencesOf: anItem

	^items occurrencesOf: anItem  ! !


!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
includes: anItem

	^items includes: anItem ! !

!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
isEmpty
	
	^items isEmpty ! !


!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:44'!
add: anItem

	^ self add: 1 of: anItem ! !

!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:51'!
add: aQuantity of: anItem

	self assertIsValidQuantity: aQuantity.
	self assertIsValidItem: anItem.

	1 to: aQuantity do: [ :aNumber | items add: anItem ]! !


!Cart methodsFor: 'as yet unclassified' stamp: 'AE 11/4/2021 00:07:30'!
withItemsDo: aBlock
	
	items do: aBlock.
	! !


!Cart methodsFor: 'accessing' stamp: 'AE 11/4/2021 00:07:52'!
catalog
	^catalog! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: 'TusLibros'!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 17:48'!
acceptingItemsOf: aCatalog

	^self new initializeAcceptingItemsOf: aCatalog ! !


!classDefinition: #Cashier category: 'TusLibros'!
Object subclass: #Cashier
	instanceVariableNames: 'cart salesBook merchantProcessor creditCard'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cashier methodsFor: 'initialization' stamp: 'AE 11/4/2021 16:01:01'!
initializeWith: aCart aCreditCard: charging registerOn: aSalesBook processOn: aMerchantProcessor  

	cart _ aCart.
	
	salesBook _ aSalesBook.

	merchantProcessor _ 	aMerchantProcessor .
	! !


!Cashier methodsFor: 'checkout' stamp: 'AE 11/4/2021 16:29:32'!
checkOut

	| totalAmount |
	totalAmount _ 0.
	
	cart isEmpty ifTrue: [self error: self class emptyCartCheckOutErrorMessage].

	cart withItemsDo: [ :item |  totalAmount _ totalAmount + cart catalog at: item ].
	
	merchantProcessor charge: totalAmount to: creditCard.
	
	salesBook add: totalAmount .
	
	^totalAmount 
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cashier class' category: 'TusLibros'!
Cashier class
	instanceVariableNames: 'cart'!

!Cashier class methodsFor: 'instance creation' stamp: 'AE 11/4/2021 16:01:47'!
withCart: aCart charging: aCreditCard on: aDate registerOn: aSalesBook processOn: aMerchantProcessor  

	 (aCreditCard isExpiredOn: 	aDate) ifTrue: [ self error: self expiredCreditCardErrorMessage  ].
	
	^self basicNew initializeWith: aCart aCreditCard: aCreditCard registerOn: aSalesBook processOn: aMerchantProcessor .
	! !


!Cashier class methodsFor: 'error handling' stamp: 'JDS 11/1/2021 20:02:12'!
emptyCartCheckOutErrorMessage
	
	^ 'Can not checkout from an empty cart'! !

!Cashier class methodsFor: 'error handling' stamp: 'AE 11/4/2021 01:16:21'!
expiredCreditCardErrorMessage
	^'Trying to checkout with an expired Credit card'.
	! !

!Cashier class methodsFor: 'error handling' stamp: 'AE 11/4/2021 16:14:34'!
paymentRejectedErrorMessage
	^'Transaction was denied.'! !



!classDefinition: #CreditCard category: 'TusLibros'!
Object subclass: #CreditCard
	instanceVariableNames: 'ownerName expirationDate ccNumber'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCard methodsFor: 'testing' stamp: 'AE 11/4/2021 15:25:50'!
assertValidName: aName

	((aName isEmpty )
	or: [aName size > 30])
		ifTrue: [ self error: CreditCard invalidOwnerName. ].		
					! !

!CreditCard methodsFor: 'testing' stamp: 'AE 11/4/2021 12:53:19'!
assertValidNumber: aCCNumber

	((aCCNumber size = 16) 
		and: [ aCCNumber allSatisfy: [ :char | char isDigit ] ])
		
		ifFalse: [ self error: CreditCard invalidCreditCardNumberErrorMessage ].
		
					! !

!CreditCard methodsFor: 'testing' stamp: 'AE 11/4/2021 14:18:44'!
isExpiredOn: aDate
	
	^ expirationDate  < aDate .
	! !


!CreditCard methodsFor: 'initialization' stamp: 'AE 11/4/2021 14:20:03'!
initializeOf: aName number: aCCNumber expiresMonth: aMonth expirationYear: expirationYear 
	
	self assertValidNumber: aCCNumber.
	self assertValidName: aName.
	
	ownerName _ aName.
	expirationDate _ (Date newDay: 1 month: aMonth year: expirationYear). 
	
	ccNumber _ aCCNumber.
	
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCard class' category: 'TusLibros'!
CreditCard class
	instanceVariableNames: ''!

!CreditCard class methodsFor: 'class initialization' stamp: 'AE 11/4/2021 14:04:28'!
of: aName number: number expirationMonth: expirationDate expirationYear: expirationYear 

	^self basicNew initializeOf: aName number: number expiresMonth: expirationDate expirationYear: expirationYear.! !


!CreditCard class methodsFor: 'error handling' stamp: 'AE 11/4/2021 12:57:45'!
invalidCreditCardNumberErrorMessage
	^'Credit card number has an invalid format'
	! !

!CreditCard class methodsFor: 'error handling' stamp: 'AE 11/4/2021 15:26:30'!
invalidOwnerName
	^ 'Owner name length is not valid.'
	! !


!classDefinition: #MerchantProcessor category: 'TusLibros'!
Object subclass: #MerchantProcessor
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!


!classDefinition: #MerchantProcessorSimulator category: 'TusLibros'!
MerchantProcessor subclass: #MerchantProcessorSimulator
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!MerchantProcessorSimulator methodsFor: 'payment' stamp: 'AE 11/4/2021 15:55:56'!
charge: anAmount to: aCreditCard
	
	self subclassResponsibility 
	! !


!classDefinition: #MerchantProcessorSimulatorAlwaysAccept category: 'TusLibros'!
MerchantProcessorSimulator subclass: #MerchantProcessorSimulatorAlwaysAccept
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!MerchantProcessorSimulatorAlwaysAccept methodsFor: 'payment' stamp: 'AE 11/4/2021 16:13:33'!
charge: aSmallInteger to: anUndefinedObject 
	! !


!classDefinition: #MerchantProcessorSimulatorAlwaysReject category: 'TusLibros'!
MerchantProcessorSimulator subclass: #MerchantProcessorSimulatorAlwaysReject
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!MerchantProcessorSimulatorAlwaysReject methodsFor: 'payment' stamp: 'AE 11/4/2021 16:39:54'!
charge: anAmount to: aCreditCard
	
	| error |
	
	error _ TransactionRejected reason: 'Simulated reason'.
	error signal.
	 
	! !
