!classDefinition: #AuthenticationClientWindow category: 'TusLibros-Client'!
Panel subclass: #AuthenticationClientWindow
	instanceVariableNames: 'userTextBoxMorph passwordTextBoxMorph'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros-Client'!

!AuthenticationClientWindow methodsFor: 'initialization' stamp: 'AE 11/29/2021 12:09:07'!
initializeWith: aTitle onPort: aPort 

	self titleMorph showButtonsNamed: #( close collapse ).
	self setLabel: aTitle.
	self model: (AuthenticationClientWindowModel onPort: aPort).
	self morphExtent: (self defaultExtent).
	self buildMorphicWindow.
	self openInWorld.
					
	self model when: #createCartSuccessful send: #openShoppingWindow to: self.
	self model when: #invalidLogin send: #loginNotValidWindow to: self.
	! !


!AuthenticationClientWindow methodsFor: 'next windows' stamp: 'AE 11/29/2021 12:10:26'!
loginNotValidWindow

	self notify: 'Login incorrect'.
! !

!AuthenticationClientWindow methodsFor: 'next windows' stamp: 'AE 11/29/2021 00:19:55'!
openShoppingWindow

	ShoppingWindow openWithCartId: self model cartId 
					user: self model clientId 
					password: self model password 
					onPort: self model port.
	self closeButtonClicked. ! !


!AuthenticationClientWindow methodsFor: 'layout' stamp: 'AE 11/29/2021 16:12:24'!
build1stRow
	| sendRequestButtonMorph firstRowLayoutMorph |
	
	sendRequestButtonMorph := PluggableButtonMorph model: self model 
												 stateGetter: nil 
												 action: #createCartRequest  
												 label: 'Aceptar'.
	
	userTextBoxMorph := TextModelMorph textProvider: self model textGetter: #clientId textSetter: #clientId:. 
	userTextBoxMorph innerTextMorph setProperty: #keyStroke: toValue: [ :key | 	userTextBoxMorph innerTextMorph acceptContents ] .
	userTextBoxMorph  borderWidth: 1; borderColor: Color skyBlue; morphWidth: 100; morphHeight: 20.
	
	passwordTextBoxMorph := TextModelMorph textProvider: self model textGetter: #password textSetter: #password:. 
	passwordTextBoxMorph innerTextMorph setProperty: #keyStroke: toValue: [ :key | 	passwordTextBoxMorph innerTextMorph acceptContents ] .
	passwordTextBoxMorph  borderWidth: 1; borderColor: Color skyBlue; morphWidth: 100; morphHeight: 20. 
 
		
	firstRowLayoutMorph := LayoutMorph newColumn.
	firstRowLayoutMorph separation: 25;
	axisEdgeWeight: 0.5;
	addMorph: (LabelMorph contents:'Ingrese usuario y contrase�a:');
	addMorph: userTextBoxMorph;
 	addMorph: passwordTextBoxMorph;
	addMorph: sendRequestButtonMorph.
	
	^firstRowLayoutMorph.! !

!AuthenticationClientWindow methodsFor: 'layout' stamp: 'AE 11/29/2021 16:12:59'!
buildMorphicWindow
		
	self layoutMorph beRow;
	separation: 15;
	axisEdgeWeight: 0;
	addMorph: self build1stRow.
	"addMorph: self build2ndRow."
	! !

!AuthenticationClientWindow methodsFor: 'layout' stamp: 'AE 11/29/2021 15:33:13'!
defaultExtent

	^ 635@485! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'AuthenticationClientWindow class' category: 'TusLibros-Client'!
AuthenticationClientWindow class
	instanceVariableNames: ''!

!AuthenticationClientWindow class methodsFor: 'instance creation' stamp: 'AE 11/29/2021 00:13:52'!
connectTo: aPort
	
	^self new initializeWith: 'TusLibros Authentication Client Window' onPort: aPort.! !


!classDefinition: #CheckOutClientWindow category: 'TusLibros-Client'!
Panel subclass: #CheckOutClientWindow
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros-Client'!

!CheckOutClientWindow methodsFor: 'as yet unclassified' stamp: 'AE 11/29/2021 16:31:33'!
build1stColumn
	
	| cartListMorph |

	cartListMorph := PluggableListMorph model: self model 
									listGetter: #cartItems 
									indexGetter: #cartItemsListIndex 
									indexSetter: #cartItemsListIndex:.
	cartListMorph  borderColor: Color skyBlue; borderWidth: 1; morphWidth:300.


	layoutMorph := LayoutMorph newColumn .
	layoutMorph separation: 5; 
				axisEdgeWeight: 0.5;
				addMorph: cartListMorph;
				addMorph: (LabelMorph contents:'Items comprados por un total de $', self model totalPrice asString).

	
	^layoutMorph 
! !

!CheckOutClientWindow methodsFor: 'as yet unclassified' stamp: 'AE 11/29/2021 16:28:18'!
build2ndColumn

	| closeButtonMorph keepBuyingButtonMorph |
	keepBuyingButtonMorph := PluggableButtonMorph model: self model 
												stateGetter: nil 
												action: #startOver
												 label: 'Realizar una nueva compra'.
	closeButtonMorph := PluggableButtonMorph model: self model 
										 	stateGetter: nil
											action: #finish  label: 'Cerrar sesi�n'.

	layoutMorph := LayoutMorph newColumn .
	layoutMorph separation: 5; 
				axisEdgeWeight: 0.5;
				addMorph: keepBuyingButtonMorph;
				addMorph: closeButtonMorph.

	
	^layoutMorph 
! !

!CheckOutClientWindow methodsFor: 'as yet unclassified' stamp: 'AE 11/29/2021 16:30:11'!
buildMorphicWindow
		
	self layoutMorph beRow;
		separation: 15;
		axisEdgeWeight: 0.5;
		addMorph: self build1stColumn;
		addMorph: self build2ndColumn .
		
		
! !

!CheckOutClientWindow methodsFor: 'as yet unclassified' stamp: 'AE 11/29/2021 00:26:01'!
closeAndStartAgain

	| cartId |
	cartId _ self model createCartRequest.
	ShoppingWindow openWithCartId: cartId 
					user: self model clientId 
					password: self model password 
					onPort: self model port.
	self closeButtonClicked. ! !

!CheckOutClientWindow methodsFor: 'as yet unclassified' stamp: 'JDS 11/28/2021 19:58:21'!
closeCheckOutWindow

	self closeButtonClicked ! !

!CheckOutClientWindow methodsFor: 'as yet unclassified' stamp: 'AE 11/29/2021 16:32:18'!
defaultExtent

	^ 735@285! !


!CheckOutClientWindow methodsFor: 'initialization' stamp: 'AE 11/29/2021 00:09:33'!
initializeWith: aModel onPort: aPort  
	
	self titleMorph showButtonsNamed: #( close collapse ).
	self setLabel: 'Lista de items comprados'.
	self model: aModel.
	self morphExtent: (self defaultExtent).
	self buildMorphicWindow.
	self openInWorld.
	
	self model when: #finishEvent send: #closeCheckOutWindow to: self. 
	self model when: #startOverEvent send: #closeAndStartAgain to: self. 
	
	"self model when: #refreshItems send: #refreshCartItemsList to: self."
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CheckOutClientWindow class' category: 'TusLibros-Client'!
CheckOutClientWindow class
	instanceVariableNames: ''!

!CheckOutClientWindow class methodsFor: 'as yet unclassified' stamp: 'AE 11/29/2021 00:09:33'!
openUsingModel: aModel

	^self new initializeWith: aModel onPort: fillme
! !


!classDefinition: #ListPurchasesWindow category: 'TusLibros-Client'!
Panel subclass: #ListPurchasesWindow
	instanceVariableNames: 'spentMoneyTextBoxMorph'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros-Client'!

!ListPurchasesWindow methodsFor: 'initialization' stamp: 'AE 11/29/2021 13:49:03'!
initializeWith: aModel 
	
	self titleMorph showButtonsNamed: #( close collapse ).
	self setLabel: 'Lista hist�rica de items comprados'.
	self model: aModel.
	self morphExtent: (self defaultExtent).
	self buildMorphicWindow.
	self openInWorld.
! !


!ListPurchasesWindow methodsFor: 'layout' stamp: 'AE 11/29/2021 13:44:02'!
build
	| firstRowLayoutMorph  purchasesListMorph totalSpent |
	
	purchasesListMorph := PluggableListMorph model: self
										 listGetter: #purchasesFormatted 
										indexGetter: nil 
										indexSetter: nil.
	purchasesListMorph  borderColor: Color skyBlue; borderWidth: 1; morphWidth:300.	
	
	totalSpent _ self model purchases values sum: [ :e| e ] ifEmpty: 0.
	
	firstRowLayoutMorph := LayoutMorph newRow.
	firstRowLayoutMorph separation: 25;
				   	axisEdgeWeight: 0.5;
					addMorph: purchasesListMorph;
					addMorph: (LabelMorph contents:'Precio total: ', totalSpent asString).
	

	^firstRowLayoutMorph.
	! !

!ListPurchasesWindow methodsFor: 'layout' stamp: 'JDS 11/28/2021 20:43:37'!
buildMorphicWindow
		
	self layoutMorph beColumn;
	separation: 15;
	axisEdgeWeight: 0;
	addMorph: self build.! !

!ListPurchasesWindow methodsFor: 'layout' stamp: 'JDS 11/28/2021 20:44:41'!
defaultExtent

	^ 1035@485! !

!ListPurchasesWindow methodsFor: 'layout' stamp: 'AE 11/29/2021 16:14:55'!
purchasesFormatted

	| list |
	list _ OrderedCollection new.
	self model purchases associationsDo: [ :e |   list  add: ('$', e value asString, ' on ', e key asString) ].
	
	^list.
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'ListPurchasesWindow class' category: 'TusLibros-Client'!
ListPurchasesWindow class
	instanceVariableNames: ''!

!ListPurchasesWindow class methodsFor: 'instance creation' stamp: 'AE 11/29/2021 00:33:04'!
openUsingModel: aModel 
	
		^self new initializeWith: aModel! !


!classDefinition: #ShoppingWindow category: 'TusLibros-Client'!
Panel subclass: #ShoppingWindow
	instanceVariableNames: 'cartListMorph totalPriceLabelMorph'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros-Client'!

!ShoppingWindow methodsFor: 'initialization' stamp: 'AE 11/29/2021 15:50:37'!
initializeWith: aTitle cartId: aCartId user: aClientId password: aPassword onPort: aPort 

	self titleMorph showButtonsNamed: #( close collapse ).
	self setLabel: aTitle.
	self model: (ShoppingWindowModel withCartId: aCartId user: aClientId password: aPassword onPort: aPort).
	self morphExtent: (self defaultExtent).
	self buildMorphicWindow.
	self openInWorld.
			
	self model when: #refreshItems send: #refreshCartItemsList to: self.
	self model when: #checkOutSuccessful send: #openCheckOut to: self.
	self model when: #checkOutFailed send: #warnFailedCheckout to: self.
	self model when: #listPurchasesEvent send: #openListPurchasesWindow to: self. 
	"Investigar:
	self model when: #newWordsArrived send: #refreshListOfWords:and: to: self."! !


!ShoppingWindow methodsFor: 'change reporting' stamp: 'AE 11/29/2021 13:50:38'!
refreshCartItemsList

	cartListMorph updateList.
	cartListMorph setSelectionIndex: 0.

	totalPriceLabelMorph contents: 'Precio Total: $', self model totalPrice asString.
	! !


!ShoppingWindow methodsFor: 'next windows' stamp: 'AE 11/28/2021 23:45:19'!
openCheckOut

	CheckOutClientWindow openUsingModel: self model.
	self closeButtonClicked. ! !

!ShoppingWindow methodsFor: 'next windows' stamp: 'AE 11/28/2021 23:45:32'!
openListPurchasesWindow

	ListPurchasesWindow openUsingModel: self model.
! !


!ShoppingWindow methodsFor: 'layout' stamp: 'AE 11/29/2021 13:54:40'!
build1stColumn
	
	| firstColumnLayoutMorph removeButtonMorph checkOutButtonMorph listPurchasesButtonMorph removeQuantityTextBoxMorph |

	cartListMorph := PluggableListMorph model: self model listGetter: #cartItems indexGetter: #cartItemsListIndex indexSetter: #cartItemsListIndex:.
	cartListMorph  borderColor: Color skyBlue; borderWidth: 1; morphWidth:300.

	removeButtonMorph := PluggableButtonMorph model: self model stateGetter: nil action: #removeItemToCartRequest  label: 'Sacar item'.
	checkOutButtonMorph := PluggableButtonMorph model: self model stateGetter: nil action: #checkOutRequest  label: 'Checkout carrito'.
	listPurchasesButtonMorph := PluggableButtonMorph model: self model stateGetter: nil action: #listPurchasesRequest  label: 'Hist�rico de compras'.

	removeQuantityTextBoxMorph := TextModelMorph textProvider: self model textGetter: #removeQuantity textSetter: #removeQuantity:. 
	removeQuantityTextBoxMorph innerTextMorph setProperty: #keyStroke: toValue: [ :key | 	removeQuantityTextBoxMorph innerTextMorph acceptContents ] .
	removeQuantityTextBoxMorph  borderWidth: 1; borderColor: Color skyBlue; morphWidth: 50; morphHeight: 20.

	totalPriceLabelMorph _ LabelMorph contents: 'Precio total: $0'.
	
	firstColumnLayoutMorph := LayoutMorph newColumn.
	firstColumnLayoutMorph separation: 5; 
	axisEdgeWeight: 0.5;
	addMorph: (LabelMorph contents:'Items en carrito');
	addMorph: cartListMorph;
	addMorph: removeButtonMorph;
	addMorph: removeQuantityTextBoxMorph;
	addMorph: totalPriceLabelMorph ;
	addMorph: checkOutButtonMorph;
	addMorph: listPurchasesButtonMorph.

	
	^firstColumnLayoutMorph 


! !

!ShoppingWindow methodsFor: 'layout' stamp: 'AE 11/29/2021 13:50:22'!
build2ndColumn
	
	| catalogueListMorph secondColumnLayoutMorph addButtonMorph addQuantityTextBoxMorph |

	catalogueListMorph := PluggableListMorph model: self model 
										listGetter: #catalogueItemsWithPrice 
										indexGetter: #catalogueItemsListIndex
										indexSetter: #catalogueItemsListIndex:.
	catalogueListMorph  borderColor: Color skyBlue; borderWidth: 1; morphWidth:300.
	
	addButtonMorph := PluggableButtonMorph model: self model 
										  stateGetter: nil 
										  action: #addItemToCartRequest 
										  label: 'Agregar item'.


	addQuantityTextBoxMorph := TextModelMorph textProvider: self model textGetter: #addQuantity textSetter: #addQuantity:. 
	addQuantityTextBoxMorph innerTextMorph setProperty: #keyStroke: toValue: [ :key | 	addQuantityTextBoxMorph innerTextMorph acceptContents ] .
	addQuantityTextBoxMorph  borderWidth: 1; borderColor: Color skyBlue; morphWidth: 50; morphHeight: 20.
	
	secondColumnLayoutMorph := LayoutMorph newColumn.
	secondColumnLayoutMorph separation: 5; 
		axisEdgeWeight: 0.5;
		addMorph: (LabelMorph contents:'Cat�logo de items');
		addMorph: catalogueListMorph;
		addMorph: addButtonMorph;
		addMorph: addQuantityTextBoxMorph .
	
	^secondColumnLayoutMorph 


! !

!ShoppingWindow methodsFor: 'layout' stamp: 'AE 11/29/2021 12:15:29'!
buildMorphicWindow
		
	self layoutMorph beRow ;
	separation: 15;
	axisEdgeWeight: 0;
	addMorph: self build1stColumn;
	addMorph: self build2ndColumn. ! !

!ShoppingWindow methodsFor: 'layout' stamp: 'AE 11/29/2021 16:05:26'!
defaultExtent

	^ 735@485! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'ShoppingWindow class' category: 'TusLibros-Client'!
ShoppingWindow class
	instanceVariableNames: ''!

!ShoppingWindow class methodsFor: 'as yet unclassified' stamp: 'AE 11/29/2021 00:17:59'!
openWithCartId: aCartId user: aClientId password: aPassword onPort: aPort 
	
	^self new initializeWith: 'TusLibros Shopping Window' cartId: aCartId user: aClientId password: aPassword onPort: aPort.! !


!classDefinition: #AuthenticationClientWindowModel category: 'TusLibros-Client'!
Object subclass: #AuthenticationClientWindowModel
	instanceVariableNames: 'clientId password restInterface cartIdentity'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros-Client'!

!AuthenticationClientWindowModel methodsFor: 'accessing' stamp: 'JDS 11/27/2021 17:05:06'!
cartId

	^ cartIdentity! !

!AuthenticationClientWindowModel methodsFor: 'accessing' stamp: 'JDS 11/18/2021 12:49:40'!
clientId

	^clientId! !

!AuthenticationClientWindowModel methodsFor: 'accessing' stamp: 'JDS 11/18/2021 12:50:35'!
clientId: aClientId

	clientId _ aClientId! !

!AuthenticationClientWindowModel methodsFor: 'accessing' stamp: 'AE 11/29/2021 00:12:21'!
initializeOnPort: aPort

	clientId _ ''.
	password _ ''.
	restInterface  _ RestClientInterface onPort: aPort. ! !

!AuthenticationClientWindowModel methodsFor: 'accessing' stamp: 'JDS 11/18/2021 12:49:59'!
password

	^password! !

!AuthenticationClientWindowModel methodsFor: 'accessing' stamp: 'JDS 11/18/2021 12:50:59'!
password: aPassword

	password _ aPassword! !


!AuthenticationClientWindowModel methodsFor: 'Pluggable Button Selectors' stamp: 'AE 11/29/2021 12:08:48'!
createCartRequest
	
	clientId _ clientId asString withBlanksTrimmed .
	
	[
		cartIdentity _ restInterface createCartWith: clientId and: password.
	] 
		on: Error
		do: [ ^self triggerEvent: #invalidLogin ].
		
	self triggerEvent: #createCartSuccessful. ! !


!AuthenticationClientWindowModel methodsFor: 'as yet unclassified' stamp: 'AE 11/29/2021 00:20:38'!
port
	^restInterface port ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'AuthenticationClientWindowModel class' category: 'TusLibros-Client'!
AuthenticationClientWindowModel class
	instanceVariableNames: ''!

!AuthenticationClientWindowModel class methodsFor: 'as yet unclassified' stamp: 'AE 11/29/2021 00:11:47'!
onPort: aPort

	^self new initializeOnPort: aPort.! !


!classDefinition: #RestClientInterface category: 'TusLibros-Client'!
Object subclass: #RestClientInterface
	instanceVariableNames: 'port'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros-Client'!

!RestClientInterface methodsFor: 'initialization' stamp: 'AE 11/29/2021 00:15:12'!
initializeOnPort: aPort 
	
	port := aPort.! !

!RestClientInterface methodsFor: 'initialization' stamp: 'AE 11/29/2021 00:21:36'!
port
	
	^port.! !

!RestClientInterface methodsFor: 'initialization' stamp: 'JDS 11/18/2021 14:26:45'!
url
	
	^'http://localhost:', self port asString! !


!RestClientInterface methodsFor: 'private - requests' stamp: 'AE 11/29/2021 10:39:59'!
doRequest: path fields: aFields	
	
	| resp |
	
	resp:= WebClient htmlSubmit: (self url,path) fields: aFields.
	
	resp isSuccess 
		ifTrue:[^((resp content) readStream)] 
		ifFalse:[^ self error: resp content].

	! !

!RestClientInterface methodsFor: 'private - requests' stamp: 'AE 11/29/2021 11:41:38'!
doRequestAndCheckOk: path fields: aFields	

	^ 'OK' = (self doRequest: path fields: aFields).
	
	! !

!RestClientInterface methodsFor: 'private - requests' stamp: 'AE 11/29/2021 11:01:04'!
doRequestAndDecodeJson: path fields: aFields	
	
	^WebUtils jsonDecode: (self doRequest: path fields: aFields).
	
! !


!RestClientInterface methodsFor: 'sending requests' stamp: 'AE 11/29/2021 11:25:56'!
addToCart: aCartId bookISBN: aBookISBN quantity: aQuantity
	
	| fieldDict |

	fieldDict _ self makeFields: {
		'cartId' -> aCartId.
		'bookIsbn' -> aBookISBN.
		'bookQuantity' -> aQuantity 
		}.
		
	^self doRequestAndCheckOk: '/addToCart' fields: fieldDict.
	
		! !

!RestClientInterface methodsFor: 'sending requests' stamp: 'AE 11/29/2021 11:01:04'!
catalogue

	^self doRequestAndDecodeJson: '/catalogue' fields: ''.! !

!RestClientInterface methodsFor: 'sending requests' stamp: 'AE 11/29/2021 11:38:04'!
checkOutCart: aCartId ccn: aValidCreditCardNumber cced: aCreditCardExpDate cco: aValidCreditCardOwner

	| fieldDict |
	
	fieldDict _ self makeFields: {
		'cartId' -> aCartId.
		'ccn' -> aValidCreditCardNumber.
		'cced' -> aCreditCardExpDate .
		'cco' ->aValidCreditCardOwner 
		}.
		
	^self doRequestAndCheckOk: '/checkOutCart' fields: fieldDict.
	
	! !

!RestClientInterface methodsFor: 'sending requests' stamp: 'AE 11/29/2021 11:24:01'!
createCartWith: aUser and: aPassword

	| fieldDict |
	
	fieldDict _ self makeFields: {
		'clientId' 	-> aUser.
		'password' -> aPassword
		}.
	
	
	^self doRequestAndDecodeJson: '/createCart' fields: fieldDict.
	! !

!RestClientInterface methodsFor: 'sending requests' stamp: 'AE 11/29/2021 11:42:11'!
listCart: aCartId

	| fieldDict |
	
	fieldDict _ self makeFields: { 
		'cartId' -> aCartId.
		}.
		
	^self doRequestAndDecodeJson: '/listCart' fields: fieldDict.
	! !

!RestClientInterface methodsFor: 'sending requests' stamp: 'AE 11/29/2021 11:50:12'!
listPurchasesOf: aUser authenticatingWith: aPassword

	| fieldDict |
	
	fieldDict _ self makeFields: {
		'clientId' -> aUser.
		'password' -> aPassword.
		}.
		
	^self doRequestAndDecodeJson: '/listPurchases' fields: fieldDict.
	! !

!RestClientInterface methodsFor: 'sending requests' stamp: 'AE 11/29/2021 11:59:02'!
removeFromCart: aCartId bookISBN: aBookISBN quantity: aQuantity
	
	| fieldDict |
	
	fieldDict _ self makeFields: {
		'cartId' -> aCartId.
		'bookIsbn' -> aBookISBN .
		'bookQuantity' -> aQuantity .
		}.
		
	^self doRequestAndCheckOk: '/removeFromCart' fields: fieldDict.

	! !


!RestClientInterface methodsFor: 'url field management' stamp: 'AE 11/29/2021 08:54:38'!
addField: aDict field: aField value: aValue

	aDict at: aField put: (self correctlyEncodeSpacesForUrlRequestParameter: aValue asString).
	
	^aDict.
	
! !

!RestClientInterface methodsFor: 'url field management' stamp: 'JDS 11/18/2021 14:27:29'!
correctlyEncodeSpacesForUrlRequestParameter: aParameter
	
	^ aParameter copyReplaceAll: ' ' with: '%20'. ! !

!RestClientInterface methodsFor: 'url field management' stamp: 'AE 11/29/2021 15:47:30'!
makeFields: arrayOfAssoc

	| dict |
	dict _ Dictionary new.
	 
	arrayOfAssoc do: [ :element |  self addField: dict field: (element key) value: (element value) ].
	
	^dict	
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'RestClientInterface class' category: 'TusLibros-Client'!
RestClientInterface class
	instanceVariableNames: ''!

!RestClientInterface class methodsFor: 'instance creation' stamp: 'AE 11/29/2021 00:14:52'!
onPort: aPort
	^self new initializeOnPort: aPort. ! !


!classDefinition: #ShoppingWindowModel category: 'TusLibros-Client'!
Object subclass: #ShoppingWindowModel
	instanceVariableNames: 'catalogueItems catalogueItemsListIndex cartItems cartItemsListIndex restInterface cartId clientId password addQuantity purchases purchasesListIndex removeQuantity'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros-Client'!

!ShoppingWindowModel methodsFor: 'model' stamp: 'AE 11/29/2021 12:33:19'!
addQuantity
	^addQuantity asString.
	! !

!ShoppingWindowModel methodsFor: 'model' stamp: 'AE 11/29/2021 13:02:04'!
addQuantity: aQuantity
	
	[ addQuantity _ aQuantity asNumber ceiling . ]
		on: Error
		do: [ addQuantity _ 1 ].
			
	self assert: addQuantity > 0.
	! !

!ShoppingWindowModel methodsFor: 'model' stamp: 'JDS 11/27/2021 19:18:58'!
cartItems

	^ cartItems ! !

!ShoppingWindowModel methodsFor: 'model' stamp: 'JDS 11/27/2021 19:22:03'!
cartItemsListIndex

	^ cartItemsListIndex ! !

!ShoppingWindowModel methodsFor: 'model' stamp: 'JDS 11/27/2021 19:22:21'!
cartItemsListIndex: anIndex

	cartItemsListIndex  _ anIndex.! !

!ShoppingWindowModel methodsFor: 'model' stamp: 'AE 11/29/2021 12:22:19'!
catalogueItems

	| list |
	list _ OrderedCollection new.
	
	catalogueItems associationsDo: [ :element | 
		list add:
			(element key, ': $', element value asString)
			].
		
	^ catalogueItems keys! !

!ShoppingWindowModel methodsFor: 'model' stamp: 'JDS 11/27/2021 19:19:27'!
catalogueItemsListIndex

	^ catalogueItemsListIndex ! !

!ShoppingWindowModel methodsFor: 'model' stamp: 'JDS 11/27/2021 19:19:55'!
catalogueItemsListIndex: anIndex

	catalogueItemsListIndex _ anIndex.  ! !

!ShoppingWindowModel methodsFor: 'model' stamp: 'AE 11/29/2021 12:22:39'!
catalogueItemsWithPrice

	| list |
	list _ OrderedCollection new.
	
	catalogueItems associationsDo: [ :element | 
		list add:
			(element key, ': $', element value asString)
			].
		
	^list
! !

!ShoppingWindowModel methodsFor: 'model' stamp: 'JDS 11/28/2021 19:49:23'!
clientId
	^clientId! !

!ShoppingWindowModel methodsFor: 'model' stamp: 'JDS 11/28/2021 19:49:36'!
password
	^password ! !

!ShoppingWindowModel methodsFor: 'model' stamp: 'AE 11/29/2021 00:28:59'!
port
	^restInterface port.
	! !

!ShoppingWindowModel methodsFor: 'model' stamp: 'AE 11/29/2021 08:17:20'!
purchases

	^ purchases.
	! !

!ShoppingWindowModel methodsFor: 'model' stamp: 'AE 11/29/2021 12:42:37'!
removeQuantity
	^removeQuantity asString.
	! !

!ShoppingWindowModel methodsFor: 'model' stamp: 'AE 11/29/2021 13:02:25'!
removeQuantity: aQuantity
	
	[ removeQuantity _ aQuantity asNumber ceiling . ]
		on: Error
		do: [ removeQuantity _ 1 ].
		! !

!ShoppingWindowModel methodsFor: 'model' stamp: 'AE 11/29/2021 12:57:51'!
totalPrice
	
	| total |
	total _ 0.
	
	cartItems do: [ :item |  total _ total + catalogueItems at: item ].
	
	^total.
	! !


!ShoppingWindowModel methodsFor: 'initialization' stamp: 'AE 11/29/2021 12:35:55'!
initializeWith: aCartId user: aClientId password: aPassword onPort: aPort 

	restInterface _ RestClientInterface onPort: aPort.
	cartItems := OrderedCollection new.
	cartItemsListIndex := 0.
	catalogueItems := restInterface catalogue.
	catalogueItemsListIndex := 0.
	purchases _ Dictionary new.
	purchasesListIndex _ 0.
	addQuantity _ 1.
	removeQuantity _ 1.

	cartId _ aCartId.
	clientId _ aClientId.
	password _ aPassword. 
	
! !


!ShoppingWindowModel methodsFor: 'private - requests to server' stamp: 'AE 11/29/2021 12:39:48'!
addItemToCartRequest
	
	| item |
	
	(catalogueItemsListIndex = 0) ifTrue: [ ^self ].
	
	item _ self catalogueItems at: catalogueItemsListIndex.
	
	restInterface addToCart: cartId bookISBN: item quantity: addQuantity .
	addQuantity timesRepeat: [  cartItems add: item. ].
	
	self triggerEvent: #refreshItems.
	
	! !

!ShoppingWindowModel methodsFor: 'private - requests to server' stamp: 'AE 11/29/2021 15:54:31'!
checkOutRequest
	
	[
		self checkSynchronizationWithServer .
		restInterface checkOutCart: cartId ccn: 'aValidWebCCN' cced: '12/34' cco: 'aValidWebOwner'.
	] on: Error
	  do: [ ^self triggerEvent: #checkOutFailed ].
		
	self triggerEvent: #checkOutSuccessful.
	
! !

!ShoppingWindowModel methodsFor: 'private - requests to server' stamp: 'AE 11/29/2021 09:41:00'!
checkSynchronizationWithServer

	| serverVersion cartDict |
	serverVersion _ restInterface listCart: cartId.
	
	cartDict _ Dictionary new.
	
	cartItems do: [ :e | cartDict at: e put: (cartItems occurrencesOf: e) ].
	
	self assert: cartDict = serverVersion.
	! !

!ShoppingWindowModel methodsFor: 'private - requests to server' stamp: 'JDS 11/28/2021 19:44:05'!
createCartRequest

	^ restInterface createCartWith: clientId and: password.
	! !

!ShoppingWindowModel methodsFor: 'private - requests to server' stamp: 'JDS 11/28/2021 20:22:48'!
listPurchasesRequest

	purchases _ restInterface listPurchasesOf: clientId authenticatingWith: password.
	self triggerEvent: #listPurchasesEvent.
	! !

!ShoppingWindowModel methodsFor: 'private - requests to server' stamp: 'AE 11/29/2021 13:12:29'!
removeItemToCartRequest
	
	| item |
	
	(cartItemsListIndex = 0) ifTrue: [^self ].
	
	item _ self cartItems at: cartItemsListIndex.
	restInterface removeFromCart: cartId bookISBN: item quantity: removeQuantity .
	removeQuantity timesRepeat: [ cartItems remove: item ifAbsent: [] ].
	
	Transcript show: 'Removing item: ', item asString, ' x ', removeQuantity asString.
	
	self triggerEvent: #refreshItems.! !


!ShoppingWindowModel methodsFor: 'events-triggering' stamp: 'JDS 11/28/2021 19:28:35'!
finish

	self triggerEvent: #finishEvent.! !

!ShoppingWindowModel methodsFor: 'events-triggering' stamp: 'JDS 11/28/2021 19:47:01'!
startOver

	self triggerEvent: #startOverEvent! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'ShoppingWindowModel class' category: 'TusLibros-Client'!
ShoppingWindowModel class
	instanceVariableNames: ''!

!ShoppingWindowModel class methodsFor: 'as yet unclassified' stamp: 'AE 11/29/2021 00:19:10'!
withCartId: aCartId user: aClientId password: aPassword onPort: aPort 

	^ self new initializeWith: aCartId user: aClientId password: aPassword onPort: aPort ! !


!classDefinition: #TusLibrosClient category: 'TusLibros-Client'!
Object subclass: #TusLibrosClient
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros-Client'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TusLibrosClient class' category: 'TusLibros-Client'!
TusLibrosClient class
	instanceVariableNames: ''!

!TusLibrosClient class methodsFor: 'as yet unclassified' stamp: 'AE 11/29/2021 16:40:56'!
connectTo: aPort
	
	^AuthenticationClientWindow connectTo: aPort.
	! !
