!classDefinition: #MarsRoverTest category: 'MarsRover'!
TestCase subclass: #MarsRoverTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!MarsRoverTest methodsFor: 'instance creation' stamp: 'JDS 10/5/2021 00:35:53'!
createMarsRoverAt: aPosition with: aHeading
	
	^MarsRover createAt: aPosition headingTo: aHeading! !


!MarsRoverTest methodsFor: 'auxiliar testing' stamp: 'JDS 10/6/2021 15:59:57'!
assertRoverPosition: anOrigin withHeading: aHeading process: aProcess expectedPosition: anExpectedValue expectedHeading: anExpectedHeading    

	| rover |
	rover _ self createMarsRoverAt: anOrigin with: aHeading.
	
	rover process: aProcess.
	
	self assert: anExpectedValue equals: rover position.
	self assert: anExpectedHeading equals: rover heading! !


!MarsRoverTest methodsFor: 'testing' stamp: 'JDS 10/6/2021 16:21:15'!
test01RoverProcessNothing

	self 
		assertRoverPosition: 0 @ 0 
		withHeading: North 
		process: '' 
		expectedPosition: 0 @ 0 
		expectedHeading: North
	! !

!MarsRoverTest methodsFor: 'testing' stamp: 'JDS 10/6/2021 15:58:43'!
test02RoverHeadingNorthMovesForwardEndsAtCorrectPosition

	self 
		assertRoverPosition: 0 @ 0 
		withHeading: North 
		process: 'f' 
		expectedPosition: 0 @ 1 expectedHeading: North! !

!MarsRoverTest methodsFor: 'testing' stamp: 'JDS 10/6/2021 15:58:43'!
test03RoverHeadingNorthMovesBackwardEndsAtCorrectPosition

	self 
		assertRoverPosition: 0 @ 0 
		withHeading: North 
		process: 'b' 
		expectedPosition: 0 @ -1 expectedHeading: North
! !

!MarsRoverTest methodsFor: 'testing' stamp: 'JDS 10/6/2021 16:01:52'!
test04RoverHeadingNorthRotateLeftHeadsToWest

	self 
		assertRoverPosition: 2 @ 2 
		withHeading: North 
		process: 'l' 
		expectedPosition: 2 @ 2 
		expectedHeading: West! !

!MarsRoverTest methodsFor: 'testing' stamp: 'JDS 10/6/2021 16:02:52'!
test05RoverHeadingNorthRotateRightHeadsToEast

	self 
		assertRoverPosition: 2 @ 2 
		withHeading: North 
		process: 'r' 
		expectedPosition: 2 @ 2 
		expectedHeading: East! !

!MarsRoverTest methodsFor: 'testing' stamp: 'JDS 10/6/2021 16:04:19'!
test06RoverHeadingSouthMovesForwardEndsAtCorrectPosition

	self 
		assertRoverPosition: 2 @ 2 
		withHeading: South 
		process: 'f' 
		expectedPosition: 2 @ 1 
		expectedHeading: South ! !

!MarsRoverTest methodsFor: 'testing' stamp: 'JDS 10/6/2021 16:04:33'!
test07RoverHeadingWestMovesForwardEndsAtCorrectPosition

	self 
		assertRoverPosition: 2 @ 2 
		withHeading: West 
		process: 'f' 
		expectedPosition: 1 @ 2 
		expectedHeading: West! !

!MarsRoverTest methodsFor: 'testing' stamp: 'JDS 10/6/2021 16:05:44'!
test08RoverHeadingEastMovesForwardEndsAtCorrectPosition

	self 
		assertRoverPosition: 2 @ 2 
		withHeading: East 
		process: 'f' 
		expectedPosition: 3 @ 2 
		expectedHeading: East
	! !

!MarsRoverTest methodsFor: 'testing' stamp: 'JDS 10/6/2021 15:14:31'!
test09RoverReceivesInvalidCommandsAndThrowsAnError

	| rover originPosition expectedPosition originHeading |
	
	originPosition _  0 @ 0.
	originHeading _ North.
	expectedPosition _ originPosition + (0 @ 2).
	rover _ self createMarsRoverAt: originPosition with: originHeading.
	

	self should: [rover process: 'ffelf'] raise: Error description: MarsRover badCommandErrorDescription.
	self assert: originHeading equals: rover heading.
	self assert: expectedPosition equals: rover position.! !

!MarsRoverTest methodsFor: 'testing' stamp: 'JDS 10/6/2021 16:07:28'!
test10RoverAfterReceivesMultipleCommandsEndsInRightPosition

	self 
		assertRoverPosition: 0 @ 0 
		withHeading: North 
		process: 'ffrbrf' 
		expectedPosition: -1 @ 1 
		expectedHeading: South
	! !


!classDefinition: #MarsRover category: 'MarsRover'!
Object subclass: #MarsRover
	instanceVariableNames: 'position heading'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!MarsRover methodsFor: 'accessing' stamp: 'JDS 10/4/2021 22:54:12'!
heading
	
	^heading! !

!MarsRover methodsFor: 'accessing' stamp: 'JDS 10/4/2021 22:31:08'!
position
	
	^position! !


!MarsRover methodsFor: 'operation' stamp: 'JDS 10/5/2021 23:45:49'!
headTo: aMarsRoverOrientation

	heading _ aMarsRoverOrientation

	! !

!MarsRover methodsFor: 'operation' stamp: 'JDS 10/6/2021 15:19:24'!
headingEastProcess: aRoverDirective

	aRoverDirective headingEastProcess: self.
! !

!MarsRover methodsFor: 'operation' stamp: 'JDS 10/6/2021 15:19:31'!
headingNorthProcess: aRoverDirective

	aRoverDirective headingNorthProcess: self.
! !

!MarsRover methodsFor: 'operation' stamp: 'JDS 10/6/2021 15:19:35'!
headingSouthProcess: aRoverDirective

	aRoverDirective headingSouthProcess: self.
! !

!MarsRover methodsFor: 'operation' stamp: 'JDS 10/6/2021 15:19:40'!
headingWestProcess: aRoverDirective

	aRoverDirective headingWestProcess: self.
! !

!MarsRover methodsFor: 'operation' stamp: 'JDS 10/5/2021 23:29:17'!
process: aStream

	aStream do: [ :anOrder | | directive | 
		directive _ RoverDirective new: anOrder. 
		self heading process: directive for: self]. 

	
	! !


!MarsRover methodsFor: 'initialization' stamp: 'JDS 10/4/2021 23:59:05'!
initializeCreateAt: aPosition headingTo: aHeading 
	
	position _ aPosition.
	heading _ aHeading.! !


!MarsRover methodsFor: 'error' stamp: 'JDS 10/5/2021 23:02:21'!
commandError

	^self error: self class badCommandErrorDescription ! !


!MarsRover methodsFor: 'private' stamp: 'JDS 10/5/2021 23:42:06'!
addOneUnitOnX

	position _ position + (1 @ 0).! !

!MarsRover methodsFor: 'private' stamp: 'JDS 10/5/2021 23:42:49'!
addOneUnitOnY

	position _ position + (0 @ 1).! !

!MarsRover methodsFor: 'private' stamp: 'JDS 10/5/2021 23:42:21'!
subOneUnitOnX

	position _ position - (1 @ 0).! !

!MarsRover methodsFor: 'private' stamp: 'JDS 10/5/2021 23:42:34'!
subOneUnitOnY

	position _ position - (0 @ 1).! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRover class' category: 'MarsRover'!
MarsRover class
	instanceVariableNames: ''!

!MarsRover class methodsFor: 'instance creation' stamp: 'JDS 10/4/2021 23:59:05'!
createAt: aPosition headingTo: aHeading 
	
	^self new initializeCreateAt: aPosition headingTo: aHeading ! !


!MarsRover class methodsFor: 'error' stamp: 'JDS 10/5/2021 22:59:07'!
badCommandErrorDescription

	^ 'I receive a bad command, only accept  f, b, r, l.'! !


!classDefinition: #MarsRoverOrientation category: 'MarsRover'!
Object subclass: #MarsRoverOrientation
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverOrientation class' category: 'MarsRover'!
MarsRoverOrientation class
	instanceVariableNames: ''!

!MarsRoverOrientation class methodsFor: 'private' stamp: 'JDS 10/5/2021 00:54:38'!
new

	self shouldNotImplement ! !

!MarsRoverOrientation class methodsFor: 'private' stamp: 'JDS 10/5/2021 00:48:23'!
process: anOrder for: aRover

	self subclassResponsibility ! !


!classDefinition: #East category: 'MarsRover'!
MarsRoverOrientation subclass: #East
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'East class' category: 'MarsRover'!
East class
	instanceVariableNames: ''!

!East class methodsFor: 'private' stamp: 'JDS 10/5/2021 00:42:50'!
process: anOrder for: aRover

	^ aRover headingEastProcess: anOrder! !


!classDefinition: #North category: 'MarsRover'!
MarsRoverOrientation subclass: #North
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'North class' category: 'MarsRover'!
North class
	instanceVariableNames: ''!

!North class methodsFor: 'private' stamp: 'JDS 10/5/2021 22:53:03'!
process: anOrder for: aRover

	^ aRover headingNorthProcess: anOrder! !


!classDefinition: #South category: 'MarsRover'!
MarsRoverOrientation subclass: #South
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'South class' category: 'MarsRover'!
South class
	instanceVariableNames: ''!

!South class methodsFor: 'private' stamp: 'JDS 10/5/2021 00:42:13'!
process: anOrder for: aRover

	^ aRover headingSouthProcess: anOrder! !


!classDefinition: #West category: 'MarsRover'!
MarsRoverOrientation subclass: #West
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'West class' category: 'MarsRover'!
West class
	instanceVariableNames: ''!

!West class methodsFor: 'private' stamp: 'JDS 10/5/2021 00:42:28'!
process: anOrder for: aRover

	^ aRover headingWestProcess: anOrder! !


!classDefinition: #RoverDirective category: 'MarsRover'!
Object subclass: #RoverDirective
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'RoverDirective class' category: 'MarsRover'!
RoverDirective class
	instanceVariableNames: ''!

!RoverDirective class methodsFor: 'instance creation' stamp: 'JDS 10/6/2021 16:19:56'!
new: anOrder

	^self subclasses 
		detect: [ :aSubclass | aSubclass canHandle: anOrder] 
		ifNone: [self error: MarsRover badCommandErrorDescription ] ! !


!RoverDirective class methodsFor: 'testing' stamp: 'JDS 10/5/2021 23:24:45'!
canHandle: anOrder

	self subclassResponsibility ! !


!classDefinition: #Backward category: 'MarsRover'!
RoverDirective subclass: #Backward
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Backward class' category: 'MarsRover'!
Backward class
	instanceVariableNames: ''!

!Backward class methodsFor: 'testing' stamp: 'JDS 10/5/2021 23:23:20'!
canHandle: anOrder

	^ anOrder = $b! !


!Backward class methodsFor: 'private' stamp: 'JDS 10/5/2021 23:50:57'!
headingEastProcess: aRover

	aRover subOneUnitOnX! !

!Backward class methodsFor: 'private' stamp: 'JDS 10/5/2021 23:54:42'!
headingNorthProcess: aRover

	aRover subOneUnitOnY! !

!Backward class methodsFor: 'private' stamp: 'JDS 10/5/2021 23:54:54'!
headingSouthProcess: aRover

	aRover addOneUnitOnY! !

!Backward class methodsFor: 'private' stamp: 'JDS 10/5/2021 23:54:19'!
headingWestProcess: aRover

	aRover addOneUnitOnX! !


!classDefinition: #Forward category: 'MarsRover'!
RoverDirective subclass: #Forward
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Forward class' category: 'MarsRover'!
Forward class
	instanceVariableNames: ''!

!Forward class methodsFor: 'testing' stamp: 'JDS 10/5/2021 23:23:42'!
canHandle: anOrder

	^ anOrder = $f! !


!Forward class methodsFor: 'private' stamp: 'JDS 10/5/2021 23:56:08'!
headingEastProcess: aRover

	aRover addOneUnitOnX! !

!Forward class methodsFor: 'private' stamp: 'JDS 10/5/2021 23:55:22'!
headingNorthProcess: aRover

	aRover addOneUnitOnY! !

!Forward class methodsFor: 'private' stamp: 'JDS 10/5/2021 23:55:38'!
headingSouthProcess: aRover

	aRover subOneUnitOnY! !

!Forward class methodsFor: 'private' stamp: 'JDS 10/5/2021 23:56:21'!
headingWestProcess: aRover

	aRover subOneUnitOnX! !


!classDefinition: #Left category: 'MarsRover'!
RoverDirective subclass: #Left
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Left class' category: 'MarsRover'!
Left class
	instanceVariableNames: ''!

!Left class methodsFor: 'testing' stamp: 'JDS 10/5/2021 23:23:52'!
canHandle: anOrder

	^ anOrder = $l! !


!Left class methodsFor: 'private' stamp: 'JDS 10/6/2021 00:10:27'!
headingEastProcess: aRover

	aRover headTo: North! !

!Left class methodsFor: 'private' stamp: 'JDS 10/6/2021 00:11:15'!
headingNorthProcess: aRover

	aRover headTo: West! !

!Left class methodsFor: 'private' stamp: 'JDS 10/6/2021 00:11:30'!
headingSouthProcess: aRover

	aRover headTo: East! !

!Left class methodsFor: 'private' stamp: 'JDS 10/6/2021 00:10:58'!
headingWestProcess: aRover

	aRover headTo: South! !


!classDefinition: #Right category: 'MarsRover'!
RoverDirective subclass: #Right
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Right class' category: 'MarsRover'!
Right class
	instanceVariableNames: ''!

!Right class methodsFor: 'private' stamp: 'JDS 10/6/2021 00:12:08'!
headingEastProcess: aRover

	aRover headTo: South! !

!Right class methodsFor: 'private' stamp: 'JDS 10/6/2021 00:12:47'!
headingNorthProcess: aRover

	aRover headTo: East! !

!Right class methodsFor: 'private' stamp: 'JDS 10/6/2021 00:12:36'!
headingSouthProcess: aRover

	aRover headTo: West! !

!Right class methodsFor: 'private' stamp: 'JDS 10/6/2021 00:12:22'!
headingWestProcess: aRover

	aRover headTo: North! !


!Right class methodsFor: 'testing' stamp: 'JDS 10/5/2021 23:24:03'!
canHandle: anOrder

	^ anOrder = $r! !
