!classDefinition: #MarsRoverTest category: 'MarsRover-WithHeading'!
TestCase subclass: #MarsRoverTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:21:23'!
test01DoesNotMoveWhenNoCommand

	self 
		assertIsAt: 1@2 
		heading: self north 
		afterProcessing: '' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:12'!
test02IsAtFailsForDifferentPosition

	| marsRover |
	
	marsRover := MarsRover at: 1@1 heading: self north . 
	
	self deny: (marsRover isAt: 1@2 heading: self north)! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:31'!
test03IsAtFailsForDifferentHeading

	| marsRover |
	
	marsRover := MarsRover at: 1@1 heading: self north . 
	
	self deny: (marsRover isAt: 1@1 heading: self south)! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:30:17'!
test04IncrementsYAfterMovingForwardWhenHeadingNorth

	self 
		assertIsAt: 1@3 
		heading: self north 
		afterProcessing: 'f' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:30:11'!
test06DecrementsYAfterMovingBackwardsWhenHeadingNorth

	self 
		assertIsAt: 1@1 
		heading: self north 
		afterProcessing: 'b' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:59'!
test07PointToEashAfterRotatingRightWhenHeadingNorth

	self 
		assertIsAt: 1@2 
		heading: self east 
		afterProcessing: 'r' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:51'!
test08PointsToWestAfterRotatingLeftWhenPointingNorth

	self 
		assertIsAt: 1@2 
		heading: self west 
		afterProcessing: 'l' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:45'!
test09DoesNotProcessInvalidCommand

	| marsRover |
	
	marsRover := MarsRover at: 1@2 heading: self north.
	
	self 
		should: [ marsRover process: 'x' ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: marsRover invalidCommandErrorDescription.
			self assert: (marsRover isAt: 1@2 heading: self north) ]! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:39'!
test10CanProcessMoreThanOneCommand

	self 
		assertIsAt: 1@4 
		heading: self north 
		afterProcessing: 'ff' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:31'!
test11IncrementsXAfterMovingForwareWhenHeadingEast

	self 
		assertIsAt: 2@2 
		heading: self east 
		afterProcessing: 'f' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:19'!
test12DecrementsXAfterMovingBackwardWhenHeadingEast

	self 
		assertIsAt: 0@2 
		heading: self east 
		afterProcessing: 'b' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:14'!
test13PointsToSouthAfterRotatingRightWhenHeadingEast

		self 
		assertIsAt: 1@2 
		heading: self south 
		afterProcessing: 'r' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:05'!
test14PointsToNorthAfterRotatingLeftWhenPointingEast

		self 
		assertIsAt: 1@2 
		heading: self north 
		afterProcessing: 'l' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:00'!
test15ForwardBackwardsAndRotateRightWorkAsExpectedWhenPointingSouth

	self 
		assertIsAt: 1@1 
		heading: self west 
		afterProcessing: 'ffblrr' 
		whenStartingAt: 1@2 
		heading: self south 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:52'!
test16ForwardBackwardsAndRotateRightWorkAsExpectedWhenPointingWest

	self 
		assertIsAt: 0@2 
		heading: self north 
		afterProcessing: 'ffblrr' 
		whenStartingAt: 1@2 
		heading: self west 
! !


!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:31'!
east

	^ MarsRoverHeadingEast ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:38'!
north

	^ MarsRoverHeadingNorth ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:45'!
south

	^ MarsRoverHeadingSouth ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:54'!
west

	^ MarsRoverHeadingWest ! !


!MarsRoverTest methodsFor: 'assertions' stamp: 'HAW 10/7/2021 20:20:47'!
assertIsAt: newPosition heading: newHeadingType afterProcessing: commands whenStartingAt: startPosition heading: startHeadingType

	| marsRover |
	
	marsRover := MarsRover at: startPosition heading: startHeadingType. 
	
	marsRover process: commands.
	
	self assert: (marsRover isAt: newPosition heading: newHeadingType)! !


!classDefinition: #TrackingTest category: 'MarsRover-WithHeading'!
TestCase subclass: #TrackingTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!TrackingTest methodsFor: 'testing' stamp: 'JDS 10/27/2021 18:36:01'!
test01CheckPositionChangesLogAfterMovingForwardIsLogged

	| marsRover positionTracker logReadStream |
	marsRover _  MarsRover  at: 1@1 heading: MarsRoverHeadingNorth.
	
	positionTracker _ self trackingPositionChangesOf: marsRover.
	
	marsRover  process: 'f'.
	
	logReadStream _ ReadStream on: positionTracker contents.
	
	self assert: '1@2' equals: logReadStream nextLine. 
	self assert: logReadStream atEnd.! !

!TrackingTest methodsFor: 'testing' stamp: 'JDS 10/27/2021 18:36:16'!
test02CheckPositionChangesLogAfterMovingBackwardIsLogged

	| marsRover positionTracker logReadStream |
	marsRover _  MarsRover  at: 1@1 heading: MarsRoverHeadingNorth.
	
	positionTracker _ self trackingPositionChangesOf: marsRover.
	
	marsRover  process: 'b'.
	
	logReadStream _ ReadStream on: positionTracker contents.
	
	self assert: '1@0' equals: logReadStream nextLine. 
	self assert: logReadStream atEnd.! !

!TrackingTest methodsFor: 'testing' stamp: 'JDS 10/27/2021 20:33:08'!
test03HeadingChangesLogAfterMovingForwardTwiceIsNotLogged

	| marsRover x logReadStream |
	marsRover _  MarsRover  at: 1@1 heading: MarsRoverHeadingNorth.
	
	x _ self trackingHeadingChangesOf: marsRover.
	
	marsRover  process: 'ff'.
	
	logReadStream _ ReadStream on: x contents.
	
	self assert: logReadStream atEnd.! !

!TrackingTest methodsFor: 'testing' stamp: 'JDS 10/27/2021 20:33:24'!
test04CheckHeadingChangesLogAfterRigthRotateIsLogged

	| marsRover headingTracker logReadStream |
	marsRover _  MarsRover  at: 1@1 heading: MarsRoverHeadingNorth.
	
	headingTracker _ self trackingHeadingChangesOf: marsRover.
	
	marsRover  process: 'ffr'.
	
	logReadStream _ ReadStream on: headingTracker contents.
	
	self assert: 'East' equals: logReadStream nextLine. 
	self assert: logReadStream atEnd.! !

!TrackingTest methodsFor: 'testing' stamp: 'JDS 10/27/2021 21:29:32'!
test05CheckPositionAndHeadingChangesLogsAfterForwardAndRigthRotateIsLogged

	| marsRover headingTracker logReadStream |
	marsRover _  MarsRover  at: 1@1 heading: MarsRoverHeadingNorth.
	
	headingTracker _ self trackingPositionAndHeadingChangesOf: marsRover.
	
	marsRover  process: 'fr'.
	
	logReadStream _ ReadStream on: headingTracker contents.
	
	self assert: '1@2' equals: logReadStream nextLine. 
	self assert: 'East' equals: logReadStream nextLine. 
	self assert: logReadStream atEnd.! !

!TrackingTest methodsFor: 'testing' stamp: 'JDS 10/27/2021 21:25:26'!
test06CheckPositionAndHeadingChangesLogAfterMultipleMovesIsLogged

	| marsRover headingTracker logReadStream |
	marsRover _  MarsRover  at: 1@1 heading: MarsRoverHeadingEast.
	
	headingTracker _ self trackingPositionAndHeadingChangesOf: marsRover.

	marsRover  process: 'frfrfr'.
	
	logReadStream _ ReadStream on: headingTracker contents.
	
	self assert: '2@1' equals: logReadStream nextLine.
	self assert: 'South' equals: logReadStream nextLine.
	self assert: '2@0' equals: logReadStream nextLine. 
	self assert: 'West' equals: logReadStream nextLine.
	self assert: '1@0' equals: logReadStream nextLine. 
	self assert: 'North' equals: logReadStream nextLine.
	self assert: logReadStream atEnd.
	
! !

!TrackingTest methodsFor: 'testing' stamp: 'JDS 10/27/2021 22:45:35'!
test07CheckPositionChangesTextFieldAfterMovingForwardIsLogged

	| marsRover positionTFTracker |
	marsRover _  MarsRover  at: 1@1 heading: MarsRoverHeadingNorth.
	
	positionTFTracker _ self trackingTFPositionChangesOf: marsRover.
	
	marsRover  process: 'f'.
	
	self assert: 1@2 equals: positionTFTracker positionTextFieldModel. 
	! !

!TrackingTest methodsFor: 'testing' stamp: 'JDS 10/27/2021 22:47:57'!
test08CheckHeadingChangesTextFieldAfterRightRotateIsLogged

	| marsRover headingTFTracker |
	marsRover _  MarsRover  at: 1@1 heading: MarsRoverHeadingNorth.
	
	headingTFTracker _ self trackingTFHeadingChangesOf: marsRover.
	
	marsRover  process: 'r'.
	
	self assert: MarsRoverHeadingEast equals: headingTFTracker headingTextFieldModel. 
	! !

!TrackingTest methodsFor: 'testing' stamp: 'JDS 10/28/2021 16:44:42'!
test09CheckPositionAndHeadingChangesTextFieldAfterMoveForwardAndRightRotateIsLogged

	| marsRover tracker |
	marsRover _  MarsRover  at: 1@1 heading: MarsRoverHeadingNorth.
	
	tracker _ self trackingTFPositionAndHeadingChangesOf: marsRover.
	
	marsRover  process: 'frfr'.
	
	self assert: 2@2 equals: tracker positionTextFieldModel. 
	self assert: MarsRoverHeadingSouth equals: tracker headingTextFieldModel. 
	! !

!TrackingTest methodsFor: 'testing' stamp: 'AE 10/28/2021 16:39:24'!
test10_funcional_CheckPositionAndHeadingChangesLogAfterMultipleMovesIsLogged

	| marsRover tracker |
	marsRover _  MarsRover  at: 1@1 heading: MarsRoverHeadingEast.
	
	tracker _ self trackingTFPositionAndHeadingChangesOf: marsRover.

	marsRover  process: 'frfr'.
	
	self assert: 2@0 equals: tracker positionTextFieldModel. 
	self assert: MarsRoverHeadingWest equals: tracker headingTextFieldModel. 
! !


!TrackingTest methodsFor: 'associating' stamp: 'AE 10/28/2021 14:21:36'!
trackingHeadingChangesOf: aMarsRover 
	
	| tracker | 
	tracker _ LogTracker heading.
	aMarsRover trackChangesTo: tracker.
	^ tracker! !

!TrackingTest methodsFor: 'associating' stamp: 'AE 10/28/2021 14:21:52'!
trackingPositionAndHeadingChangesOf: aMarsRover 
		
	| tracker | 
	tracker _ LogTracker headingAndPosition .
	aMarsRover trackChangesTo: tracker.
	^ tracker! !

!TrackingTest methodsFor: 'associating' stamp: 'AE 10/28/2021 14:22:21'!
trackingPositionChangesOf: aMarsRover 
	
	| tracker | 
	tracker _ LogTracker position.
	aMarsRover trackChangesTo: tracker.
	^ tracker! !

!TrackingTest methodsFor: 'associating' stamp: 'AE 10/28/2021 16:14:40'!
trackingTFPositionAndHeadingChangesOf: aMarsRover 
	|tracker| 
	tracker _ TextFieldTracker headingAndPosition.
	aMarsRover trackChangesTo: tracker.
	^tracker
	! !


!TrackingTest methodsFor: 'as yet unclassified' stamp: 'AE 10/28/2021 14:44:35'!
trackingTFHeadingChangesOf: aMarsRover 
	
	| tracker |
	
	tracker _ TextFieldTracker heading.
	aMarsRover trackChangesTo: tracker.
	^tracker
	! !

!TrackingTest methodsFor: 'as yet unclassified' stamp: 'AE 10/28/2021 14:45:01'!
trackingTFPositionChangesOf: aMarsRover 
	
	| tracker |

	tracker _ TextFieldTracker position.	
	aMarsRover trackChangesTo: tracker.
	^tracker! !


!classDefinition: #MarsRover category: 'MarsRover-WithHeading'!
Object subclass: #MarsRover
	instanceVariableNames: 'position head trackers'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRover methodsFor: 'exceptions' stamp: 'HAW 6/30/2018 19:48:45'!
invalidCommandErrorDescription
	
	^'Invalid command'! !

!MarsRover methodsFor: 'exceptions' stamp: 'HAW 6/30/2018 19:50:26'!
signalInvalidCommand
	
	self error: self invalidCommandErrorDescription ! !


!MarsRover methodsFor: 'initialization' stamp: 'AE 10/28/2021 14:17:24'!
initializeAt: aPosition heading: aHeadingType

	position := aPosition.
	head := aHeadingType for: self.
	
	trackers _ OrderedCollection  new.
	headingTrackers _ OrderedCollection new.! !


!MarsRover methodsFor: 'heading' stamp: 'AE 10/28/2021 14:42:21'!
changeHeadTo: aNewHeading
	
	head := aNewHeading for: self.
	
	trackers do: [ :aTracker | aTracker updateHeading: head ].
! !

!MarsRover methodsFor: 'heading' stamp: 'JDS 10/27/2021 21:36:35'!
headEast
	
	self changeHeadTo: MarsRoverHeadingEast.! !

!MarsRover methodsFor: 'heading' stamp: 'JDS 10/27/2021 21:35:41'!
headNorth
	
	self changeHeadTo: MarsRoverHeadingNorth.
! !

!MarsRover methodsFor: 'heading' stamp: 'JDS 10/27/2021 21:36:03'!
headSouth
	
	self changeHeadTo: MarsRoverHeadingSouth.
! !

!MarsRover methodsFor: 'heading' stamp: 'JDS 10/27/2021 21:36:16'!
headWest
	
	self changeHeadTo: MarsRoverHeadingWest.
! !

!MarsRover methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	head rotateLeft! !

!MarsRover methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	head rotateRight! !


!MarsRover methodsFor: 'testing' stamp: 'HAW 10/7/2021 20:16:32'!
isAt: aPosition heading: aHeadingType

	^position = aPosition and: [ head isHeading: aHeadingType ]! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:16:51'!
isBackwardCommand: aCommand

	^aCommand = $b! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:16:19'!
isForwardCommand: aCommand

	^aCommand = $f ! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:17:51'!
isRotateLeftCommand: aCommand

	^aCommand = $l! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:17:21'!
isRotateRightCommand: aCommand

	^aCommand = $r! !


!MarsRover methodsFor: 'moving' stamp: 'JDS 10/27/2021 21:13:52'!
moveBackward
	
	head moveBackward.
	! !

!MarsRover methodsFor: 'moving' stamp: 'JDS 10/27/2021 21:32:39'!
moveEast
	
	self movePositionWith: (1@0)
	! !

!MarsRover methodsFor: 'moving' stamp: 'JDS 10/27/2021 20:26:30'!
moveForward
	
	head moveForward	! !

!MarsRover methodsFor: 'moving' stamp: 'JDS 10/27/2021 21:33:02'!
moveNorth
	
	self movePositionWith: (0@1).
! !

!MarsRover methodsFor: 'moving' stamp: 'AE 10/28/2021 14:18:08'!
movePositionWith: anOffset 
	
	position := position + anOffset.
	
	trackers do: [ :aTracker | aTracker updatePosition: position].! !

!MarsRover methodsFor: 'moving' stamp: 'JDS 10/27/2021 21:33:21'!
moveSouth
	
	self movePositionWith: (0@-1).
! !

!MarsRover methodsFor: 'moving' stamp: 'JDS 10/27/2021 21:33:33'!
moveWest
	
	self movePositionWith: (-1@0).! !


!MarsRover methodsFor: 'command processing' stamp: 'HAW 6/30/2018 19:48:26'!
process: aSequenceOfCommands

	aSequenceOfCommands do: [:aCommand | self processCommand: aCommand ]
! !

!MarsRover methodsFor: 'command processing' stamp: 'HAW 8/22/2019 12:08:50'!
processCommand: aCommand

	(self isForwardCommand: aCommand) ifTrue: [ ^ self moveForward ].
	(self isBackwardCommand: aCommand) ifTrue: [ ^ self moveBackward ].
	(self isRotateRightCommand: aCommand) ifTrue: [ ^ self rotateRight ].
	(self isRotateLeftCommand: aCommand) ifTrue: [ ^ self rotateLeft ].

	self signalInvalidCommand.! !


!MarsRover methodsFor: 'events-registering' stamp: 'AE 10/28/2021 14:21:20'!
trackChangesTo: aTracker 
	
	trackers add: aTracker. 
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRover class' category: 'MarsRover-WithHeading'!
MarsRover class
	instanceVariableNames: 'headings'!

!MarsRover class methodsFor: 'instance creation' stamp: 'HAW 10/7/2021 20:10:30'!
at: aPosition heading: aHeadingType
	
	^self new initializeAt: aPosition heading: aHeadingType! !


!classDefinition: #MarsRoverHeading category: 'MarsRover-WithHeading'!
Object subclass: #MarsRoverHeading
	instanceVariableNames: 'marsRover'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeading methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft

	self subclassResponsibility ! !

!MarsRoverHeading methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight

	self subclassResponsibility ! !


!MarsRoverHeading methodsFor: 'testing' stamp: 'HAW 10/7/2021 20:15:38'!
isHeading: aHeadingType

	^self isKindOf: aHeadingType ! !


!MarsRoverHeading methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward

	self subclassResponsibility ! !

!MarsRoverHeading methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward

	self subclassResponsibility ! !


!MarsRoverHeading methodsFor: 'initialization' stamp: 'HAW 10/7/2021 20:11:59'!
initializeFor: aMarsRover 
	
	marsRover := aMarsRover.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverHeading class' category: 'MarsRover-WithHeading'!
MarsRoverHeading class
	instanceVariableNames: ''!

!MarsRoverHeading class methodsFor: 'instance creation' stamp: 'HAW 10/7/2021 20:11:35'!
for: aMarsRover 
	
	^self new initializeFor: aMarsRover ! !


!classDefinition: #MarsRoverHeadingEast category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingEast
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingEast methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveWest! !

!MarsRoverHeadingEast methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveEast! !


!MarsRoverHeadingEast methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headNorth! !

!MarsRoverHeadingEast methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headSouth! !


!MarsRoverHeadingEast methodsFor: 'printing' stamp: 'JDS 10/27/2021 19:02:14'!
printOn: aStream

	aStream nextPutAll: 'East'! !


!classDefinition: #MarsRoverHeadingNorth category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingNorth
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingNorth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveSouth! !

!MarsRoverHeadingNorth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveNorth! !


!MarsRoverHeadingNorth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headWest! !

!MarsRoverHeadingNorth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headEast! !


!MarsRoverHeadingNorth methodsFor: 'printing' stamp: 'JDS 10/27/2021 21:20:14'!
printOn: aStream

	aStream nextPutAll: 'North'! !


!classDefinition: #MarsRoverHeadingSouth category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingSouth
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingSouth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveNorth! !

!MarsRoverHeadingSouth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveSouth! !


!MarsRoverHeadingSouth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headEast! !

!MarsRoverHeadingSouth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headWest! !


!MarsRoverHeadingSouth methodsFor: 'printing' stamp: 'JDS 10/27/2021 21:20:53'!
printOn: aStream

	aStream nextPutAll: 'South'! !


!classDefinition: #MarsRoverHeadingWest category: 'MarsRover-WithHeading'!
MarsRoverHeading subclass: #MarsRoverHeadingWest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverHeadingWest methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward

	^marsRover moveEast! !

!MarsRoverHeadingWest methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveWest! !


!MarsRoverHeadingWest methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headSouth! !

!MarsRoverHeadingWest methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headNorth! !


!MarsRoverHeadingWest methodsFor: 'printing' stamp: 'JDS 10/27/2021 21:21:12'!
printOn: aStream

	aStream nextPutAll: 'West'! !


!classDefinition: #MarsRoverStatusView category: 'MarsRover-WithHeading'!
Object subclass: #MarsRoverStatusView
	instanceVariableNames: 'positionTextFieldModel headingTextFieldModel'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!MarsRoverStatusView methodsFor: 'as yet unclassified' stamp: 'JDS 10/27/2021 22:58:45'!
headingTextFieldModel: aClass 
	
	headingTextFieldModel _ aClass! !

!MarsRoverStatusView methodsFor: 'as yet unclassified' stamp: 'JDS 10/27/2021 22:42:46'!
positionTextFieldModel: aPosition

	positionTextFieldModel _ aPosition.
	! !


!MarsRoverStatusView methodsFor: 'accessing' stamp: 'JDS 10/27/2021 22:53:33'!
headingTextFieldModel
	^headingTextFieldModel! !

!MarsRoverStatusView methodsFor: 'accessing' stamp: 'JDS 10/27/2021 22:44:10'!
positionTextFieldModel
	
	^positionTextFieldModel! !


!classDefinition: #Tracker category: 'MarsRover-WithHeading'!
Object subclass: #Tracker
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!Tracker methodsFor: 'event-reporting' stamp: 'JDS 10/28/2021 01:10:39'!
updateHeading: aHeading

	self subclassResponsibility ! !

!Tracker methodsFor: 'event-reporting' stamp: 'JDS 10/28/2021 01:10:54'!
updatePosition: aPosition

	self subclassResponsibility ! !


!classDefinition: #LogTracker category: 'MarsRover-WithHeading'!
Tracker subclass: #LogTracker
	instanceVariableNames: 'stream'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!LogTracker methodsFor: 'initialization' stamp: 'AE 10/28/2021 14:38:09'!
initialize

	stream _ WriteStream on: ''.! !


!LogTracker methodsFor: 'accessing' stamp: 'JDS 10/27/2021 23:34:26'!
contents
	
	^ stream contents! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'LogTracker class' category: 'MarsRover-WithHeading'!
LogTracker class
	instanceVariableNames: ''!

!LogTracker class methodsFor: 'instance creation' stamp: 'AE 10/28/2021 14:38:27'!
heading
	^ LogHeadingTracker new.
	! !

!LogTracker class methodsFor: 'instance creation' stamp: 'AE 10/28/2021 14:38:34'!
headingAndPosition
	^ LogHeadingPositionTracker new.	! !

!LogTracker class methodsFor: 'instance creation' stamp: 'AE 10/28/2021 14:38:43'!
position
	^ LogPositionTracker new.
	! !


!classDefinition: #LogHeadingPositionTracker category: 'MarsRover-WithHeading'!
LogTracker subclass: #LogHeadingPositionTracker
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!LogHeadingPositionTracker methodsFor: 'event-reporting' stamp: 'AE 10/28/2021 11:54:41'!
updateHeading: aHeading

	stream nextPutAll: aHeading printString; newLine.

	! !

!LogHeadingPositionTracker methodsFor: 'event-reporting' stamp: 'AE 10/28/2021 16:10:31'!
updatePosition: aPosition

	stream nextPutAll: aPosition printString ; newLine.
	! !


!classDefinition: #LogHeadingTracker category: 'MarsRover-WithHeading'!
LogTracker subclass: #LogHeadingTracker
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!LogHeadingTracker methodsFor: 'event-reporting' stamp: 'AE 10/28/2021 11:54:34'!
updateHeading: aHeading

	stream nextPutAll: aHeading printString; newLine.

	! !

!LogHeadingTracker methodsFor: 'event-reporting' stamp: 'AE 10/28/2021 14:41:12'!
updatePosition: aPosition
! !


!classDefinition: #LogPositionTracker category: 'MarsRover-WithHeading'!
LogTracker subclass: #LogPositionTracker
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!LogPositionTracker methodsFor: 'event-reporting' stamp: 'AE 10/28/2021 14:36:24'!
updateHeading: aHeading

	! !

!LogPositionTracker methodsFor: 'event-reporting' stamp: 'AE 10/28/2021 11:55:02'!
updatePosition: aPosition

	stream nextPutAll: aPosition printString ; newLine.! !


!classDefinition: #TextFieldTracker category: 'MarsRover-WithHeading'!
Tracker subclass: #TextFieldTracker
	instanceVariableNames: 'statusView'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!TextFieldTracker methodsFor: 'initialization' stamp: 'AE 10/28/2021 14:35:04'!
initialize
	
	statusView := MarsRoverStatusView new.! !


!TextFieldTracker methodsFor: 'accessing' stamp: 'JDS 10/28/2021 00:44:35'!
headingTextFieldModel

	 ^ statusView headingTextFieldModel! !

!TextFieldTracker methodsFor: 'accessing' stamp: 'JDS 10/28/2021 00:30:59'!
positionTextFieldModel

	^ statusView positionTextFieldModel! !


!TextFieldTracker methodsFor: 'events-registering' stamp: 'JDS 10/28/2021 01:00:22'!
updateHeading: aHeading
	
	statusView headingTextFieldModel: aHeading class.! !

!TextFieldTracker methodsFor: 'events-registering' stamp: 'JDS 10/28/2021 00:24:59'!
updatePosition: aPosition 
	
	statusView positionTextFieldModel: aPosition.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TextFieldTracker class' category: 'MarsRover-WithHeading'!
TextFieldTracker class
	instanceVariableNames: ''!

!TextFieldTracker class methodsFor: 'instance creation' stamp: 'AE 10/28/2021 14:44:35'!
heading

	^TextFieldHeadingTracker new.
	! !

!TextFieldTracker class methodsFor: 'instance creation' stamp: 'AE 10/28/2021 16:15:04'!
headingAndPosition

	^TextFieldHeadingPositionTracker new.
	! !

!TextFieldTracker class methodsFor: 'instance creation' stamp: 'AE 10/28/2021 14:45:01'!
position

	^TextFieldPositionTracker new.
	! !


!classDefinition: #TextFieldHeadingPositionTracker category: 'MarsRover-WithHeading'!
TextFieldTracker subclass: #TextFieldHeadingPositionTracker
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!TextFieldHeadingPositionTracker methodsFor: 'as yet unclassified' stamp: 'AE 10/28/2021 16:06:58'!
updateHeading: aHeading

	statusView headingTextFieldModel: aHeading class.
	! !

!TextFieldHeadingPositionTracker methodsFor: 'as yet unclassified' stamp: 'AE 10/28/2021 16:09:54'!
updatePosition: aPosition 
	
	statusView positionTextFieldModel: aPosition.
	! !


!classDefinition: #TextFieldHeadingTracker category: 'MarsRover-WithHeading'!
TextFieldTracker subclass: #TextFieldHeadingTracker
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!TextFieldHeadingTracker methodsFor: 'event-reporting' stamp: 'AE 10/28/2021 14:30:31'!
updateHeading: aHeading
	
	statusView headingTextFieldModel: aHeading class.
	! !

!TextFieldHeadingTracker methodsFor: 'event-reporting' stamp: 'AE 10/28/2021 14:30:40'!
updatePosition: aHeading
	! !


!classDefinition: #TextFieldPositionTracker category: 'MarsRover-WithHeading'!
TextFieldTracker subclass: #TextFieldPositionTracker
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-WithHeading'!

!TextFieldPositionTracker methodsFor: 'as yet unclassified' stamp: 'AE 10/28/2021 16:08:21'!
updateHeading: aHeading
! !

!TextFieldPositionTracker methodsFor: 'as yet unclassified' stamp: 'AE 10/28/2021 14:29:35'!
updatePosition: aPosition 
	
	statusView positionTextFieldModel: aPosition.! !
