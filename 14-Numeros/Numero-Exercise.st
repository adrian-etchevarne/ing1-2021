!classDefinition: #NumeroTest category: 'Numero-Exercise'!
TestCase subclass: #NumeroTest
	instanceVariableNames: 'zero one two four oneFifth oneHalf five twoFifth twoTwentyfifth fiveHalfs three eight negativeOne negativeTwo'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:11'!
test01isCeroReturnsTrueWhenAskToZero

	self assert: zero isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:12'!
test02isCeroReturnsFalseWhenAskToOthersButZero

	self deny: one isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:13'!
test03isOneReturnsTrueWhenAskToOne

	self assert: one isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:13'!
test04isOneReturnsFalseWhenAskToOtherThanOne

	self deny: zero isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:14'!
test05EnteroAddsWithEnteroCorrectly

	self assert: one + one equals: two! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:18'!
test06EnteroMultipliesWithEnteroCorrectly

	self assert: two * two equals: four! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:20'!
test07EnteroDividesEnteroCorrectly

	self assert: two / two equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'AE 9/13/2021 02:15:21'!
test08FraccionAddsWithFraccionCorrectly
"
    La suma de fracciones es:
	 
	a/b + c/d = (a.d + c.b) / (b.d)
	 
	SI ESTAN PENSANDO EN LA REDUCCION DE FRACCIONES NO SE PREOCUPEN!!
	TODAVIA NO SE ESTA TESTEANDO ESE CASO
"
	| sevenTenths result|

	sevenTenths := (Entero with: 7) / (Entero with: 10).

       result := oneFifth + oneHalf.

	self assert: oneFifth + oneHalf equals: sevenTenths! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:52'!
test09FraccionMultipliesWithFraccionCorrectly
"
    La multiplicacion de fracciones es:
	 
	(a/b) * (c/d) = (a.c) / (b.d)
"

	self assert: oneFifth * twoFifth equals: twoTwentyfifth! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:56'!
test10FraccionDividesFraccionCorrectly
"
    La division de fracciones es:
	 
	(a/b) / (c/d) = (a.d) / (b.c)
"

	self assert: oneHalf / oneFifth equals: fiveHalfs! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:07'!
test11EnteroAddsFraccionCorrectly
"
	Ahora empieza la diversion!!
"

	self assert: one + oneFifth equals: (Entero with: 6) / (Entero with: 5)! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:07'!
test12FraccionAddsEnteroCorrectly

	self assert: oneFifth + one equals: (Entero with: 6) / (Entero with: 5)! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:50'!
test13EnteroMultipliesFraccionCorrectly

	self assert: two * oneFifth equals: twoFifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:52'!
test14FraccionMultipliesEnteroCorrectly

	self assert: oneFifth * two equals: twoFifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:57'!
test15EnteroDividesFraccionCorrectly

	self assert: one / twoFifth equals: fiveHalfs  ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:59'!
test16FraccionDividesEnteroCorrectly

	self assert: twoFifth / five equals: twoTwentyfifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:38'!
test17AFraccionCanBeEqualToAnEntero

	self assert: two equals: four / two! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:39'!
test18AparentFraccionesAreEqual

	self assert: oneHalf equals: two / four! !

!NumeroTest methodsFor: 'tests' stamp: 'AE 9/13/2021 02:26:08'!
test19AddingFraccionesCanReturnAnEntero

	| result | 
	result := oneHalf + oneHalf.
	
	self assert: oneHalf + oneHalf equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:42'!
test20MultiplyingFraccionesCanReturnAnEntero

	self assert: (two/five) * (five/two) equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:42'!
test21DividingFraccionesCanReturnAnEntero

	self assert: oneHalf / oneHalf equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:43'!
test22DividingEnterosCanReturnAFraccion

	self assert: two / four equals: oneHalf! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:46'!
test23CanNotDivideEnteroByZero

	self 
		should: [ one / zero ]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Numero canNotDivideByZeroErrorDescription ]
	! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:46'!
test24CanNotDivideFraccionByZero

	self 
		should: [ oneHalf / zero ]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Numero canNotDivideByZeroErrorDescription ]
	! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:50'!
test25AFraccionCanNotBeZero

	self deny: oneHalf isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:50'!
test26AFraccionCanNotBeOne

	self deny: oneHalf isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 4/15/2021 16:45:35'!
test27EnteroSubstractsEnteroCorrectly

	self assert: four - one equals: three! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:47:41'!
test28FraccionSubstractsFraccionCorrectly
	
	self assert: twoFifth - oneFifth equals: oneFifth.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:48:00'!
test29EnteroSubstractsFraccionCorrectly

	self assert: one - oneHalf equals: oneHalf! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:05'!
test30FraccionSubstractsEnteroCorrectly

	| sixFifth |
	
	sixFifth := (Entero with: 6) / (Entero with: 5).
	
	self assert: sixFifth - one equals: oneFifth! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:08'!
test31SubstractingFraccionesCanReturnAnEntero

	| threeHalfs |
	
	threeHalfs := (Entero with: 3) / (Entero with: 2).
	
	self assert: threeHalfs - oneHalf equals: one.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:48:48'!
test32SubstractingSameEnterosReturnsZero

	self assert: one - one equals: zero.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:01'!
test33SubstractingSameFraccionesReturnsZero

	self assert: oneHalf - oneHalf equals: zero.! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:14'!
test34SubstractingAHigherValueToANumberReturnsANegativeNumber

	| negativeThreeHalfs |
	
	negativeThreeHalfs := (Entero with: -3) / (Entero with: 2).	

	self assert: one - fiveHalfs equals: negativeThreeHalfs.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:23'!
test35FibonacciZeroIsOne

	self assert: zero fibonacci equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:32'!
test36FibonacciOneIsOne

	self assert: one fibonacci equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:39'!
test37FibonacciEnteroReturnsAddingPreviousTwoFibonacciEnteros

	self assert: four fibonacci equals: five.
	self assert: three fibonacci equals: three. 
	self assert: five fibonacci equals: four fibonacci + three fibonacci.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:47'!
test38FibonacciNotDefinedForNegativeNumbers

	self 
		should: [negativeOne fibonacci]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Entero negativeFibonacciErrorDescription ].! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:55'!
test39NegationOfEnteroIsCorrect

	self assert: two negated equals: negativeTwo.
		! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:03'!
test40NegationOfFraccionIsCorrect

	self assert: oneHalf negated equals: negativeOne / two.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:11'!
test41SignIsCorrectlyAssignedToFractionWithTwoNegatives

	self assert: oneHalf equals: (negativeOne / negativeTwo)! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:17'!
test42SignIsCorrectlyAssignedToFractionWithNegativeDivisor

	self assert: oneHalf negated equals: (one / negativeTwo)! !


!NumeroTest methodsFor: 'setup' stamp: 'NR 9/23/2018 23:46:28'!
setUp

	zero := Entero with: 0.
	one := Entero with: 1.
	two := Entero with: 2.
	three:= Entero with: 3.
	four := Entero with: 4.
	five := Entero with: 5.
	eight := Entero with: 8.
	negativeOne := Entero with: -1.
	negativeTwo := Entero with: -2.
	
	oneHalf := one / two.
	oneFifth := one / five.
	twoFifth := two / five.
	twoTwentyfifth := two / (Entero with: 25).
	fiveHalfs := five / two.
	! !


!classDefinition: #Numero category: 'Numero-Exercise'!
Object subclass: #Numero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:48'!
* aMultiplier

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:49'!
+ anAdder

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'NR 9/23/2018 22:21:28'!
- aSubtrahend

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:49'!
/ aDivisor

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:48'!
invalidNumberType

	self error: self class invalidNumberTypeErrorDescription! !

!Numero methodsFor: 'arithmetic operations' stamp: 'NR 9/23/2018 23:37:13'!
negated
	
	^self * (Entero with: -1)! !


!Numero methodsFor: 'testing' stamp: 'NR 9/23/2018 23:36:49'!
isNegative

	self subclassResponsibility ! !

!Numero methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:49'!
isOne

	self subclassResponsibility ! !

!Numero methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:49'!
isZero

	self subclassResponsibility ! !


!Numero methodsFor: 'private' stamp: 'JDS 9/15/2021 23:17:03'!
addAFraction: aFraccion 

	self subclassResponsibility.
	! !

!Numero methodsFor: 'private' stamp: 'JDS 9/15/2021 23:04:30'!
addAnEntero: anEnteroAdder
 
	self subclassResponsibility.
	! !

!Numero methodsFor: 'private' stamp: 'JDS 9/15/2021 23:00:34'!
divideAFraccion: aFraccionDividend

	self subclassResponsibility.! !

!Numero methodsFor: 'private' stamp: 'JDS 9/15/2021 23:01:37'!
divideAnEntero: anEnteroDividend

	self subclassResponsibility.! !

!Numero methodsFor: 'private' stamp: 'JDS 9/15/2021 23:03:44'!
multiplyAFraccion: aFraccionMultiplier

	self subclassResponsibility.! !

!Numero methodsFor: 'private' stamp: 'JDS 9/15/2021 23:17:17'!
multiplyAnEntero: anEntero 

	self subclassResponsibility.! !

!Numero methodsFor: 'private' stamp: 'JDS 9/15/2021 23:02:55'!
substractFromAFraccion: aFraccionMinuend

	self subclassResponsibility.! !

!Numero methodsFor: 'private' stamp: 'JDS 9/15/2021 23:17:30'!
substractFromAnEntero: anSubstrahendEntero

	self subclassResponsibility.
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Numero class' category: 'Numero-Exercise'!
Numero class
	instanceVariableNames: ''!

!Numero class methodsFor: 'error descriptions' stamp: 'NR 4/15/2021 16:42:02'!
canNotDivideByZeroErrorDescription

	^'No se puede dividir por cero!!!!!!'! !

!Numero class methodsFor: 'error descriptions' stamp: 'NR 4/15/2021 16:42:09'!
invalidNumberTypeErrorDescription
	
	^ 'Tipo de n�mero inv�lido!!!!!!'! !


!classDefinition: #Entero category: 'Numero-Exercise'!
Numero subclass: #Entero
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Entero methodsFor: 'arithmetic operations' stamp: 'AE 9/14/2021 18:27:35'!
* aMultiplier 
	
	^ aMultiplier multiplyAnEntero: self.
	! !

!Entero methodsFor: 'arithmetic operations' stamp: 'JDS 9/15/2021 23:04:30'!
+ anAdder 
	
	^anAdder addAnEntero: self.
! !

!Entero methodsFor: 'arithmetic operations' stamp: 'AE 9/14/2021 18:28:09'!
- aSubtrahend 

	^aSubtrahend substractFromAnEntero: self.
	! !

!Entero methodsFor: 'arithmetic operations' stamp: 'AE 9/14/2021 18:26:52'!
/ aDivisor 
	
	^aDivisor divideAnEntero: self.
	! !

!Entero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 21:55'!
// aDivisor 
	
	^self class with: value // aDivisor integerValue! !

!Entero methodsFor: 'arithmetic operations' stamp: 'JDS 9/15/2021 22:46:46'!
fibonacci

	^self subclassResponsibility! !

!Entero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 21:00'!
greatestCommonDivisorWith: anEntero 
	
	^self class with: (value gcd: anEntero integerValue)! !


!Entero methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 21:01'!
= anObject

	^(anObject isKindOf: self class) and: [ value = anObject integerValue ]! !

!Entero methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:17'!
hash

	^value hash! !


!Entero methodsFor: 'initialization' stamp: 'AE 9/14/2021 19:32:42'!
initializeWith: aValue 
	
	value := aValue! !


!Entero methodsFor: 'value' stamp: 'HernanWilkinson 5/7/2016 21:02'!
integerValue

	"Usamos integerValue en vez de value para que no haya problemas con el mensaje value implementado en Object - Hernan"
	
	^value! !


!Entero methodsFor: 'printing' stamp: 'HAW 9/24/2018 08:53:19'!
printOn: aStream

	aStream print: value ! !


!Entero methodsFor: 'testing' stamp: 'AE 9/14/2021 19:37:03'!
isNegative
	
	^false! !

!Entero methodsFor: 'testing' stamp: 'AE 9/14/2021 19:36:40'!
isOne
	
	^false! !

!Entero methodsFor: 'testing' stamp: 'AE 9/14/2021 19:36:33'!
isZero
	
	^false! !


!Entero methodsFor: 'implementative-arithmetic' stamp: 'AE 9/13/2021 02:37:29'!
addAFraction: aFraccionAdder 
	
 	^ Fraccion with: (self * aFraccionAdder denominator + aFraccionAdder numerator) 
	                over: aFraccionAdder denominator.
	
	! !

!Entero methodsFor: 'implementative-arithmetic' stamp: 'JDS 9/15/2021 23:04:30'!
addAnEntero: anEnteroAdder 
	
	^self class with: value + anEnteroAdder integerValue! !

!Entero methodsFor: 'implementative-arithmetic' stamp: 'AE 9/15/2021 18:29:05'!
divideAFraccion: aFraccionDividend 
	
	^aFraccionDividend numerator / (aFraccionDividend denominator * self).
	! !

!Entero methodsFor: 'implementative-arithmetic' stamp: 'JDS 9/15/2021 22:47:17'!
divideAnEntero: anEnteroDividend 
	
	self subclassResponsibility.
	! !

!Entero methodsFor: 'implementative-arithmetic' stamp: 'JDS 9/15/2021 23:03:44'!
multiplyAFraccion: aFraccionMultiplier
	
	^(self * aFraccionMultiplier numerator) / aFraccionMultiplier denominator.! !

!Entero methodsFor: 'implementative-arithmetic' stamp: 'JDS 9/15/2021 22:47:29'!
multiplyAnEntero: anEnteroMultiplier 

	^self class with: value * anEnteroMultiplier integerValue! !

!Entero methodsFor: 'implementative-arithmetic' stamp: 'AE 9/14/2021 18:28:36'!
substractFromAFraccion: aFraccionMinuend 
	
	^(aFraccionMinuend numerator - (self * aFraccionMinuend denominator))
	   / aFraccionMinuend denominator! !

!Entero methodsFor: 'implementative-arithmetic' stamp: 'AE 9/14/2021 18:28:09'!
substractFromAnEntero: anEnteroMinuend

	^ self class with: (anEnteroMinuend integerValue) - value.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Entero class' category: 'Numero-Exercise'!
Entero class
	instanceVariableNames: ''!

!Entero class methodsFor: 'instance creation' stamp: 'AE 9/14/2021 17:49:16'!
canHandle: anEntero
	
	self subclassResponsibility.
	! !

!Entero class methodsFor: 'instance creation' stamp: 'NR 4/15/2021 16:42:24'!
negativeFibonacciErrorDescription
	^ ' Fibonacci no est� definido aqu� para Enteros Negativos!!!!!!'! !

!Entero class methodsFor: 'instance creation' stamp: 'AE 9/14/2021 19:35:44'!
with: aValue 
	
	| subclass | 
	
	"Esta verificacion esta puesta por si se equivocan y quieren crear un Entero pasando otra cosa que un Integer - Hernan"
	aValue isInteger ifFalse: [  self error: 'aValue debe ser anInteger' ].
	
	
	subclass := Entero subclasses detect: [ :aSubclass | aSubclass canHandle: aValue ].
	
	^subclass new initializeWith: aValue.		
	
	"^self new initalizeWith: aValue"! !


!classDefinition: #EnteroNegativo category: 'Numero-Exercise'!
Entero subclass: #EnteroNegativo
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!EnteroNegativo methodsFor: 'testing' stamp: 'JDS 9/15/2021 22:47:47'!
isNegative

	^true
	! !


!EnteroNegativo methodsFor: 'implementative-arithmetic' stamp: 'AE 9/15/2021 18:30:52'!
divideAnEntero: anEnteroDividend 
	
	^anEnteroDividend negated / self negated
	
	! !


!EnteroNegativo methodsFor: 'arithmetic operations' stamp: 'AE 9/14/2021 19:53:18'!
fibonacci

	self error: Entero negativeFibonacciErrorDescription.
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EnteroNegativo class' category: 'Numero-Exercise'!
EnteroNegativo class
	instanceVariableNames: ''!

!EnteroNegativo class methodsFor: 'as yet unclassified' stamp: 'AE 9/14/2021 17:53:18'!
canHandle: anInteger
	
	^anInteger < 0.
	! !


!classDefinition: #EnteroPositivoMayorQueUno category: 'Numero-Exercise'!
Entero subclass: #EnteroPositivoMayorQueUno
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!EnteroPositivoMayorQueUno methodsFor: 'private' stamp: 'JDS 9/15/2021 22:41:44'!
divideAnEnteroWithoutGCD: anEnteroDividend 
	
	^Fraccion createWithoutGCD: anEnteroDividend over: self.
	! !


!EnteroPositivoMayorQueUno methodsFor: 'implementative-arithmetic' stamp: 'AE 9/15/2021 19:14:56'!
divideAnEntero: anEnteroDividend 
		
	| denominator greatestCommonDivisor numerator |
	greatestCommonDivisor := anEnteroDividend greatestCommonDivisorWith: self. 
	numerator := anEnteroDividend // greatestCommonDivisor.
	denominator := self // greatestCommonDivisor.

	^denominator divideAnEnteroWithoutGCD: numerator.
	
	! !


!EnteroPositivoMayorQueUno methodsFor: 'arithmetic operations' stamp: 'AE 9/14/2021 19:52:15'!
fibonacci

	| one two |
	
	one := Entero with: 1.
	two := Entero with: 2.

	^ (self - one) fibonacci + (self - two) fibonacci
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EnteroPositivoMayorQueUno class' category: 'Numero-Exercise'!
EnteroPositivoMayorQueUno class
	instanceVariableNames: ''!

!EnteroPositivoMayorQueUno class methodsFor: 'as yet unclassified' stamp: 'AE 9/14/2021 17:53:45'!
canHandle: anInteger

	^anInteger > 1.
	! !


!classDefinition: #EnteroUno category: 'Numero-Exercise'!
Entero subclass: #EnteroUno
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!EnteroUno methodsFor: 'testing' stamp: 'AE 9/14/2021 19:39:31'!
isOne
	^true.
	! !


!EnteroUno methodsFor: 'arithmetic operations' stamp: 'AE 9/14/2021 19:52:33'!
fibonacci
	^Entero with: 1.
	! !


!EnteroUno methodsFor: 'private' stamp: 'AE 9/15/2021 19:14:56'!
divideAnEnteroWithoutGCD: anEnteroDividend
	^anEnteroDividend 
	! !


!EnteroUno methodsFor: 'implementative-arithmetic' stamp: 'AE 9/15/2021 18:45:02'!
divideAFraccion: aFraccionDividend 
	
	^aFraccionDividend
	! !

!EnteroUno methodsFor: 'implementative-arithmetic' stamp: 'AE 9/15/2021 19:20:57'!
divideAnEntero: anEnteroDividend
	^anEnteroDividend .
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EnteroUno class' category: 'Numero-Exercise'!
EnteroUno class
	instanceVariableNames: ''!

!EnteroUno class methodsFor: 'as yet unclassified' stamp: 'AE 9/14/2021 19:33:49'!
canHandle: anInteger

	^anInteger = 1.
	! !


!classDefinition: #EnteroZero category: 'Numero-Exercise'!
Entero subclass: #EnteroZero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!EnteroZero methodsFor: 'testing' stamp: 'AE 9/14/2021 19:38:20'!
isZero
	^true
	! !


!EnteroZero methodsFor: 'arithmetic operations' stamp: 'AE 9/14/2021 19:52:54'!
fibonacci

	^Entero with: 1.
	! !


!EnteroZero methodsFor: 'implementative-arithmetic' stamp: 'AE 9/14/2021 20:18:24'!
divideAFraccion: aFraccion

	self error: Numero canNotDivideByZeroErrorDescription.! !

!EnteroZero methodsFor: 'implementative-arithmetic' stamp: 'AE 9/14/2021 20:16:45'!
divideAnEntero: anEntero

	self error: Numero canNotDivideByZeroErrorDescription.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EnteroZero class' category: 'Numero-Exercise'!
EnteroZero class
	instanceVariableNames: ''!

!EnteroZero class methodsFor: 'as yet unclassified' stamp: 'AE 9/14/2021 17:54:22'!
canHandle: anInteger

	^anInteger = 0.
	! !


!classDefinition: #Fraccion category: 'Numero-Exercise'!
Numero subclass: #Fraccion
	instanceVariableNames: 'numerator denominator'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Fraccion methodsFor: 'arithmetic operations' stamp: 'JDS 9/15/2021 23:03:44'!
* aMultiplier 

	^aMultiplier multiplyAFraccion: self.
! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'JDS 9/15/2021 23:05:31'!
+ anAdder 
	
	^anAdder addAFraction: self.
	! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'AE 9/14/2021 18:28:36'!
- aSubtrahend 
	
	^ aSubtrahend  substractFromAFraccion: self.
! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'AE 9/14/2021 18:27:13'!
/ aDivisor 
		
	^aDivisor divideAFraccion: self.
! !


!Fraccion methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:42'!
= anObject

	^(anObject isKindOf: self class) and: [ (numerator * anObject denominator) = (denominator * anObject numerator) ]! !

!Fraccion methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:50'!
hash

	^(numerator hash / denominator hash) hash! !


!Fraccion methodsFor: 'accessing' stamp: 'HernanWilkinson 5/7/2016 21:56'!
denominator

	^ denominator! !

!Fraccion methodsFor: 'accessing' stamp: 'HernanWilkinson 5/7/2016 21:56'!
numerator

	^ numerator! !


!Fraccion methodsFor: 'initialization' stamp: 'HernanWilkinson 5/7/2016 22:54'!
initializeWith: aNumerator over: aDenominator

	"Estas precondiciones estan por si se comenten errores en la implementacion - Hernan"
	aNumerator isZero ifTrue: [ self error: 'una fraccion no puede ser cero' ].
	aDenominator isOne ifTrue: [ self error: 'una fraccion no puede tener denominador 1 porque sino es un entero' ].
	
	numerator := aNumerator.
	denominator := aDenominator ! !


!Fraccion methodsFor: 'testing' stamp: 'AE 9/13/2021 01:09:10'!
isNegative
	
	^numerator integerValue < 0! !

!Fraccion methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:51'!
isOne
	
	^false! !

!Fraccion methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:51'!
isZero
	
	^false! !


!Fraccion methodsFor: 'printing' stamp: 'HAW 9/24/2018 08:54:46'!
printOn: aStream

	aStream 
		print: numerator;
		nextPut: $/;
		print: denominator ! !


!Fraccion methodsFor: 'implementative-arithmetic' stamp: 'AE 9/13/2021 01:35:10'!
addAFraction: anAdderFraccion 

	| newDenominator newNumerator |
	newNumerator := (numerator * anAdderFraccion denominator) + (denominator * anAdderFraccion numerator).
	newDenominator := denominator * anAdderFraccion denominator.
	^newNumerator / newDenominator ! !

!Fraccion methodsFor: 'implementative-arithmetic' stamp: 'JDS 9/15/2021 23:04:30'!
addAnEntero: anEntero 
	
	^Fraccion with: anEntero * denominator + numerator over: denominator! !

!Fraccion methodsFor: 'implementative-arithmetic' stamp: 'AE 9/14/2021 18:27:13'!
divideAFraccion: aFraccionDividend 
	
	^aFraccionDividend * (denominator / numerator )
	! !

!Fraccion methodsFor: 'implementative-arithmetic' stamp: 'JDS 9/15/2021 23:05:08'!
divideAnEntero: anEntero 
	
	^ (anEntero * denominator) / numerator
	 ! !

!Fraccion methodsFor: 'implementative-arithmetic' stamp: 'JDS 9/15/2021 23:03:44'!
multiplyAFraccion: aMultiplierFraccion 
	
	^(numerator * aMultiplierFraccion numerator) / (denominator * aMultiplierFraccion denominator)! !

!Fraccion methodsFor: 'implementative-arithmetic' stamp: 'AE 9/14/2021 18:27:35'!
multiplyAnEntero: anAdderEntero 
	
	^ (numerator * anAdderEntero) / denominator
	! !

!Fraccion methodsFor: 'implementative-arithmetic' stamp: 'AE 9/14/2021 18:28:36'!
substractFromAFraccion: aFraccion 
	| newNumerator newDenominator |
	
	newNumerator := (aFraccion numerator * denominator) - (aFraccion denominator * numerator).
	newDenominator := aFraccion denominator * denominator.
	
	^newNumerator / newDenominator. 
	! !

!Fraccion methodsFor: 'implementative-arithmetic' stamp: 'AE 9/14/2021 18:28:09'!
substractFromAnEntero: anEnteroSubstrahend 
	
	^ (anEnteroSubstrahend * denominator - numerator) / denominator.
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Fraccion class' category: 'Numero-Exercise'!
Fraccion class
	instanceVariableNames: ''!

!Fraccion class methodsFor: 'intance creation' stamp: 'JDS 9/15/2021 22:41:44'!
createWithoutGCD: aDividend over: aDivisor

	^self new initializeWith: aDividend over: aDivisor.
	! !

!Fraccion class methodsFor: 'intance creation' stamp: 'AE 9/15/2021 19:16:58'!
with: aDividend over: aDivisor

	| greatestCommonDivisor numerator denominator |
	
	"aDivisor assertCanBeDenominator ."
	"Checked on EnteroZero"
	"aDivisor isZero ifTrue: [ self error: self canNotDivideByZeroErrorDescription ]."
	
	"Checked on EnteroZero /"
	"aDividend isZero ifTrue: [ ^aDividend ]."
	
	"Checked on EnteroNegativo"
	"aDivisor isNegative ifTrue:[ ^aDividend negated / aDivisor negated]."
	
	greatestCommonDivisor := aDividend greatestCommonDivisorWith: aDivisor. 
	numerator := aDividend // greatestCommonDivisor.
	denominator := aDivisor // greatestCommonDivisor.
	
	^numerator / denominator.
	
	"Checked on EnteroUno"
	"denominator isOne ifTrue: [ ^numerator ]."

	"^self new initializeWith: numerator over: denominator"
	! !
