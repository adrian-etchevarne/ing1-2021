!classDefinition: #OOStackTest category: 'Stack-Exercise'!
TestCase subclass: #OOStackTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:30'!
test01StackShouldBeEmptyWhenCreated

	| stack |
	
	stack := OOStack new.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:29:55'!
test02PushAddElementsToTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	
	self deny: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:01'!
test03PopRemovesElementsFromTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:09'!
test04PopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	stack push: pushedObject.
	
	self assert: stack pop = pushedObject! !

!OOStackTest methodsFor: 'test' stamp: 'NR 9/16/2021 17:40:17'!
test05StackBehavesLIFO

	| stack firstPushedObject secondPushedObject |
	
	stack := OOStack new.
	firstPushedObject := 'firstSomething'.
	secondPushedObject := 'secondSomething'.
	
	stack push: firstPushedObject.
	stack push: secondPushedObject.
	
	self assert: stack pop = secondPushedObject.
	self assert: stack pop = firstPushedObject.
	self assert: stack isEmpty 
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:20'!
test06TopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack top = pushedObject.
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:24'!
test07TopDoesNotRemoveObjectFromStack

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack size = 1.
	stack top.
	self assert: stack size = 1.
	! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:26'!
test08CanNotPopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:31'!
test09CanNotPopWhenThereAreNoObjectsInTheStackAndTheStackHadObjects

	| stack  |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:44'!
test10CanNotTopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack top ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !


!classDefinition: #SentenceFinderByPrefixTest category: 'Stack-Exercise'!
TestCase subclass: #SentenceFinderByPrefixTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'JDS 9/20/2021 01:46:15'!
test01NoSentencesOnAnEmptyStack

	| sentences strFinder emptyStack |
	
	emptyStack := OOStack new.
	strFinder := SentenceFinderByPrefix new: emptyStack.
	
	sentences := strFinder find: 'something'.
	
	self assert: sentences isEmpty ! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'JDS 9/20/2021 00:27:15'!
test02IsPrefixOfWholeSentences
	
	|sentences strFinder prefixes s1 s2 s3 |
	
	sentences := OOStack new.
	s1 := 'Hola mundo'.
	s2 := 'Hola '.
	s3 := 'Hola   Susana'.
	sentences push: s1.
	sentences push: s2.
	sentences push: s3.
	strFinder := SentenceFinderByPrefix new: sentences.
	
	prefixes := strFinder find: 'Hola'.

	self assert: (prefixes includes: s1).
	self assert: (prefixes includes: s2).
	self assert: (prefixes includes: s3).
	self assert: prefixes size equals: sentences size.
	
! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'JDS 9/19/2021 23:32:34'!
test03IsNotPrefixInAnySentence

	|sentences strFinder prefixes|
	
	sentences := OOStack new.
	sentences push: 'Hello world'.
	sentences push: 'Hello '.
	sentences push: 'wood'.
	strFinder := SentenceFinderByPrefix new: sentences.

	prefixes := strFinder find: 'world'.
	
	self assert: prefixes isEmpty! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'JDS 9/19/2021 23:44:46'!
test04IsCaseSensitive

	|sentences strFinder prefixes s1 s2 s3 s4 s5 |

	sentences := OOStack new.
	s1 _ 'winter is coming'.
	s2 _ 'we win in everything'.
	s3 _ 'Winter is here'.
	s4 _ 'winnie pooh'.
	s5 _ 'Winona Rider'.
	sentences push:  s1.
	sentences push: s2. 
	sentences push: s3.
	sentences push: s4.
	sentences push:  s5.
	strFinder := SentenceFinderByPrefix new: sentences.

	prefixes := strFinder find: 'win'.
	
	self deny: (prefixes includes: s2).
	self deny: (prefixes includes: s3).
	self deny: (prefixes includes: s5).
	self assert: (prefixes includes: s1).
	self assert: (prefixes includes: s4).

	! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'JDS 9/20/2021 01:13:15'!
test05FinderNoAcceptAnEmptyStringParameter

	|sentences myFinder anEmptyStrParam |
	
	sentences := OOStack new.
	sentences push: 'winter is coming'. 
	sentences push: 'winning is everything'. 
	anEmptyStrParam := ''.
	myFinder := SentenceFinderByPrefix new: sentences.
	
	self  should: [ myFinder find: anEmptyStrParam ]
		raise: AssertionFailure 
		withExceptionDo: [ :anError |
			self assert: anError messageText = SentenceFinderByPrefix emptyPrefixErrorDescription ]
		! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'JDS 9/20/2021 01:12:35'!
test06FinderNoAcceptAStringParameterWithBlanks

	|sentences myFinder strParamWithBlanks |
	
	sentences := OOStack new.
	sentences push: 'Something in the way she moves'. 
	sentences push: 'Attracks me like no other lover'. 
	strParamWithBlanks := 'Something in her style that shows me'.
	myFinder := SentenceFinderByPrefix new: sentences.
	
	self  should: [ myFinder find: strParamWithBlanks ]
		raise: AssertionFailure 
		withExceptionDo: [ :anError |
			self assert: anError messageText = SentenceFinderByPrefix prefixWithBlanksErrorDescription ]
		! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'JDS 9/20/2021 01:26:46'!
test07PrefixEqualOrBiggerThanSentences
	
	|sentences  strFinder selectedSentences s1 s2 s3 s4 s5 s6 |
	
	sentences := OOStack new.
	s1 _ 'Holi'.
	s2 _ 'Holy'.
	s3 _ 'Hoi'.
	s4 _ 'Holis'.
	s5 _ 'Hola'.
	s6 _ 'Hol'.
	sentences push: s1. 
	sentences push: s2.
	sentences push: s3.
	sentences push: s4. 
	sentences push: s5. 
	sentences push: s6.
	strFinder := SentenceFinderByPrefix new: sentences.
	selectedSentences := strFinder find: s4.
	
	self deny: (selectedSentences includes: s1).
	self deny: (selectedSentences includes: s2).
	self deny: (selectedSentences includes: s3).
	self deny: (selectedSentences includes: s5).
	self deny: (selectedSentences includes: s6).
	self assert: (selectedSentences includes: s4).	
	
	
	! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'JDS 9/20/2021 01:45:55'!
test08FindManyTimesNoBreaksTheStack

	|sentences strFinder selectedSentences s1 s2 selectedSentencesAgain s3 |
	sentences := OOStack new.
	s1 _ 'hola mundo'.
	s2 _ 'hola new york'.
	s3 _ 'Hello kitty'.	
	sentences push: s1.
	sentences push: s2.
	sentences push: s3.
	
	strFinder := SentenceFinderByPrefix new: sentences.
	
	selectedSentences := strFinder find: 'hola'.
	self assert: selectedSentences includes: s1.
	self assert: selectedSentences includes: s2.
	self deny: (selectedSentences includes: s3).
	self assert: selectedSentences size equals: sentences size - 1.
	
	selectedSentencesAgain := strFinder find: 'hola'.
	self assert: selectedSentencesAgain includes: s1.
	self assert: selectedSentencesAgain includes: s2.
	self deny: (selectedSentences includes: s3).
	self assert: selectedSentencesAgain size equals: sentences size - 1.
	
	
	
	
	! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'JDS 9/20/2021 01:55:38'!
test09FindReturnsWithTheStackInTheSameOrder

	|sentences origStack strFinder s1 s2 s3 |
	
	sentences _ OOStack new.
	origStack _ OOStack new. 
	s1 _ 'hola mundo'.
	s2 _ 'hola new york'.	
	s3 _ 'Hello kitty'.	
	sentences push: s1.
	origStack push: s1.
	sentences push: s2.
	origStack push: s2.
	sentences push: s3.
	origStack push: s3.
	strFinder _ SentenceFinderByPrefix new: sentences.
	
	strFinder find: 'hola'.
	
	self assert: sentences size equals: origStack size.
	[sentences isEmpty] whileFalse: [ 	self assert: sentences pop equals: origStack pop ].
	
	
	
	! !


!classDefinition: #BaseStack category: 'Stack-Exercise'!
Object subclass: #BaseStack
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!BaseStack methodsFor: 'accessing' stamp: 'JDS 9/20/2021 01:02:50'!
content

	^self error: OOStack stackEmptyErrorDescription ! !

!BaseStack methodsFor: 'accessing' stamp: 'JDS 9/18/2021 13:56:58'!
size

	^ 0! !


!BaseStack methodsFor: 'testing' stamp: 'JDS 9/18/2021 11:12:22'!
isEmpty

	^true! !


!classDefinition: #CellStack category: 'Stack-Exercise'!
Object subclass: #CellStack
	instanceVariableNames: 'element previous'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!CellStack methodsFor: 'testing' stamp: 'JDS 9/18/2021 11:12:05'!
isEmpty

	^false! !


!CellStack methodsFor: 'private' stamp: 'JDS 9/18/2021 11:42:54'!
content: anObject

	element := anObject! !

!CellStack methodsFor: 'private' stamp: 'JDS 9/18/2021 11:41:25'!
previous: aCell

	previous := aCell! !


!CellStack methodsFor: 'accessing' stamp: 'JDS 9/18/2021 11:26:27'!
content

	^element! !

!CellStack methodsFor: 'accessing' stamp: 'JDS 9/18/2021 11:32:32'!
previous

	^previous! !

!CellStack methodsFor: 'accessing' stamp: 'JDS 9/18/2021 13:56:35'!
size

	^self previous size + 1! !


!CellStack methodsFor: 'printing' stamp: 'JDS 9/19/2021 18:34:33'!
printOn: aStream

	aStream nextPutAll: 'a cell containing '.
	element printOn: aStream.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CellStack class' category: 'Stack-Exercise'!
CellStack class
	instanceVariableNames: ''!

!CellStack class methodsFor: 'as yet unclassified' stamp: 'JDS 9/18/2021 11:57:33'!
new: anObject withAPreviousCell: aCell

	| cell |
	cell := self new content: anObject.
	cell previous: aCell.
	^cell! !


!classDefinition: #OOStack category: 'Stack-Exercise'!
Object subclass: #OOStack
	instanceVariableNames: 'topCell'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!
!OOStack commentStamp: '<historical>' prior: 0!
isEmpty

	^list isEmpty !


!OOStack methodsFor: 'testing' stamp: 'JDS 9/18/2021 13:30:32'!
isEmpty

	^topCell isEmpty! !


!OOStack methodsFor: 'initialization' stamp: 'JDS 9/18/2021 14:14:26'!
initialize

	topCell := BaseStack new.! !


!OOStack methodsFor: 'accessing' stamp: 'JDS 9/18/2021 15:27:18'!
pop

	| topElement |
	topElement := self top.
	self topCell: self topCell previous.
	^topElement
! !

!OOStack methodsFor: 'accessing' stamp: 'JDS 9/18/2021 15:26:20'!
push: anObject

	self topCell: (CellStack new: anObject withAPreviousCell: topCell).
	! !

!OOStack methodsFor: 'accessing' stamp: 'JDS 9/18/2021 15:25:44'!
size

	^self topCell size! !

!OOStack methodsFor: 'accessing' stamp: 'JDS 9/18/2021 11:25:34'!
top

	^topCell content! !


!OOStack methodsFor: 'printing' stamp: 'JDS 9/19/2021 22:44:02'!
printOn: aStream

	aStream nextPutAll: 'a stack with '.	
	topCell printOn: aStream.
	aStream nextPutAll: ' on top.'.
	! !


!OOStack methodsFor: 'private' stamp: 'JDS 9/18/2021 15:25:11'!
topCell

	^topCell! !

!OOStack methodsFor: 'private' stamp: 'JDS 9/18/2021 15:25:27'!
topCell: aCell

	topCell := aCell! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStack class' category: 'Stack-Exercise'!
OOStack class
	instanceVariableNames: ''!

!OOStack class methodsFor: 'error descriptions' stamp: 'NR 9/16/2021 17:39:43'!
stackEmptyErrorDescription
	
	^ 'stack is empty!!!!!!'! !


!classDefinition: #SentenceFinderByPrefix category: 'Stack-Exercise'!
Object subclass: #SentenceFinderByPrefix
	instanceVariableNames: 'stackSentences'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefix methodsFor: 'auxiliar' stamp: 'JDS 9/19/2021 15:20:45'!
add: strElem ifIsPrefixedWith: aStringPrefix to: aCollection

	(strElem beginsWith: aStringPrefix) ifTrue: [aCollection add: strElem]! !


!SentenceFinderByPrefix methodsFor: 'operation' stamp: 'JDS 9/20/2021 00:42:24'!
find: aStringPrefix

	| selectedSentences auxStack |

	self assert: [ aStringPrefix ~= '' ] description: self class emptyPrefixErrorDescription.
	self assert: [ (aStringPrefix includesAnyOf: ' ') not] description: self class prefixWithBlanksErrorDescription .

	selectedSentences := OrderedCollection  new.
	auxStack := OOStack new.

	[stackSentences isEmpty] whileFalse: [ | strElem | 
			strElem := stackSentences pop.
			auxStack push: strElem. 
			self add: strElem ifIsPrefixedWith: aStringPrefix to: selectedSentences].
	self restoreStackWith: auxStack.
	^selectedSentences! !


!SentenceFinderByPrefix methodsFor: 'private' stamp: 'JDS 9/20/2021 00:22:44'!
restoreStackWith: aStack

	"Restore the orginal state of the stack linked to myself. 
	aStack must contain the same elements in an inverse order to the original state."

	[aStack isEmpty] whileFalse: [ | anElement | anElement := aStack pop. stackSentences push: anElement ]! !

!SentenceFinderByPrefix methodsFor: 'private' stamp: 'JDS 9/20/2021 00:22:44'!
stack: aStackOfStringElements

	stackSentences := aStackOfStringElements! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SentenceFinderByPrefix class' category: 'Stack-Exercise'!
SentenceFinderByPrefix class
	instanceVariableNames: ''!

!SentenceFinderByPrefix class methodsFor: 'instance creation' stamp: 'JDS 9/19/2021 12:42:05'!
new: aStackOfStringElements

	^self new stack: aStackOfStringElements 

	! !


!SentenceFinderByPrefix class methodsFor: 'error description' stamp: 'JDS 9/20/2021 00:39:12'!
emptyPrefixErrorDescription
	
	^ 'prefix is empty!!!!!!'! !

!SentenceFinderByPrefix class methodsFor: 'error description' stamp: 'JDS 9/20/2021 00:40:48'!
prefixWithBlanksErrorDescription
	
	^ 'prefix contains blanks!!!!!!'! !
