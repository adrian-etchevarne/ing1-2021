!classDefinition: #ISWPropTest category: 'ISW1-2021-2C-Recuperatorio-2doParcial'!
TestCase subclass: #ISWPropTest
	instanceVariableNames: 'listaDePrecios precioMetroAlmagroEnero precioMetroAlmagroDiciembre inmobiliaria barrio area'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2021-2C-Recuperatorio-2doParcial'!

!ISWPropTest methodsFor: 'testing' stamp: 'AE 12/6/2021 21:10:14'!
test01PrecioDePropiedadSinModificadoresSeCalculaCorrectamente

	|  prop fechaTasacion |

	prop _ Propiedad en: barrio de: area.

	fechaTasacion _ (January/15/2021).
	
	self assert: (precioMetroAlmagroEnero * area) = 	(inmobiliaria tasar: prop en: fechaTasacion).
	
	
	
	
	
	! !

!ISWPropTest methodsFor: 'testing' stamp: 'AE 12/6/2021 21:10:21'!
test02PrecioDePropiedadConDiferencialSeCalculaCorrectamente

	| prop fechaTasacion modificador porcentajeModificacion precioEsperado |
	
	porcentajeModificacion _ 15.
		
	modificador _ PorcentajeDiferencial de: porcentajeModificacion.
	
	prop _ Propiedad en: barrio de: area modificador: modificador.
	
	fechaTasacion _ (January/15/2021).
	
	precioEsperado _ precioMetroAlmagroEnero * area * (1 + (porcentajeModificacion / 100)).

	self assert: precioEsperado equals: 	(inmobiliaria tasar: prop en: fechaTasacion).
	
	
	
	
	
	! !

!ISWPropTest methodsFor: 'testing' stamp: 'AE 12/6/2021 21:10:28'!
test03PrecioDePropiedadConPrecioFinDeSemanaSeCalculaCorrectamente

	| prop modificador porcentajeModificacion fechaTasacionDiaHabil fechaTasacionDomingo precioEsperadoDiaHabil precioEsperadoDomingo |
	
	porcentajeModificacion _ 12.
	modificador _ PorcentajeFinDeSemana de: porcentajeModificacion.
	
	prop _ Propiedad en: barrio de: area modificador: modificador.
	
	fechaTasacionDiaHabil _ (December/6/2021).
	fechaTasacionDomingo _ (December/5/2021).
	
	precioEsperadoDiaHabil _ precioMetroAlmagroDiciembre * area.
	precioEsperadoDomingo _ precioMetroAlmagroDiciembre * area * (1 + (porcentajeModificacion / 100)).
	
	self assert: precioEsperadoDiaHabil equals: 	(inmobiliaria tasar: prop en: fechaTasacionDiaHabil).
	self assert: precioEsperadoDomingo equals: 	(inmobiliaria tasar: prop en: fechaTasacionDomingo).
	
	
	! !

!ISWPropTest methodsFor: 'testing' stamp: 'AE 12/6/2021 21:10:35'!
test04PrecioDePropiedadConDiasParticularesSeCalculaCorrectamente

	| prop modificador porcentajeModificacion fechaTasacionDiaEspecial fechaTasacionComun precioEsperadoDiaComun precioEsperadoDiaEspecial |
	
	porcentajeModificacion _ 17.
		
	modificador _ PorcentajeDiasEspeciales 
					de: porcentajeModificacion 
					en: { Monday. Wednesday. Friday }.
		
	prop _ Propiedad en: barrio de: area modificador: modificador.
	
	fechaTasacionDiaEspecial _ (December/8/2021).
	fechaTasacionComun _ (December/5/2021).
	
	precioEsperadoDiaEspecial _ precioMetroAlmagroDiciembre * area  * (1 + (porcentajeModificacion / 100)).
	precioEsperadoDiaComun _ precioMetroAlmagroDiciembre * area.
	
	self assert: precioEsperadoDiaEspecial equals: 	(inmobiliaria tasar: prop en: fechaTasacionDiaEspecial).
	self assert: precioEsperadoDiaComun equals: 	(inmobiliaria tasar: prop en: fechaTasacionComun).
	
	
	! !

!ISWPropTest methodsFor: 'testing' stamp: 'AE 12/6/2021 21:10:42'!
test05PrecioDePropiedadVariaConElTiempo

	| prop modificador porcentajeModificacion fechaTasacionDiaEspecialEnero fechaTasacionDiaEspecialDiciembre precioEsperadoDiaEspecialEnero precioEsperadoDiaEspecialDiciembre |
	
	porcentajeModificacion _ 16.
		
	modificador _ PorcentajeDiasEspeciales 
					de: porcentajeModificacion 
					en: { Monday. Wednesday. Friday }.
		
	prop _ Propiedad en: barrio de: area modificador: modificador.
	
	fechaTasacionDiaEspecialDiciembre _ (December/8/2021).
	fechaTasacionDiaEspecialEnero _ (January/1/2021).
	
	precioEsperadoDiaEspecialEnero _ precioMetroAlmagroEnero * area  * (1 + (porcentajeModificacion / 100)).
	precioEsperadoDiaEspecialDiciembre _ precioMetroAlmagroDiciembre * area  * (1 + (porcentajeModificacion / 100)).
	
	
	self assert: precioEsperadoDiaEspecialEnero equals: 	(inmobiliaria tasar: prop en: fechaTasacionDiaEspecialEnero).
	self assert: precioEsperadoDiaEspecialDiciembre equals: 	(inmobiliaria tasar: prop en: fechaTasacionDiaEspecialDiciembre).
	
	
	! !

!ISWPropTest methodsFor: 'testing' stamp: 'AE 12/6/2021 21:45:20'!
test06PropiedadPuedeSerVendida

	| prop propiedadesVendidas propiedadesALaVenta fechaVenta fechaListado |
	
	prop _ Propiedad en: barrio de: area.
		
	inmobiliaria ponerEnVenta: prop.

	fechaVenta _ (January/15/2021).
	fechaListado _ (January/16/2021).
	
	inmobiliaria seVendio: prop en: fechaVenta.
	
	propiedadesVendidas _ inmobiliaria propiedadesVendidasAl: fechaListado.
	propiedadesALaVenta _ inmobiliaria propiedadesALaVentaAl: fechaListado.
	
	self assert: (propiedadesALaVenta size) equals: 0.
	self assert: (propiedadesVendidas size) equals: 1.	
	self assert: (propiedadesVendidas asDictionary includesKey: prop ). 

	
	
		
	
	
	
	! !

!ISWPropTest methodsFor: 'testing' stamp: 'AE 12/6/2021 21:11:31'!
test07PropiedadQueNoEsDeLaInmobiliariaNoPuedeSerVendida

	|  prop fechaVenta propiedadesVendidas |
	
	prop _ Propiedad en: barrio de: area.
		
	fechaVenta _ (January/15/2021).

	self should: 
			[ inmobiliaria seVendio: prop en: fechaVenta ]
		raise: Error 
		withMessageText: Inmobiliaria propiedadNoEstaALaVenta.
	
	propiedadesVendidas _ inmobiliaria propiedadesVendidasAl: fechaVenta.	
	self assert: (propiedadesVendidas size) equals: 0.
	

	
	! !

!ISWPropTest methodsFor: 'testing' stamp: 'AE 12/6/2021 21:26:36'!
test08PropiedadPuedeSerSeñada

	|  prop propiedadesVendidas propiedadesALaVenta fechaListado fechaFinDeSeña fechaSeña propiedadesSeñadas |
	
	prop _ Propiedad en: barrio de: area.
		
	inmobiliaria ponerEnVenta: prop.

	fechaSeña _ (January/15/2021).
	fechaFinDeSeña _ (August/16/2021).
	
	fechaListado _ March/15/2021.
	
	inmobiliaria seSeño: prop en: fechaSeña hasta: fechaFinDeSeña.
	
	propiedadesSeñadas _ inmobiliaria propiedadesSeñadasAl: fechaListado.
	propiedadesVendidas _ inmobiliaria propiedadesVendidasAl: fechaListado.
	propiedadesALaVenta _ inmobiliaria propiedadesALaVentaAl: fechaListado.
	
	self assert: (propiedadesSeñadas size) equals: 1.
	self assert: (propiedadesSeñadas includes: prop).
	
	self assert: (propiedadesALaVenta size) equals: 0.
	self assert: (propiedadesVendidas size) equals: 0.	


	
	
		
	
	
	
	! !

!ISWPropTest methodsFor: 'testing' stamp: 'AE 12/6/2021 21:33:25'!
test09PropiedadSeñadaPuedeSerVendidaYConservaPrecio

	|  prop propiedadesVendidas propiedadesALaVenta fechaListado fechaFinDeSeña fechaSeña propiedadesSeñadas fechaVenta |
	
	prop _ Propiedad en: barrio de: area.
		
	inmobiliaria ponerEnVenta: prop.

	fechaSeña _ (January/15/2021).
	fechaFinDeSeña _ (December/16/2021).
	
	fechaListado _ January/1/2022.
	
	"En diciembre Almagro está más caro."
	fechaVenta _ December/15/2021.
		
	inmobiliaria seSeño: prop en: fechaSeña hasta: fechaFinDeSeña.

	inmobiliaria seVendio: prop en: fechaVenta.
	
	propiedadesSeñadas _ inmobiliaria propiedadesSeñadasAl: fechaListado.
	propiedadesVendidas _ inmobiliaria propiedadesVendidasAl: fechaListado.
	propiedadesALaVenta _ inmobiliaria propiedadesALaVentaAl: fechaListado.
	
	self assert: (propiedadesSeñadas size) equals: 0.
	
	self assert: (propiedadesALaVenta size) equals: 0.
	self assert: (propiedadesVendidas size) equals: 1.	


	
	
		
	
	
	
	! !


!ISWPropTest methodsFor: 'setUp/tearDown' stamp: 'AE 12/6/2021 21:12:48'!
setUp
	
	precioMetroAlmagroEnero _ 1000 * peso / (meter^2).
	precioMetroAlmagroDiciembre _ 1500 * peso / (meter^2).
	
	
	listaDePrecios _
		{  'Almagro' -> 
			{
				 (January of: 2021) -> precioMetroAlmagroEnero.
				 (December of: 2021) -> precioMetroAlmagroDiciembre.
			 }.
			
		    'Palermo' ->
			{
				 (January of: 2021) -> (4000 * peso / (meter^2)).
				 (December of: 2021) -> (10000 * peso / (meter^2)).
			 }.
			
		}.

	inmobiliaria _ Inmobiliaria with: listaDePrecios .
	
	barrio _ 'Almagro'.
	
	area _ 50 * meter * meter.! !


!classDefinition: #Inmobiliaria category: 'ISW1-2021-2C-Recuperatorio-2doParcial'!
Object subclass: #Inmobiliaria
	instanceVariableNames: 'listaDePrecios propiedadesVendidas señas propiedadesEnVenta'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2021-2C-Recuperatorio-2doParcial'!

!Inmobiliaria methodsFor: 'initialization' stamp: 'AE 12/6/2021 21:18:13'!
initializeWith: aListaDePrecios 
	
	| dict |
	dict _ Dictionary newFrom: aListaDePrecios.

	listaDePrecios _ Dictionary new.

	dict keysAndValuesDo: 
		[ :key :value |
			
			listaDePrecios at: key put: (Dictionary newFrom: 	value) 
		].
				
				
	propiedadesVendidas _ OrderedCollection new.
	propiedadesEnVenta _ Set new.
	
	
	señas _ Dictionary new.
	! !


!Inmobiliaria methodsFor: 'private' stamp: 'AE 12/6/2021 21:32:36'!
sacarDeLaVenta: aPropiedad 

	 propiedadesEnVenta remove: aPropiedad ifAbsent: 
		[ señas removeKey: aPropiedad ifAbsent:
			[self error: self class propiedadNoEstaALaVenta ] ].
	
! !


!Inmobiliaria methodsFor: 'accessing' stamp: 'AE 12/6/2021 21:49:37'!
estaSeñado: aPropiedad en: fecha


	^ (señas includesKey: aPropiedad) and:
		[ (señas at: aPropiedad ) includes: fecha ].

	
	
	
	
	! !

!Inmobiliaria methodsFor: 'accessing' stamp: 'AE 12/6/2021 21:06:56'!
fueVendida: aPropiedad

	| ventas |
	ventas _ self propiedadesVendidas .
	
	ventas do: [ :prop | 
		(ventas key = aPropiedad)
			 ifTrue: [ ^true ] ].
	
	^false.
	
	
	
	
	! !

!Inmobiliaria methodsFor: 'accessing' stamp: 'AE 12/6/2021 20:30:53'!
ponerEnVenta: aPropiedad 
	
	propiedadesEnVenta add: aPropiedad.
	! !

!Inmobiliaria methodsFor: 'accessing' stamp: 'AE 12/6/2021 20:33:05'!
propiedadesALaVenta
	
	
	^propiedadesEnVenta ! !

!Inmobiliaria methodsFor: 'accessing' stamp: 'AE 12/6/2021 20:51:55'!
propiedadesALaVentaAl: aFixedGregorianDate 
	
	^propiedadesEnVenta ! !

!Inmobiliaria methodsFor: 'accessing' stamp: 'AE 12/6/2021 21:23:25'!
propiedadesSeñadasAl: aFixedGregorianDate 
	
	| result |
	result _ OrderedCollection new.
	
	señas associationsDo: [ :seña |  
		(seña value includes: aFixedGregorianDate )
			ifTrue: [ result add: seña key ].
		].
		
	^result.
	
	! !

!Inmobiliaria methodsFor: 'accessing' stamp: 'AE 12/6/2021 21:44:34'!
propiedadesVendidasAl: fechaListado 
	
	| propiedades |
	propiedades _ OrderedCollection new.
	
	propiedadesVendidas do: [ :prop | prop value <= fechaListado
			ifTrue: [ propiedades add: prop ]. ].
	
	^propiedades.
	! !

!Inmobiliaria methodsFor: 'accessing' stamp: 'AE 12/6/2021 21:25:48'!
seSeño: aPropiedad en: fechaDeSeña hasta: finDeSeña
	
	self sacarDeLaVenta: aPropiedad.
	
	señas at: aPropiedad put: (fechaDeSeña to: finDeSeña)
	
	
	
	
	! !

!Inmobiliaria methodsFor: 'accessing' stamp: 'AE 12/6/2021 21:53:22'!
seVendio: aPropiedad en: lastPaymentDay
	| fechaDeVenta | 
	(self estaSeñado: aPropiedad en: lastPaymentDay )
		ifTrue: [ fechaDeVenta _ (señas at: aPropiedad) from ]
		ifFalse: [ fechaDeVenta _ lastPaymentDay ].
			
	self sacarDeLaVenta: aPropiedad.
	 	
	propiedadesVendidas add: ( aPropiedad -> fechaDeVenta ).
	
! !

!Inmobiliaria methodsFor: 'accessing' stamp: 'AE 12/6/2021 19:30:46'!
tasar: aPropiedad en: aFechaTasacion 
	
	
	| barrio listaDePreciosPorBarrio mes precioBase precioAlMes |
	barrio _ aPropiedad barrio.
	
	mes _ aFechaTasacion monthOfYear .
	
	
	listaDePreciosPorBarrio _ listaDePrecios at: barrio.
	
	precioAlMes _ listaDePreciosPorBarrio at: mes.
	
	precioBase _ precioAlMes * (aPropiedad area).
	
	
	^ aPropiedad modificaPrecio: precioBase en: aFechaTasacion .
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Inmobiliaria class' category: 'ISW1-2021-2C-Recuperatorio-2doParcial'!
Inmobiliaria class
	instanceVariableNames: ''!

!Inmobiliaria class methodsFor: 'instance creation' stamp: 'AE 12/6/2021 18:07:11'!
with: listaDePrecios
	^self new initializeWith: listaDePrecios ! !


!Inmobiliaria class methodsFor: 'error handling' stamp: 'AE 12/6/2021 20:58:29'!
propiedadNoEstaALaVenta
	^'La propiedad no está a la venta por esta inmobiliaria.'
	! !


!classDefinition: #Modificador category: 'ISW1-2021-2C-Recuperatorio-2doParcial'!
Object subclass: #Modificador
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2021-2C-Recuperatorio-2doParcial'!

!Modificador methodsFor: 'accessing' stamp: 'AE 12/6/2021 18:39:35'!
modifica: aPrice en: aFixedGregorianDate 
	^self subclassResponsibility 
	! !


!classDefinition: #PorcentajeDiasEspeciales category: 'ISW1-2021-2C-Recuperatorio-2doParcial'!
Modificador subclass: #PorcentajeDiasEspeciales
	instanceVariableNames: 'percentage arrayOfDays'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2021-2C-Recuperatorio-2doParcial'!

!PorcentajeDiasEspeciales methodsFor: 'initialization' stamp: 'AE 12/6/2021 19:00:00'!
initializeDe: aPercentage en: anArrayOfDays 
	percentage := aPercentage.
	arrayOfDays := anArrayOfDays.! !


!PorcentajeDiasEspeciales methodsFor: 'accessing' stamp: 'AE 12/6/2021 19:02:56'!
modifica: aPrice en: aFixedGregorianDate 
	
	
	| diaTasacion |
	diaTasacion _ aFixedGregorianDate day .
	
	(arrayOfDays includes: diaTasacion)
		ifTrue: [ ^aPrice * (1 + (percentage / 100)) ]
		ifFalse: [ ^aPrice ].
		
		! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'PorcentajeDiasEspeciales class' category: 'ISW1-2021-2C-Recuperatorio-2doParcial'!
PorcentajeDiasEspeciales class
	instanceVariableNames: ''!

!PorcentajeDiasEspeciales class methodsFor: 'instance creation' stamp: 'AE 12/6/2021 18:59:39'!
de: aPercentage en: anArrayOfDays
	^self new initializeDe: aPercentage en: anArrayOfDays! !


!classDefinition: #PorcentajeDiferencial category: 'ISW1-2021-2C-Recuperatorio-2doParcial'!
Modificador subclass: #PorcentajeDiferencial
	instanceVariableNames: 'percentage'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2021-2C-Recuperatorio-2doParcial'!

!PorcentajeDiferencial methodsFor: 'initialization' stamp: 'AE 12/6/2021 22:01:03'!
initializeDe: aPercentage 

	((-15 to: 15) includes: aPercentage)
		ifFalse: [ self error: 	'Porcentaje no válido' ].
		
	
	percentage := aPercentage.! !


!PorcentajeDiferencial methodsFor: 'accessing' stamp: 'AE 12/6/2021 18:41:48'!
modifica: aPrice en: aFixedGregorianDate 
	
	
	^ aPrice * ( 1 + (percentage / 100)).
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'PorcentajeDiferencial class' category: 'ISW1-2021-2C-Recuperatorio-2doParcial'!
PorcentajeDiferencial class
	instanceVariableNames: ''!

!PorcentajeDiferencial class methodsFor: 'instance creation' stamp: 'AE 12/6/2021 18:32:21'!
de: aPercentage
	
	^self new initializeDe: aPercentage .
	! !


!classDefinition: #PorcentajeFinDeSemana category: 'ISW1-2021-2C-Recuperatorio-2doParcial'!
Modificador subclass: #PorcentajeFinDeSemana
	instanceVariableNames: 'percentage'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2021-2C-Recuperatorio-2doParcial'!

!PorcentajeFinDeSemana methodsFor: 'initialization' stamp: 'AE 12/6/2021 18:50:15'!
initializeDe: aPercentage 
	percentage := aPercentage.! !


!PorcentajeFinDeSemana methodsFor: 'accessing' stamp: 'AE 12/6/2021 18:53:29'!
modifica: aPrice en: aFixedGregorianDate 
	
	
	(aFixedGregorianDate isSaturday or:			[ aFixedGregorianDate isSunday ])
		ifTrue: [ ^aPrice * (1 + ( percentage / 100)) ]
		ifFalse: [ ^aPrice ].
		
		! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'PorcentajeFinDeSemana class' category: 'ISW1-2021-2C-Recuperatorio-2doParcial'!
PorcentajeFinDeSemana class
	instanceVariableNames: ''!

!PorcentajeFinDeSemana class methodsFor: 'instance creation' stamp: 'AE 12/6/2021 18:49:59'!
de: aPercentage
	^self new initializeDe: aPercentage! !


!classDefinition: #PrecioEstablecido category: 'ISW1-2021-2C-Recuperatorio-2doParcial'!
Modificador subclass: #PrecioEstablecido
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2021-2C-Recuperatorio-2doParcial'!

!PrecioEstablecido methodsFor: 'accessing' stamp: 'AE 12/6/2021 18:40:12'!
modifica: aPrice en: aFixedGregorianDate 
	^aPrice! !


!classDefinition: #Propiedad category: 'ISW1-2021-2C-Recuperatorio-2doParcial'!
Object subclass: #Propiedad
	instanceVariableNames: 'barrio area string simpleMeasure modificadorDePrecio'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2021-2C-Recuperatorio-2doParcial'!

!Propiedad methodsFor: 'initialization' stamp: 'AE 12/6/2021 17:50:07'!
initializeEn: aBarrio de: anArea 
	barrio := aBarrio.
	area := anArea.! !

!Propiedad methodsFor: 'initialization' stamp: 'AE 12/6/2021 18:34:49'!
initializeEn: aBarrio de: anArea modificador: aModificador 

	barrio := aBarrio.
	area := anArea.
	modificadorDePrecio := aModificador.! !


!Propiedad methodsFor: 'accessing' stamp: 'AE 12/6/2021 18:23:34'!
area
	^area! !

!Propiedad methodsFor: 'accessing' stamp: 'AE 12/6/2021 18:13:14'!
barrio

	^barrio! !

!Propiedad methodsFor: 'accessing' stamp: 'AE 12/6/2021 18:36:41'!
modificaPrecio: aPrice en: aFixedGregorianDate 
	
	
	^ modificadorDePrecio modifica: aPrice en: aFixedGregorianDate .
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Propiedad class' category: 'ISW1-2021-2C-Recuperatorio-2doParcial'!
Propiedad class
	instanceVariableNames: ''!

!Propiedad class methodsFor: 'instance creation' stamp: 'AE 12/6/2021 18:38:35'!
en: barrio de: area

	^self en: barrio de: area modificador: (PrecioEstablecido new).
	
	 ! !

!Propiedad class methodsFor: 'instance creation' stamp: 'AE 12/6/2021 18:33:00'!
en: aString de: aSimpleMeasure modificador: aPorcentajeDiferencial 
	^self new initializeEn: aString de: aSimpleMeasure modificador: aPorcentajeDiferencial ! !
