!classDefinition: #CartTest category: 'TusLibros'!
TestCase subclass: #CartTest
	instanceVariableNames: 'testObjectsFactory'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test01NewCartsAreCreatedEmpty

	self assert: testObjectsFactory createCart isEmpty! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test02CanNotAddItemsThatDoNotBelongToStore

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self 
		should: [ cart add: testObjectsFactory itemNotSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test03AfterAddingAnItemTheCartIsNotEmptyAnymore

	| cart |
	
	cart := testObjectsFactory createCart.
	
	cart add: testObjectsFactory itemSellByTheStore.
	self deny: cart isEmpty ! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test04CanNotAddNonPositiveNumberOfItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self 
		should: [cart add: 0 of: testObjectsFactory itemSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidQuantityErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test05CanNotAddMoreThanOneItemNotSellByTheStore

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self 
		should: [cart add: 2 of: testObjectsFactory itemNotSellByTheStore  ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:11'!
test06CartRemembersAddedItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	cart add: testObjectsFactory itemSellByTheStore.
	self assert: (cart includes: testObjectsFactory itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:11'!
test07CartDoesNotHoldNotAddedItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self deny: (cart includes: testObjectsFactory itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:11'!
test08CartRemembersTheNumberOfAddedItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	cart add: 2 of: testObjectsFactory itemSellByTheStore.
	self assert: (cart occurrencesOf: testObjectsFactory itemSellByTheStore) = 2! !


!CartTest methodsFor: 'setup' stamp: 'HernanWilkinson 6/17/2013 18:09'!
setUp 

	testObjectsFactory := StoreTestObjectsFactory new.! !


!classDefinition: #CashierTest category: 'TusLibros'!
TestCase subclass: #CashierTest
	instanceVariableNames: 'testObjectsFactory debitBehavior'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:50'!
test01CanNotCheckoutAnEmptyCart

	| salesBook |
	
	salesBook := OrderedCollection new.
	self 
		should: [ Cashier 
			toCheckout: testObjectsFactory createCart 
			charging: testObjectsFactory notExpiredCreditCard 
			throught: self
			on: testObjectsFactory today
			registeringOn:  salesBook ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = Cashier cartCanNotBeEmptyErrorMessage.
			self assert: salesBook isEmpty ]! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:51'!
test02CalculatedTotalIsCorrect

	| cart cashier |
	
	cart := testObjectsFactory createCart.
	cart add: 2 of: testObjectsFactory itemSellByTheStore.
	
	cashier :=  Cashier
		toCheckout: cart 
		charging: testObjectsFactory notExpiredCreditCard 
		throught: self
		on: testObjectsFactory today 
		registeringOn: OrderedCollection new.
		
	self assert: cashier checkOut = (testObjectsFactory itemSellByTheStorePrice * 2)! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:51'!
test03CanNotCheckoutWithAnExpiredCreditCart

	| cart salesBook |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore.
	salesBook := OrderedCollection new.
	
	self
		should: [ Cashier 
				toCheckout: cart 
				charging: testObjectsFactory expiredCreditCard 
				throught: self
				on: testObjectsFactory today
				registeringOn: salesBook ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError | 
			self assert: anError messageText = Cashier canNotChargeAnExpiredCreditCardErrorMessage.
			self assert: salesBook isEmpty ]! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 19:04'!
test04CheckoutRegistersASale

	| cart cashier salesBook total |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore.
	salesBook := OrderedCollection new.
 
	cashier:= Cashier 
		toCheckout: cart 
		charging: testObjectsFactory notExpiredCreditCard
		throught: self
		on: testObjectsFactory today
		registeringOn: salesBook.
		
	total := cashier checkOut.
					
	self assert: salesBook size = 1.
	self assert: salesBook first total = total.! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 19:00'!
test05CashierChargesCreditCardUsingMerchantProcessor

	| cart cashier salesBook total creditCard debitedAmout debitedCreditCard  |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore.
	creditCard := testObjectsFactory notExpiredCreditCard.
	salesBook := OrderedCollection new.
 
	cashier:= Cashier 
		toCheckout: cart 
		charging: creditCard
		throught: self
		on: testObjectsFactory today
		registeringOn: salesBook.
		
	debitBehavior := [ :anAmount :aCreditCard | 
		debitedAmout := anAmount.
		debitedCreditCard := aCreditCard ].
	total := cashier checkOut.
					
	self assert: debitedCreditCard = creditCard.
	self assert: debitedAmout = total.! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:59'!
test06CashierDoesNotSaleWhenTheCreditCardHasNoCredit

	| cart cashier salesBook creditCard |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore.
	creditCard := testObjectsFactory notExpiredCreditCard.
	salesBook := OrderedCollection new.
 	debitBehavior := [ :anAmount :aCreditCard | self error: Cashier creditCardHasNoCreditErrorMessage].
	
	cashier:= Cashier 
		toCheckout: cart 
		charging: creditCard
		throught: self
		on: testObjectsFactory today
		registeringOn: salesBook.
		
	self 
		should: [cashier checkOut ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = Cashier creditCardHasNoCreditErrorMessage.
			self assert: salesBook isEmpty ]! !


!CashierTest methodsFor: 'setup' stamp: 'HernanWilkinson 6/17/2013 19:03'!
setUp 

	testObjectsFactory := StoreTestObjectsFactory new.
	debitBehavior := [ :anAmount :aCreditCard | ]! !


!CashierTest methodsFor: 'merchant processor protocol' stamp: 'HernanWilkinson 6/17/2013 19:02'!
debit: anAmount from: aCreditCard 

	^debitBehavior value: anAmount value: aCreditCard ! !


!classDefinition: #TusLibrosInterfaceTest category: 'TusLibros'!
TestCase subclass: #TusLibrosInterfaceTest
	instanceVariableNames: 'storeObjects clockBehavior debitBehavior authenticationBehavior'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!TusLibrosInterfaceTest methodsFor: 'setUp/tearDown' stamp: 'AE 11/11/2021 12:54:44'!
checkOutWithValidCreditCard: aCartId on: anInterface 

	| creditCardExpDate creditCardId creditCardOwner |
	
	creditCardId _ 'soy un numero' .
	creditCardOwner _ 'soy un owner'.
	creditCardExpDate _ (self now + 365 days) month.

	^ anInterface checkOutCart: aCartId ccn: creditCardId cced: creditCardExpDate cco: creditCardOwner.
	! !

!TusLibrosInterfaceTest methodsFor: 'setUp/tearDown' stamp: 'JDS 11/10/2021 20:10:23'!
createCartOn: aInterface
	
	| userId pass |
	userId _ storeObjects validUser .
	pass _ storeObjects validPassword .
	
	^ aInterface createCartFor: userId pass: pass .
	
	! !

!TusLibrosInterfaceTest methodsFor: 'setUp/tearDown' stamp: 'AE 11/10/2021 01:02:12'!
createInterface
	
	^TusLibrosInterface withAuthenticator: self with: storeObjects defaultCatalog clock: self processor: self.
	
	! !

!TusLibrosInterfaceTest methodsFor: 'setUp/tearDown' stamp: 'AE 11/10/2021 01:03:30'!
setUp
	
	storeObjects := StoreTestObjectsFactory new.
	
	authenticationBehavior _ [ :user :pass | 
		(user = (storeObjects validUser)) 
		and: [pass = (storeObjects validPassword)].  ].
	
	debitBehavior := [ :anAmount :aCreditCard | ].
		
	clockBehavior _ [ DateAndTime now ].
	! !


!TusLibrosInterfaceTest methodsFor: 'testing' stamp: 'AE 11/9/2021 23:30:08'!
test01UsuarioCreaCarritoYElCarritoEstaVacio

	| cartid interface |
	
	interface := self createInterface .
	
	cartid := self createCartOn: interface .
	
	self assert: (interface listCart: cartid ) isEmpty ! !

!TusLibrosInterfaceTest methodsFor: 'testing' stamp: 'JDS 11/10/2021 20:10:22'!
test02UsuarioInvalidoNoPuedeCrearCarrito

	| interface   idClient passClient |
	
	idClient := 'usuarioInvalido'.
	passClient := 'passInvalida'.
	interface := self createInterface .
	
	self should: [interface createCartFor: idClient pass: passClient.]
	 	raise: Error  
		withExceptionDo: [  :anError |
			self assert: anError messageText = TusLibrosInterface InvalidUserError]
	 ! !

!TusLibrosInterfaceTest methodsFor: 'testing' stamp: 'AE 11/9/2021 23:35:19'!
test03AgregarUnItemAUnCarritoYPuedeListarlo

	| cartid interface list anItem |
	
	interface := self createInterface .
	
	anItem := storeObjects itemSellByTheStore.
	cartid := self createCartOn: interface .
	
	interface addtoCart: cartid items: anItem withQuantity: 1.
	list := interface listCart: cartid.

	self assert: (list size) equals: 1. 
	self assert:  list includes: anItem.
	
	
	! !

!TusLibrosInterfaceTest methodsFor: 'testing' stamp: 'JDS 11/10/2021 15:03:40'!
test04ListarUnCarritoInvalidoDaError

	| interface invalidCartId |

	invalidCartId _ -1.	
	interface := self createInterface .
	
	self should: 	[interface listCart: invalidCartId ]
		raise: Error
		withMessageText: TusLibrosInterface invalidCartIdErrorDescription.
		
	
	
	
	! !

!TusLibrosInterfaceTest methodsFor: 'testing' stamp: 'JDS 11/10/2021 15:03:05'!
test05AgregarItemACarritoInvalidoDaError

	| interface invalidCartId |

	invalidCartId _ -1.	
	interface := self createInterface .
	
	self should: 	[interface addtoCart: invalidCartId  items: storeObjects itemSellByTheStore withQuantity: 1 ]
		raise: Error
		withMessageText: TusLibrosInterface invalidCartIdErrorDescription.
		
	
	
	
	! !

!TusLibrosInterfaceTest methodsFor: 'testing' stamp: 'JDS 11/10/2021 20:10:23'!
test06AgregarItemACarritoInactivoDaError

	| interface cartid idClient passClient |
	
	clockBehavior _ [ DateAndTime now ].

	idClient := storeObjects validUser .
	passClient := storeObjects validPassword .
	interface := self createInterface .
	
	cartid := interface createCartFor: idClient pass: passClient .
	
	clockBehavior _ [ DateAndTime now + 40 minutes ].
	
	self should: 	[ interface addtoCart: cartid items: storeObjects itemSellByTheStore withQuantity: 1 ]
		raise: Error
		withMessageText: TusLibrosInterface invalidCartIdErrorDescription.
		
	
	
	
	! !

!TusLibrosInterfaceTest methodsFor: 'testing' stamp: 'JDS 11/10/2021 15:04:22'!
test07ListarUnCarritoInactivoDaError

	| cartid interface anItem |
	
	interface := self createInterface .

	clockBehavior _ [ DateAndTime now ].

	cartid := self createCartOn: interface .
	anItem := storeObjects itemSellByTheStore.
	interface addtoCart: cartid items: anItem withQuantity: 1.
	
	clockBehavior _ [ DateAndTime now  + 31 minutes ].

	self should: 	[ interface listCart: cartid. ]
		raise: Error
		withMessageText: TusLibrosInterface invalidCartIdErrorDescription.
	
	
	
	
	! !

!TusLibrosInterfaceTest methodsFor: 'testing' stamp: 'AE 11/11/2021 12:54:44'!
test08CheckoutConIdInactivoDaError

	| cartid interface anItem |

	clockBehavior _ [ DateAndTime now ].
	
	interface := self createInterface .
	anItem := storeObjects itemSellByTheStore.
	cartid := self createCartOn: interface .
	interface addtoCart: cartid items: anItem withQuantity: 1.

	clockBehavior _ [ DateAndTime now + 31 minutes].

	self should: [ self checkOutWithValidCreditCard: cartid on: interface ]
		raise: Error
		withMessageText: TusLibrosInterface invalidCartIdErrorDescription.
	
	
	
	
	! !

!TusLibrosInterfaceTest methodsFor: 'testing' stamp: 'AE 11/11/2021 12:54:44'!
test09CheckoutConIdInvalidoDaError

	| interface invalidCartId |

	invalidCartId _ 1.	
	interface := self createInterface .
	
	self should: [ self checkOutWithValidCreditCard: invalidCartId on: interface ]
		raise: Error
		withMessageText: TusLibrosInterface invalidCartIdErrorDescription.
	
	
	
	
	! !

!TusLibrosInterfaceTest methodsFor: 'testing' stamp: 'AE 11/11/2021 12:54:44'!
test10CheckoutConCarritoVacioDaError

	| interface cartid |

	interface := self createInterface .
	cartid := self createCartOn: interface .
	
	self should: [ self checkOutWithValidCreditCard: cartid on: interface ]
		raise: Error - MessageNotUnderstood 
		withMessageText: TusLibrosInterface emptyCartCheckoutErrorDescription.
	
	
	
	
	
! !

!TusLibrosInterfaceTest methodsFor: 'testing' stamp: 'JDS 11/10/2021 17:07:00'!
test11CheckoutConCarritoValidoDejaDeExistir

	| cartId interface anItem creditCardExpDate creditCardId creditCardOwner |

	interface := self createInterface .
	anItem := storeObjects itemSellByTheStore.
	cartId := self createCartOn: interface .
	interface addtoCart: cartId items: anItem withQuantity: 1.

	creditCardId _ 'soy un numero' .
	creditCardOwner _ 'soy un owner'.
	creditCardExpDate _ (self now + 365 days) month.
	
	interface checkOutCart: cartId ccn: creditCardId cced:  creditCardExpDate  cco: creditCardOwner.
	self  should: [interface addtoCart: cartId items: anItem withQuantity: 1] 
		raise: Error 
		withMessageText: TusLibrosInterface invalidCartIdErrorDescription.
	! !

!TusLibrosInterfaceTest methodsFor: 'testing' stamp: 'JDS 11/10/2021 21:12:18'!
test12CheckoutConCarritoValidoPermiteListarCompra

	| cartId interface anItem creditCardExpDate creditCardId idClient passClient purchaseListExpected |

	idClient := storeObjects validUser .
	passClient := storeObjects validPassword .
	interface := self createInterface .
	
	cartId := interface createCartFor: idClient pass: passClient .

	anItem := storeObjects itemSellByTheStore.
	interface addtoCart: cartId items: anItem withQuantity: 1.
	purchaseListExpected _ interface listCart: cartId . 

	creditCardId _ 'soy un numero' .
	creditCardExpDate _ (self now + 365 days) month.
	
	interface checkOutCart: cartId ccn: creditCardId cced:  creditCardExpDate  cco: idClient.
	self assert: purchaseListExpected equals: (interface listPurchase: idClient ). ! !

!TusLibrosInterfaceTest methodsFor: 'testing' stamp: 'AE 11/11/2021 12:52:35'!
test13CheckoutConMultiplesCarritosYSePuedenListarLasCompras

	| cartId interface anItem creditCardExpDate creditCardId idClient passClient purchaseListExpected cartId2 creditCardId2 idClient2 passClient2 purchaseListExpected2 |

	idClient := storeObjects validUser .
	passClient := storeObjects validPassword .
	interface := self createInterface .
	
	cartId := interface createCartFor: idClient pass: passClient .

	anItem := storeObjects itemSellByTheStore.
	interface addtoCart: cartId items: anItem withQuantity: 1.
	purchaseListExpected _ interface listCart: cartId . 

	creditCardId _ 'soy un numero' .
	creditCardExpDate _ (self now + 365 days) month.
	


	idClient2 := storeObjects validUser .
	passClient2 := storeObjects validPassword .
	
	cartId2 := interface createCartFor: idClient2 pass: passClient2.

	interface addtoCart: cartId2 items: anItem withQuantity: 1.
	purchaseListExpected2 _ interface listCart: cartId2. 

	creditCardId2 _ 'soy otro numero' .

	interface checkOutCart: cartId ccn: creditCardId cced:  creditCardExpDate  cco: idClient.
	self assert: purchaseListExpected equals: (interface listPurchase: idClient ).
	
	interface checkOutCart: cartId2 ccn: creditCardId2 cced:  creditCardExpDate  cco: idClient2.
	self assert: purchaseListExpected2 equals: (interface listPurchase: idClient2 ).
		
	 ! !

!TusLibrosInterfaceTest methodsFor: 'testing' stamp: 'AE 11/11/2021 12:53:09'!
test14CheckoutConTarjetaExpiradaDaError

	| cartId interface anItem creditCardExpDate creditCardId idClient passClient |

	idClient := storeObjects validUser .
	passClient := storeObjects validPassword .
	interface := self createInterface .
	
	cartId := interface createCartFor: idClient pass: passClient .

	anItem := storeObjects itemSellByTheStore.
	interface addtoCart: cartId items: anItem withQuantity: 1.

	creditCardId _ 'soy un numero' .
	creditCardExpDate _ (self now - 365 days)  month.
	
	self  should: [interface checkOutCart: cartId ccn: creditCardId cced:  creditCardExpDate  cco: idClient] 
		raise: Error 
		withMessageText: [ TusLibrosInterface canNotChargeAnExpiredCreditCardErrorMessage ].! !


!TusLibrosInterfaceTest methodsFor: 'authenticator protocol' stamp: 'AE 11/9/2021 19:30:02'!
checkUser: anUser andPass: aPass

	^authenticationBehavior value: anUser value: aPass.
	 
! !


!TusLibrosInterfaceTest methodsFor: 'clock protocol' stamp: 'AE 11/9/2021 22:24:55'!
now
	^ clockBehavior value.
	! !


!TusLibrosInterfaceTest methodsFor: 'merchant processor protocol' stamp: 'AE 11/10/2021 01:02:49'!
debit: anAmount from: aCreditCard 

	^debitBehavior value: anAmount value: aCreditCard ! !


!classDefinition: #Cart category: 'TusLibros'!
Object subclass: #Cart
	instanceVariableNames: 'catalog items'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidItemErrorMessage
	
	^'Item is not in catalog'! !

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidQuantityErrorMessage
	
	^'Invalid number of items'! !


!Cart methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 18:06'!
assertIsValidItem: anItem

	(catalog includesKey: anItem) ifFalse: [ self error: self invalidItemErrorMessage ]! !

!Cart methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 17:51'!
assertIsValidQuantity: aQuantity

	aQuantity strictlyPositive ifFalse: [ self error: self invalidQuantityErrorMessage ]! !


!Cart methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 17:48'!
initializeAcceptingItemsOf: aCatalog

	catalog := aCatalog.
	items := OrderedCollection new.! !


!Cart methodsFor: 'queries' stamp: 'FM 11/8/2021 19:59:56'!
listItems
	
	^items copy
	! !

!Cart methodsFor: 'queries' stamp: 'HernanWilkinson 6/17/2013 17:45'!
occurrencesOf: anItem

	^items occurrencesOf: anItem  ! !


!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
includes: anItem

	^items includes: anItem ! !

!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
isEmpty
	
	^items isEmpty ! !


!Cart methodsFor: 'total' stamp: 'HernanWilkinson 6/17/2013 19:09'!
total

	^ items sum: [ :anItem | catalog at: anItem ]! !


!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:44'!
add: anItem

	^ self add: 1 of: anItem ! !

!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:51'!
add: aQuantity of: anItem

	self assertIsValidQuantity: aQuantity.
	self assertIsValidItem: anItem.

	1 to: aQuantity do: [ :aNumber | items add: anItem ]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: 'TusLibros'!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'instance creation' stamp: 'AE 11/9/2021 21:36:18'!
acceptingItemsOf: aCatalog

	^self basicNew initializeAcceptingItemsOf: aCatalog ! !

!Cart class methodsFor: 'instance creation' stamp: 'AE 11/9/2021 21:35:53'!
new
	self error: 'Call with acceptingItemsOf:'.
	! !


!classDefinition: #Cashier category: 'TusLibros'!
Object subclass: #Cashier
	instanceVariableNames: 'cart salesBook merchantProcessor creditCard total'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cashier methodsFor: 'checkout - private' stamp: 'HernanWilkinson 6/17/2013 19:08'!
calculateTotal

	total := cart total.
	! !

!Cashier methodsFor: 'checkout - private' stamp: 'HernanWilkinson 6/17/2013 19:07'!
createSale

	^ Sale of: total
! !

!Cashier methodsFor: 'checkout - private' stamp: 'HernanWilkinson 6/17/2013 19:06'!
debitTotal

	merchantProcessor debit: total from: creditCard.
	! !

!Cashier methodsFor: 'checkout - private' stamp: 'HernanWilkinson 6/17/2013 19:06'!
registerSale

	salesBook add: self createSale! !


!Cashier methodsFor: 'checkout' stamp: 'HernanWilkinson 6/17/2013 19:06'!
checkOut

	self calculateTotal.
	self debitTotal.
	self registerSale.

	^ total! !


!Cashier methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 18:53'!
initializeToCheckout: aCart charging: aCreditCard throught: aMerchantProcessor registeringOn: aSalesBook
	
	cart := aCart.
	creditCard := aCreditCard.
	merchantProcessor := aMerchantProcessor.
	salesBook := aSalesBook! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cashier class' category: 'TusLibros'!
Cashier class
	instanceVariableNames: ''!

!Cashier class methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 18:22'!
assertIsNotEmpty: aCart 
	
	aCart isEmpty ifTrue: [self error: self cartCanNotBeEmptyErrorMessage ]! !

!Cashier class methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 18:23'!
assertIsNotExpired: aCreditCard on: aDate
	
	(aCreditCard isExpiredOn: aDate) ifTrue: [ self error: self canNotChargeAnExpiredCreditCardErrorMessage ]! !


!Cashier class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 18:51'!
toCheckout: aCart charging: aCreditCard throught: aMerchantProcessor on: aDate registeringOn: aSalesBook
	
	self assertIsNotEmpty: aCart.
	self assertIsNotExpired: aCreditCard on: aDate.
	
	^self new initializeToCheckout: aCart charging: aCreditCard throught: aMerchantProcessor registeringOn: aSalesBook! !


!Cashier class methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 18:21'!
canNotChargeAnExpiredCreditCardErrorMessage
	
	^'Can not charge an expired credit card'! !

!Cashier class methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:56'!
cartCanNotBeEmptyErrorMessage
	
	^'Can not check out an empty cart'! !

!Cashier class methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 19:02'!
creditCardHasNoCreditErrorMessage
	
	^'Credit card has no credit'! !


!classDefinition: #CreditCard category: 'TusLibros'!
Object subclass: #CreditCard
	instanceVariableNames: 'expiration'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCard methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 18:39'!
isExpiredOn: aDate 
	
	^expiration start < (Month month: aDate monthIndex year: aDate yearNumber) start ! !


!CreditCard methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 18:38'!
initializeExpiringOn: aMonth 
	
	expiration := aMonth ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCard class' category: 'TusLibros'!
CreditCard class
	instanceVariableNames: ''!

!CreditCard class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 18:38'!
expiringOn: aMonth 
	
	^self new initializeExpiringOn: aMonth! !


!classDefinition: #Sale category: 'TusLibros'!
Object subclass: #Sale
	instanceVariableNames: 'total'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Sale methodsFor: 'total' stamp: 'HernanWilkinson 6/17/2013 18:48'!
total
	
	^ total! !


!Sale methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 18:47'!
initializeTotal: aTotal

	total := aTotal ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Sale class' category: 'TusLibros'!
Sale class
	instanceVariableNames: ''!

!Sale class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 18:47'!
of: aTotal

	"should assert total is not negative or 0!!"
	^self new initializeTotal: aTotal ! !


!classDefinition: #StoreTestObjectsFactory category: 'TusLibros'!
Object subclass: #StoreTestObjectsFactory
	instanceVariableNames: 'today'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'HernanWilkinson 6/17/2013 18:08'!
itemNotSellByTheStore
	
	^'invalidBook'! !

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'HernanWilkinson 6/17/2013 18:08'!
itemSellByTheStore
	
	^ 'validBook'! !

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'HernanWilkinson 6/17/2013 18:08'!
itemSellByTheStorePrice
	
	^10! !


!StoreTestObjectsFactory methodsFor: 'cart' stamp: 'HernanWilkinson 6/17/2013 18:08'!
createCart
	
	^Cart acceptingItemsOf: self defaultCatalog! !

!StoreTestObjectsFactory methodsFor: 'cart' stamp: 'HernanWilkinson 6/17/2013 18:08'!
defaultCatalog
	
	^ Dictionary new
		at: self itemSellByTheStore put: self itemSellByTheStorePrice;
		yourself ! !

!StoreTestObjectsFactory methodsFor: 'cart' stamp: 'AE 11/9/2021 19:31:35'!
validPassword
	^'validPasswordForValidUser'.
	! !

!StoreTestObjectsFactory methodsFor: 'cart' stamp: 'AE 11/9/2021 19:31:07'!
validUser
	^'validUser'.
	! !


!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'HernanWilkinson 6/17/2013 18:37'!
expiredCreditCard
	
	^CreditCard expiringOn: (Month month: today monthIndex year: today yearNumber - 1)! !

!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'HernanWilkinson 6/17/2013 18:36'!
notExpiredCreditCard
	
	^CreditCard expiringOn: (Month month: today monthIndex year: today yearNumber + 1)! !


!StoreTestObjectsFactory methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 18:37'!
initialize

	today := DateAndTime now! !


!StoreTestObjectsFactory methodsFor: 'date' stamp: 'HernanWilkinson 6/17/2013 18:37'!
today
	
	^ today! !


!classDefinition: #TusLibrosInterface category: 'TusLibros'!
Object subclass: #TusLibrosInterface
	instanceVariableNames: 'authenticator cartItems lastCartId carts catalog clock cartLastActivity merchantProcessor listPurchase'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!TusLibrosInterface methodsFor: 'initialization' stamp: 'JDS 11/10/2021 19:53:46'!
initializeWith: anAuthenticator catalog: aCatalog clock: aClock processor: aMProcessor

	authenticator := anAuthenticator.

	carts _ Dictionary new.
	cartLastActivity _ Dictionary new.
	listPurchase _ Dictionary new.
		
	catalog _ aCatalog .

	clock _ aClock.

	merchantProcessor _ aMProcessor .
				
	lastCartId _ 1.
	! !


!TusLibrosInterface methodsFor: 'private' stamp: 'JDS 11/10/2021 23:37:27'!
cart: id

	| timestamp |
	
	timestamp _ cartLastActivity at: id
		ifAbsent: [ self error: TusLibrosInterface invalidCartIdErrorDescription ].
	
	
	((clock now - timestamp) > 30 minutes)
		ifTrue: [ 		self releaseCart: id.
				self error: TusLibrosInterface invalidCartIdErrorDescription. 	].
		
	^carts at: id
		ifAbsent: [ self shouldNotHappenBecause: 'Carrito id no deber�a existir en cartLastActivity' ].
		! !

!TusLibrosInterface methodsFor: 'private' stamp: 'AE 11/9/2021 20:02:41'!
newCartId

	lastCartId _ lastCartId + 1.
	
	^lastCartId! !

!TusLibrosInterface methodsFor: 'private' stamp: 'JDS 11/10/2021 18:41:57'!
releaseCart: aCartId
	
	cartLastActivity removeKey: aCartId.
	carts removeKey: aCartId.
	! !

!TusLibrosInterface methodsFor: 'private' stamp: 'AE 11/9/2021 22:38:55'!
updateTimestampOf: aCartID

	cartLastActivity at: aCartID put: clock now.
	! !


!TusLibrosInterface methodsFor: 'user interface' stamp: 'AE 11/9/2021 22:39:43'!
addtoCart: idCart items: anItem withQuantity: aQuantity
	
	(self cart: idCart) add: aQuantity of: anItem .
	self updateTimestampOf: idCart.
	! !

!TusLibrosInterface methodsFor: 'user interface' stamp: 'JDS 11/10/2021 23:23:57'!
checkOutCart: aCartId ccn: aCcn cced: aCced cco: owner

	| cart cashier creditCard items |
	cart _ self cart: aCartId .
	
	(cart isEmpty ) ifTrue: [ self error: self class emptyCartCheckoutErrorDescription. ].
	
	items _ cart listItems.

	creditCard _ CreditCard expiringOn: aCced .
	
	"Cashier toCheckout: cart charging: owner throught:  on:  registeringOn: "
	cashier :=  Cashier
		toCheckout: cart 
		charging: creditCard 
		throught: merchantProcessor 
		on: clock now
		registeringOn: OrderedCollection new.
		
	cashier checkOut. 
	self releaseCart: aCartId.
	listPurchase at: owner put: items. 
	^ 0! !

!TusLibrosInterface methodsFor: 'user interface' stamp: 'JDS 11/10/2021 20:10:22'!
createCartFor: aUser pass: aPass
	
	(authenticator checkUser: aUser andPass: aPass) 
		ifTrue: 
			[ | cartId | cartId _ self newCartId.
			  carts add: cartId -> (Cart acceptingItemsOf: catalog).
			  self updateTimestampOf: cartId.
			 ^cartId ].
		
	 self error: self class InvalidUserError
	! !

!TusLibrosInterface methodsFor: 'user interface' stamp: 'AE 11/9/2021 23:00:17'!
listCart: anId
	
	| cart |
	cart _ (self cart: anId) .
	
	self updateTimestampOf: anId.
	
	^ cart listItems asBag.
	
	! !

!TusLibrosInterface methodsFor: 'user interface' stamp: 'JDS 11/10/2021 20:16:36'!
listPurchase: anIdClient 

	^ (listPurchase at: anIdClient) asBag ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TusLibrosInterface class' category: 'TusLibros'!
TusLibrosInterface class
	instanceVariableNames: ''!

!TusLibrosInterface class methodsFor: 'authentication' stamp: 'AE 11/10/2021 01:01:30'!
withAuthenticator: anAuthenticator with: aCatalog clock: aClock processor: aMProcessor

	^self basicNew initializeWith: anAuthenticator catalog: aCatalog clock: aClock processor: aMProcessor .
	! !


!TusLibrosInterface class methodsFor: 'error handling' stamp: 'FM 11/4/2021 20:19:23'!
InvalidUserError
	^'No es un Usuario valido'! !

!TusLibrosInterface class methodsFor: 'error handling' stamp: 'JDS 11/10/2021 23:44:13'!
canNotChargeAnExpiredCreditCardErrorMessage

	^Cashier canNotChargeAnExpiredCreditCardErrorMessage ! !

!TusLibrosInterface class methodsFor: 'error handling' stamp: 'AE 11/10/2021 00:22:30'!
emptyCartCheckoutErrorDescription
	^'Cannot checkout an empty cart'.! !

!TusLibrosInterface class methodsFor: 'error handling' stamp: 'AE 11/9/2021 19:57:37'!
invalidCartIdErrorDescription
	^'Cart ID is invalid'.! !
